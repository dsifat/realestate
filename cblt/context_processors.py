import json

from django.core.cache import cache
from django.db.models import Q
from dateutil.relativedelta import relativedelta
from django.utils import simplejson as json
from mls.models import Area, City, Setting, View, \
                        Community, ListingStatus, HOAAmenities, \
                        WaterAmenities, Type, Listing


def search_params(request):
    search_params = {}
    subdivision = Area.objects.all().order_by('name')
    city = City.objects.all().order_by('name')
    setting = Setting.objects.all().order_by('name')
    views = View.objects.filter(active=True).order_by('name')
    community = Community.objects.all().exclude(name='SurroundingArea').order_by('name')
    status = ListingStatus.objects.all()
    hoa_amenities = HOAAmenities.objects.filter(active=True).order_by('name')
    waterfront_amenities = WaterAmenities.objects.filter(active=True)
    property_type = Type.objects.all().order_by('name')
    listing_count = Listing.objects.all().count()     
    oldest_listing = Listing.objects.all().order_by('C_LstDate')[0]
    latest_listing = Listing.objects.all().order_by('-C_LstDate')[0]
    years_of_data = relativedelta(latest_listing.C_LstDate, oldest_listing.C_LstDate).years
    community_map = {}
    city_map = {}
    for c in community:
        community_map[c.name] = []
        city_map[c.name] = []
        for area in c.area.all():
            community_map[c.name].append(str(area).title())
        for cty in c.city.all():
            city_map[c.name].append(str(cty).title())

    search_params = {'SUBDIVISIONS': subdivision,
                     'SUBDIVISIONS_JSON': json.dumps([str(sub.name).title() for sub in subdivision]),
                     'CITIES': city,
                     'SETTINGS': setting,
                     'VIEWS': views,
                     'COMMUNITIES': community,
                     'status': status,
                     'HOA_AMENITIES': hoa_amenities,
                     'WATERFRONT_AMENITIES': waterfront_amenities,
                     'PROPERTY_TYPES': property_type,
                     'COMMUNITY_MAP': json.dumps(community_map),
                     'CITY_MAP': json.dumps(city_map),
                     'listing_count' : listing_count,
                     'years_of_data' : years_of_data,
                     }

    return search_params

def featured_properties(request):
    if not cache.get('FEATURED_PROPERTIES'):
        featured_properties = Listing.objects.select_related('C_Community', 'C_City', 'img_set',)\
                                    .filter(C_Status__name='ACTIVE', C_AskPrice__gte=600000)\
                                    .exclude(Q(C_Community__name='SurroundingArea')
                                            |Q(C_Community__name='Sierra County'))\
                                    .order_by('?')\
                                    .only('C_LstDate', 'C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice','C_Address',
                                              'C_MLSNo', 'C_NoBeds', 'C_NoBaths1', 'C_Garage', 'C_SqFeet', 'C_YrBlt',
                                              'C_AcresNo', 'C_LotDims', 'C_City', 'C_GeoLat', 'C_GeoLong')[:5]
        cache.set('FEATURED_PROPERTIES', featured_properties)
    else:
        featured_properties = cache.get('FEATURED_PROPERTIES')
    return {'FEATURED_PROPERTIES': featured_properties}

def latest_properties(request):
    latest_properties = Listing.objects.select_related('C_Community', 'C_City')\
                                .filter(C_Status__name='ACTIVE')\
                                .exclude(Q(C_Community__name='SurroundingArea')
                                        |Q(C_Community__name='Sierra County'))\
                                .order_by('-C_LstDate')\
                                .only('C_LstDate', 'C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice','C_Address',
                                          'C_MLSNo', 'C_NoBeds', 'C_NoBaths1', 'C_Garage', 'C_SqFeet', 'C_YrBlt',
                                          'C_AcresNo', 'C_LotDims', 'C_City', 'C_GeoLat', 'C_GeoLong')[:5]
    return {'LATEST_PROPERTIES': latest_properties}


def listing_count(request):
    return {'available_listing_count': Listing.objects.filter(C_Status__name='ACTIVE').count()}
