from django import forms

class ContactUsForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'text'}),
                                 error_messages={'required': 'Please enter your name.'}, required=True, max_length=100)
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'text'}), \
                                error_messages={'required': 'Please enter your email address','invalid': 'Invalid email address'}, \
                                required=True, max_length=100)
    phone = forms.CharField(widget=forms.TextInput(attrs={'class':'text'}),
                                required=False, max_length=100)
    message = forms.CharField(widget=forms.Textarea(), error_messages={'required': 'Please enter a message.'}, required=True)
