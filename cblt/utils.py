from dateutil.relativedelta import relativedelta
from datetime import datetime, timedelta, date
from django.utils import simplejson as json
from django.contrib.humanize.templatetags.humanize import intcomma
from mls.models import Listing

#deprecate
def get_map_details(obj):
    #improve code too much query
    from mls.models import FeaturedListing, SellerComment
    prop_data_list = []
    latlng_list = []

    for prop in obj:
        prop_data = dict()
        latlng = list()

        if isinstance(prop, FeaturedListing) or isinstance(prop, SellerComment):
            listing = prop.listing
        else:
            listing = prop

        try:
            latlng = [float(listing.C_GeoLat), float(listing.C_GeoLong)]
            latlng_list.append(latlng)
            prop_data = '<div class="infobox"><div class="close"><img src="/static/img/close.png" alt=""></div><div class="image"><img src="'+ unicode(listing.get_thumbnail_270()).encode("utf-8") +'" alt=""><div class="price">$'+ unicode(intcomma(listing.C_AskPrice)).encode("utf-8") +'</div></div><div class="title"><a href="/realestate/listing/'+ unicode(listing.C_MLSNo).encode("utf-8") + '">'+ unicode(listing.C_Address).encode("utf-8") +'</a></div><div class="location">'+ unicode(listing.address_location()).encode("utf-8") + '</div><div class="bathrooms"><div class="content">' + unicode(listing.C_NoBaths1).encode("utf-8")+'</div></div><div class="bedrooms"><div class="content">' + unicode(listing.C_NoBeds).encode("utf-8") + '</div></div><div class="area"><div class="content">'+ unicode(listing.C_SqFeet).encode("utf-8") +' ft<sup>2</sup></div></div><div class="link"><a href="/realestate/listing/'+ unicode(listing.C_MLSNo).encode("utf-8") + '">View more</a></div></div>';
            prop_data_list.append(prop_data)
        except:
            pass
    return [latlng_list, prop_data_list]

#deprecate
def get_map_data(obj):
    #improve code too much query
    from mls.models import FeaturedListing, SellerComment
    loc_list = []

    for prop in obj:
        prop_dict = dict()

        if isinstance(prop, FeaturedListing) or isinstance(prop, SellerComment):
            listing = prop.listing
        else:
            listing = prop

        try:
            prop_dict['mls_no'] = unicode(listing.C_MLSNo).encode("utf-8")
            prop_dict['price'] = unicode(trunc(listing.C_AskPrice, 0)).encode("utf-8")
            prop_dict['address'] = unicode(listing.C_Address).encode("utf-8")
            prop_dict['beds'] = unicode(listing.C_NoBeds).encode("utf-8")
            prop_dict['baths'] = unicode(listing.C_NoBaths1).encode("utf-8")
            prop_dict['sqft'] = unicode(listing.C_SqFeet).encode("utf-8")
            prop_dict['image'] = unicode(listing.get_thumbnail_100()).encode("utf-8")
            prop_dict['community'] = unicode(listing.C_Community.shortened_name).encode("utf-8")
            prop_dict['lat'] = float(listing.C_GeoLat)
            prop_dict['lng'] = float(listing.C_GeoLong)
            loc_list.append(prop_dict)
        except:
            pass
    return loc_list

def get_plottable_content(format, content_array):
    """
    returns a list of the plottable contents from the SiteContent objects passed
    as a list.
    """
    if format == 'json':
        plottable_content = {}
        for content in content_array:
            if content.is_plottable():
                plottable_content[str(content.id)] = (content.get_json_form())
        plottable_content = json.dumps(plottable_content)
    else:
        #format == 'object'
        #create dictionary of plottable content grouped by location types
        plottable_content = {}
        for content in content_array:
            if content.is_plottable():
                location_type_set = content.location_type.all()

                for loc_type in location_type_set:
                    if loc_type.name in plottable_content:
                        plottable_content[loc_type.name][1].append(content)
                    else:
                        plottable_content[loc_type.name] = (loc_type, [])
                        plottable_content[loc_type.name][1].append(content)

    return plottable_content

def trunc(num, digits):
    sp = str(num).split('.')
    return sp[0]


def get_years_of_data():
    """
        Get Total years of data existence in the system.
    """
    oldest_listing = Listing.objects.all().order_by('C_LstDate')[0]
    latest_listing = Listing.objects.all().order_by('-C_LstDate')[0]
    years_of_data = relativedelta(latest_listing.C_LstDate, oldest_listing.C_LstDate).years
    return str(years_of_data)

def get_number_of_sales_last_days(months, community = None, city = None, area= None):
    """
        Get number of sales in past days based on months, community, city, area.

    """
    today = datetime.today()
    days = int(months)*30    
    listing = None
    if community:  
        print 'community'      
        listing = Listing.objects.filter(C_CloseDate__range=(today-timedelta(days=days), today),
                                               C_Status__name__iexact="SOLD",
                                               C_Community_id=community.id)
    elif city:
        print 'city'
        lisitng = Listing.objects.filter(C_CloseDate__range = (today-timedelta(days=days), today), 
                                      C_Status__name__iexact="SOLD", C_City_id=city.id)
    elif area:  
        print 'area'      
        lisitng = Listing.objects.filter(C_CloseDate__range=(today-timedelta(days=days), today),
                                               C_Status__name__iexact="SOLD",
                                               C_Area_id=area.id)
    
    if listing : 
        return listing.count()
