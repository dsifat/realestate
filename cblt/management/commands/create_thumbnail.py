from datetime import datetime

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand

from mls.models import Img


class Command(NoArgsCommand):
    help = 'Create image thumbnails'

    def handle_noargs(self, **options):
        print "Started creating thumbnails: " + str(datetime.now())
        images = Img.objects.filter(C_THUMB__isnull=True)
        print 'No. of images found for small -> ' + str(len(images))
        for img in images:
            try:
                img.create_thumbnail()
                print 'created thumbnail'
            except Exception as e:
                print e

        images = Img.objects.filter(C_LARGE_THUMB__isnull=True)
        print 'No. of images found for large -> ' + str(len(images))
        for img in images:
            try:
                img.create_large_thumbnail()
                print 'created large version'
            except Exception as e:
                print e

        print "Ended creating thumbnails: " + str(datetime.now())
