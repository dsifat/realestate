from datetime import datetime
from decimal import Decimal

from django.conf import settings
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from common.utils import get_or_none
import pymysql
import pygrametl
from pygrametl.datasources import SQLSource
import sys
from django import db


sys.setrecursionlimit(1500)

from mls.models import Area, Community, City, Setting,\
                        create_price_change_set, create_statistics, create_recent_sold, Listing, get_acreage, \
    get_create_area, get_create_city, get_create_community, get_create_setting, get_create_status, assoc_community_city, \
    assoc_community_area, get_create_hoa_amenities, get_create_water_amenities, get_create_view, PriceChange, \
    get_create_type


class Command(NoArgsCommand):
    help = 'Normalize Listings'

    def handle_noargs(self, **options):
        print "Started normalizing: " + str(datetime.now())
        DATABASE = db.settings.DATABASES['default']['NAME']
        USER = db.settings.DATABASES['default']['USER']
        PASSWORD = db.settings.DATABASES['default']['PASSWORD']
        print "DATABASE CONFIG :\n"
        print 'DATABASE : '+ DATABASE
        print 'USER : '+ USER
        print 'PASSWORD '+ PASSWORD
        try:
            conn = pymysql.connect(database=DATABASE, user=USER, password=PASSWORD)
            sql = 'SELECT * FROM mls_listingraw2'
            resultsSource = SQLSource(connection=conn, query=sql)
            for row in resultsSource:
                normalized = self.normalize_listing(row)
        except Exception as ex:
            print ex
            print(sys.exc_info()[0])
        print "Ended normalizing: " + str(datetime.now())

        print 'creating available property for city ...'
        for city in City.objects.all():
            city.available_properties = city.listing_set.filter(C_Status__name='ACTIVE').count()
            city.save()

        print 'creating available property for area...'
        for area in Area.objects.all():
            area.available_properties = area.listing_set.filter(C_Status__name='ACTIVE').count()
            area.save()

        print 'creating available property for community ...'
        for community in Community.objects.all():
            community.available_properties = community.listing_set.filter(C_Status__name='ACTIVE').count()
            community.save()

        print 'creating available property for setting...'
        for setting in Setting.objects.all():
            setting.available_properties = setting.listing_set.filter(C_Status__name='ACTIVE').count()
            setting.save()

        print 'create price change set ....'
        create_price_change_set()
        print 'create statistics'
        create_statistics()
        print 'create recent sold ...'
        create_recent_sold()
        print 'End ALL normalization ........................'
        
    def normalize_listing(self, raw_data):

        def to_datetime(string_obj, format):
            if bool(string_obj):
                return datetime.strptime(string_obj, format)

            return None

        normalized_listing = get_or_none(Listing, C_MLSNo=raw_data.get('C_MLSNo'))
        print "normalizing " + str(normalized_listing)
        try:
            if normalized_listing:
                listing_instance = normalized_listing
                self.create_price_change_record(normalized_listing,raw_data)
            else:
                listing_instance = Listing()

            listing_instance.C_NoBaths1 = raw_data.get('C_NoBaths')
            print "1"
            listing_instance.C_NoBeds = raw_data.get('C_NoBeds')
            print "2"
            ##listing_instance.C_N1stLnAmt = self.C_N1stLnAmt
            listing_instance.C_APN = raw_data.get('C_APN')
            print "3"
            listing_instance.C_APPLIANCES = raw_data.get('C_APPLIANCES')
            print "4"
            listing_instance.C_Addendum = raw_data.get('C_Addendum')
            print "5"
            listing_instance.C_Address = raw_data.get('C_Address')
            print "6"
            listing_instance.C_AddrDrctn = raw_data.get('C_AddrDrctn')
            print "7"
            listing_instance.C_AddrNumber = raw_data.get('C_AddrNumber')
            print "8"
            listing_instance.C_AddrStreet = raw_data.get('C_AddrStreet')
            print "9"
            listing_instance.C_Age = raw_data.get('C_Age')
            print "10"
            listing_instance.C_EMail = raw_data.get('C_EMail')
            print "11"
            listing_instance.C_Agent = raw_data.get('C_Agent')
            print "12"
            listing_instance.C_ApprxAcre = raw_data.get('C_ApprxAcre')
            print "13"
            listing_instance.C_ApprxSqFt = raw_data.get('C_ApprxSqFt')
            print "14"
            listing_instance.C_Architect = raw_data.get('C_Architect')
            print "15"
            listing_instance.C_Acreage = get_acreage(raw_data.get('C_ApprxAcre'))
            print "16"
            if raw_data.get('C_Area', None) is not None:
                raw_area = raw_data.get('C_Area',None)
                listing_instance.C_Area = get_create_area(raw_area)
                print "17"
                # listing_instance.C_Area = get_create_area(raw_data.get('C_Area'))
            try:
                listing_instance.C_AskPrice = Decimal(raw_data.get('C_AskPrice'))
                print "18"
            except:
                listing_instance.C_AskPrice = Decimal(raw_data.get('C_AskPrice'))
                print "19"
            listing_instance.C_AssocDoc = raw_data.get('C_AssocDoc')
            print "20"
            listing_instance.C_BONUSROOMS = raw_data.get('C_BONUSROOMS')
            print "21"
            listing_instance.C_BldgName = raw_data.get('C_BldgName')
            print "22"
            listing_instance.C_Show1 = raw_data.get('C_Show1')
            print "23"
            if raw_data.get('C_City', None) is not None:
                city = raw_data.get('C_City',None)
                listing_instance.C_City = get_create_city(city)
                print "24"
            listing_instance.C_Class = raw_data.get('C_Class')
            print "2"
            listing_instance.C_CloseDate = to_datetime(raw_data.get('C_CloseDate'), '%Y-%m-%d')
            print "25"
            if raw_data.get('C_Community', None) is not None:
                community = raw_data.get('C_Community',None)
                listing_instance.C_Community = get_create_community(community)
                print "26"
            listing_instance.C_ConstPhse = raw_data.get('C_ConstPhse')
            print "27"
            listing_instance.C_ContDate = to_datetime(raw_data.get('C_ContDate'), '%Y-%m-%d')
            print "28"
            listing_instance.C_County = raw_data.get('C_County')
            print "29"
            listing_instance.C_DECKS = raw_data.get('C_DECKS')
            print "30"
            listing_instance.C_DOM = raw_data.get('C_DOM')
            print "31"
            listing_instance.C_DOMLS = raw_data.get('C_DOMLS')
            print "32"
            listing_instance.C_DocDate = to_datetime(raw_data.get('C_DocDate'), '%Y-%m-%dT%H:%M:%S')
            print "33"
            listing_instance.C_FIREPLACE = raw_data.get('C_FIREPLACE')
            print "34"
            listing_instance.C_FLORCVRNGS = raw_data.get('C_FLORCVRNGS')
            print "35"
            listing_instance.C_FOUNDATION = raw_data.get('C_FOUNDATION')
            print "36"
            listing_instance.C_Furnished = raw_data.get('C_Furnished')
            print "37"
            listing_instance.C_GARAGPRKNG = raw_data.get('C_GARAGPRKNG')
            print "38"
            listing_instance.C_GAS = raw_data.get('C_GAS')
            print "39"
            listing_instance.C_GATED = raw_data.get('C_GATED')
            print "40"
            listing_instance.C_GRENFEATRS = raw_data.get('C_GRENFEATRS')
            print "41"
            listing_instance.C_Garage = raw_data.get('C_Garage')
            print "42"
            listing_instance.C_HNDCPDFRND = raw_data.get('C_HNDCPDFRND')
            print "43"
            listing_instance.C_HEATING = raw_data.get('C_HEATING')
            print "44"
            listing_instance.C_HOAAmt = raw_data.get('C_HOAAmt')
            print "45"
            listing_instance.C_HOAOpt = raw_data.get('C_HOAOpt')
            print "46"
            listing_instance.C_HOAPer = raw_data.get('C_HOAPer')
            print "47"
            listing_instance.C_HOAMbr = raw_data.get('C_HOAMbr')
            print "48"
            listing_instance.C_Show = raw_data.get('C_Show')
            print "49"
            listing_instance.C_HowSold = raw_data.get('C_HowSold')
            print "50"
            listing_instance.C_IDXIncl = raw_data.get('C_IDXIncl')
            print "51"
            listing_instance.C_IDXvt = raw_data.get('C_IDXvt')
            print "52"
            listing_instance.C_LA1Email = raw_data.get('C_LA1Email')
            print "53"
            listing_instance.C_LA1FirstNm = raw_data.get('C_LA1FirstNm')
            print "54"
            listing_instance.C_LA1LastNam = raw_data.get('C_LA1LastNam')
            print "55"
            #listing_instance.C_LA1Phon1Ar = self.C_LA1Phon1Ar
            listing_instance.C_LA1Phn1Nbr = raw_data.get('C_LA1Phn1Nbr')
            print "56"
            #listing_instance.C_LA1Phon2Ar = self.C_LA1Phon2Ar
            listing_instance.C_LA1Phn2Nbr = raw_data.get('C_LA1Phn2Nbr')
            print "57"
            #listing_instance.C_LA1Phon3Ar = self.C_LA1Phon3Ar
            listing_instance.C_LA1Phn3Nbr = raw_data.get('C_LA1Phn3Nbr')
            print "58"
            listing_instance.C_LA1Phn4Nbr = raw_data.get('C_LA1Phn4Nbr')
            print "59"
            #listing_instance.C_LA1Phon4Ar = self.C_LA1Phon4Ar
            #listing_instance.C_LA1Phon5Ar = self.C_LA1Phon5Ar
            listing_instance.C_LA1Phn5Nbr = raw_data.get('C_LA1Phn5Nbr')
            print "60"
            listing_instance.C_LA1Phn1Dsc = raw_data.get('C_LA1Phn1Dsc')
            print "61"
            listing_instance.C_LA1Phn2Dsc = raw_data.get('C_LA1Phn2Dsc')
            print "62"
            listing_instance.C_LA1Phn4Dsc = raw_data.get('C_LA1Phn4Dsc')
            print "63"
            listing_instance.C_LA1Phn5Dsc = raw_data.get('C_LA1Phn5Dsc')
            print "64"
            listing_instance.C_LA1Phn3Dsc = raw_data.get('C_LA1Phn3Dsc')
            print "65"
            listing_instance.C_LA1WorksFr = raw_data.get('C_LA1WorksFr')
            print "66"
            listing_instance.C_LAUNDRY = raw_data.get('C_LAUNDRY')
            print "67"
            listing_instance.C_LO1OfcName = raw_data.get('C_LO1OfcName')
            print "68"
            listing_instance.C_LO1Phn1Nbr = raw_data.get('C_LO1Phn1Nbr')
            print "69"
            listing_instance.C_LstDate = to_datetime(raw_data.get('C_LstDate'), '%Y-%m-%d')
            print "70"
            listing_instance.C_LType = raw_data.get('C_LType')
            print "71"
            listing_instance.C_LotNo = raw_data.get('C_LotNo')
            print "72"
            listing_instance.C_LotDims = raw_data.get('C_LotDims')
            print "73"
            listing_instance.C_MLSNo = raw_data.get('C_MLSNo')
            print "74"
            listing_instance.C_AcresNo = raw_data.get('C_AcresNo')
            print "75"
            listing_instance.C_NoShrOrUn = raw_data.get('C_NoShrOrUn')
            print "76"
            listing_instance.C_OrigPrice = Decimal(raw_data.get('C_OrigPrice')) if raw_data.get('C_OrigPrice') else raw_data.get('C_OrigPrice')
            print "77"
            listing_instance.C_OthDues = raw_data.get('C_OthDues')
            print "78"
            listing_instance.C_OthDues2 = raw_data.get('C_OthDues2')
            print "79"
            listing_instance.C_PRPRTYCNDT = raw_data.get('C_PRPRTYCNDT')
            print "80"
            listing_instance.C_BOrU = raw_data.get('C_BOrU')
            print "81"
            listing_instance.C_PhotoCount = raw_data.get('C_PhotoCount')
            print "82"
            listing_instance.C_PhotoDate = to_datetime(raw_data.get('C_PhotoDate'), '%Y-%m-%dT%H:%M:%S')
            print "83"
            listing_instance.C_PrcDate = to_datetime(raw_data.get('C_PrcDate'), '%Y-%m-%d')
            print "84"
            listing_instance.C_Remarks = raw_data.get('C_Remarks')
            print "85"
            listing_instance.C_ROOF = raw_data.get('C_ROOF')
            print "86"
            if raw_data.get('C_Setting', None) is not None:
                setting = raw_data.get('C_Setting',None)
                listing_instance.C_Setting = get_create_setting(setting)
                print "87"
            listing_instance.C_SoldPrice = raw_data.get('C_SoldPrice')
            print "88"
            listing_instance.C_SqFeet = raw_data.get('C_SqFeet')
            print "89"
            listing_instance.C_State = raw_data.get('C_State')
            print "90"
            if raw_data.get('C_Status', None) is not None:
                status = raw_data.get('C_Status',None)
                listing_instance.C_Status = get_create_status(status)
                print "91"
            listing_instance.C_StatusCat = raw_data.get('C_StatusCat')
            print "92"
            listing_instance.C_StatDate = to_datetime(raw_data.get('C_StatDate'), '%Y-%m-%d')
            print "93"
            listing_instance.C_StatusDetl = raw_data.get('C_StatusDetl')
            print "94"
            listing_instance.C_Stories = raw_data.get('C_Stories')
            print "95"
            listing_instance.C_SubStatus =raw_data.get('C_SubStatus')
            print "96"
            listing_instance.C_SubArea = raw_data.get('C_SubArea')
            print "97"
            listing_instance.C_TOPOGRAPHY = raw_data.get('C_TOPOGRAPHY')
            print "98"
            listing_instance.C_TaxDBID = raw_data.get('C_TaxDBID')
            print "99"
            listing_instance.C_TaxPropID = raw_data.get('C_TaxPropID')
            print "100"
            listing_instance.C_InitFee = raw_data.get('C_InitFee')
            print "101"
            #C_Type = models.CharField(max_length=105, help_text="Type", null=True, blank=True)

            if raw_data.get('C_Type', None) is not None:
                property_type = raw_data.get('C_Type',None)
                print get_create_type(property_type)
                print 'heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee'
                listing_instance.C_L_Type = get_create_type(property_type)
                print "102"
                #listing_instance.save()
            listing_instance.C_Address2 = raw_data.get('C_Address2')
            print "103"
            listing_instance.C_UpdtDate = datetime.strptime(raw_data.get('C_UpdtDate'), '%Y-%m-%dT%H:%M:%S') if raw_data.get('C_UpdtDate') else raw_data.get('C_UpdtDate')
            print "2"
            #listing_instance.C_VIEW = get_create_view(raw_data.get('C_VIEW'))
            listing_instance.C_WATER = raw_data.get('C_WATER')
            print "104"
            listing_instance.C_WOODSTOVE = raw_data.get('C_WOODSTOVE')
            print "105"
            listing_instance.C_WtrCo = raw_data.get('C_WtrCo')
            print "106"
            listing_instance.C_YrBlt = raw_data.get('C_YrBlt')
            print "107"
            listing_instance.C_Zip = raw_data.get('C_Zip')
            print "108"
            listing_instance.C_Zoning = raw_data.get('C_Zoning')
            print "109"
            listing_instance.C_GeoLat = raw_data.get('C_GeoLat')
            print "110"
            listing_instance.C_GeoLong = raw_data.get('C_GeoLong')
            print "111"
            listing_instance.C_SaleOrRent = raw_data.get('C_SaleOrRent')
            print "112"
            listing_instance.C_MRFee = raw_data.get('C_MRFee')
            print "113"
            assoc_community_city(listing_instance.C_Community, listing_instance.C_City)
            print "114"
            assoc_community_area(listing_instance.C_Community, listing_instance.C_Area)
            print 'Saving listing instance ...............'
            listing_instance.save()
            print 'Listing Saved Successfully....................'

            listing_instance.create_thumbnails()

            if raw_data.get('C_HOAMENITIS', None) is not None:
                hoa_objects = get_create_hoa_amenities(raw_data.get('C_HOAMENITIS', None))
                for hoa in hoa_objects:
                    listing_instance.C_HOAMENITIS.add(hoa)

            if raw_data.get('C_WTRFRNTMNT', None) is not None:
                water_objects = get_create_water_amenities(raw_data.get('C_WTRFRNTMNT', None))
                for water in water_objects:
                    listing_instance.C_WTRFRNTMNT.add(water)

            if raw_data.get('C_VIEW', None) is not None:
                view_objects = get_create_view(raw_data.get('C_VIEW', None))
                for view in view_objects:
                    listing_instance.C_VIEW.add(view)
        except Exception as ex:
            print 'HEREEEEEEEEEEEEEEEEEEEEEEEE'
            print '\n'
            print ex
            print '\n'
            print 'HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE'

    
    def create_price_change_record(self, listing,raw_data):
        """
        creates PriceChange record if there has been changes in the property C_AskPrice field
        """
        if Decimal(raw_data.get('C_AskPrice')) != listing.C_AskPrice:
            price_change = PriceChange(listing=listing,
                                       prev_price=Decimal(listing.C_AskPrice),
                                       new_price=Decimal(raw_data.get('C_AskPrice')))
            price_change.save()

        return

