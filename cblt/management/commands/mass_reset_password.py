from datetime import datetime

from django.conf import settings

from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.models import User
from django.contrib.sites.models import Site

from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from django.core.urlresolvers import reverse

from django.utils.html import strip_tags
from django.utils.http import int_to_base36
from django.utils.translation import ugettext_lazy as _

from django.template.loader import render_to_string

from mailer import send_html_mail

class Command(NoArgsCommand):
    help = 'Sends mass password reset to all users'

    def handle_noargs(self, **options):
        all_users = User.objects.all()
        token_generator = default_token_generator
        subject = _(u'[CBLakeTahoe] Password Reset for New Website')
        template_html = 'email/password_reset_email.html'
        template_text = strip_tags('email/password_reset_email.html')
        from_email = settings.EMAIL_HOST_USER

        for user in all_users:
            temp_key = token_generator.make_token(user)
            current_site = Site.objects.get_current()
            to = user.email
            # send the password reset email
            path = reverse("account_reset_password_from_key",
                           kwargs=dict(uidb36=int_to_base36(user.id),
                                       key=temp_key))
            url = 'http://%s%s' % (current_site.domain,
                                   path)
            context = { "site": current_site,
                        "user": user,
                        "password_reset_url": url }

            text_content = render_to_string(template_text, context)
            html_content = render_to_string(template_html, context)

            if isinstance(to, list):
                send_html_mail(subject, text_content, html_content, from_email, to, account=0)
            else:
                send_html_mail(subject, text_content, html_content, from_email, [to], account=0)
