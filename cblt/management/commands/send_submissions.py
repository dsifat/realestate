from django.core.management.base import BaseCommand, CommandError, NoArgsCommand

from newsletter.models import Submission

class Command(NoArgsCommand):
    help = 'Sends newsletter submissions'

    def handle_noargs(self, **options):
        submissions = Submission.objects.filter(sent=False)
        for submission in submissions:
            submission.submit()
