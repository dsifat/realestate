from datetime import datetime

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand, CommandError, NoArgsCommand
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.utils.html import strip_tags
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string

from subscription.models import Subscription

from newsletter.models import Message, Article, Submission, Newsletter

from mls.models import Listing, PriceChange, Community

from mailer import send_html_mail

class Command(NoArgsCommand):
    help = 'Generates monthly newsletter email'

    def handle_noargs(self, **options):
        today = datetime.now()
        subject = _(u'[CBLakeTahoe] Monthly Real Estate Report for %s/%s/%s' % (today.month, today.day, today.year))
        subject_slug = "monthly-report-%s-%s-%s" % (today.month, today.day, today.year)
        template_html = 'email/cblt_newsletter.html'

        active_listings = Listing.objects.filter(C_Status__name='ACTIVE')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_LstDate', '-C_AskPrice')[:3]

        sold_listings = Listing.objects.filter(C_Status__name='SOLD')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_CloseDate', '-C_AskPrice')[:3]
        pending_listings = Listing.objects.filter(C_Status__name='PENDING')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_CloseDate', '-C_AskPrice')[:3]
        pending_listings_count = Listing.objects.filter(C_Status__name='PENDING')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_CloseDate', '-C_AskPrice').count()

        market_report = Listing.market_report(sub_type='week')

        site = Site.objects.get(id=1)
        site_url = site.domain
        if not site_url.startswith('http://'):
            site_url = 'http://%s' % site_url

        context = {
                'date': today,
                'active_listings': active_listings,
                'sold_listings': sold_listings,
                'pending_listings': pending_listings,
                'pending_listings_count': pending_listings_count,
                'site_url':site_url,
                'market_report': market_report}


        newsletter = Newsletter.objects.get(slug='monthly-report-report')
        #create new message instance
        ##todo: create methods to create slug and title based on the current frequency
        html_content = render_to_string(template_html, context)
        message = Message(newsletter=newsletter, title=subject, slug=subject_slug, date_create=today, date_modify=today)
        message.save()



        newsletter_html_article = Article(post=message, title="newsletter_html_content", text=html_content)
        newsletter_html_article.save()

        submission = Submission.from_message(message)
        submission.save()
