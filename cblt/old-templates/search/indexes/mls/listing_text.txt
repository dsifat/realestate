{{ object.C_NoBeds }}
{{ object.C_Address }}
{{ object.C_NoBaths1 }}
{{ object.C_Acreage }}
{{ object.C_Area.name }}
{{ object.C_City }}
{{ object.C_MLSNo }}
{{ object.C_LType }}
{{ object.C_AskPrice }}
{{ object.C_Garage }}
{{ object.C_SqFeet }}
{{ object.C_AcresNo }}
{{ object.C_YrBlt }}
{{ object.C_LotDims }}
{{ object.C_Remarks }}
{{ object.C_Addendum }}
{{ object.C_VIEW }}
{{ object.C_Setting }}
{{ object.C_L_Type.name }}
{{ object.C_Community.name }}
{% for p in object.C_HOAMENITIS.all %}
{{ p.name }}
{% endfor %}
{% for p in object.C_WTRFRNTMNT.all %}
{{ p.name }}
{% endfor %}
{% for p in object.C_VIEW.all %}
{{ p.name }}
{% endfor %}
