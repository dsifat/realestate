from django.dispatch import receiver

from allauth.account.signals import user_signed_up
from subscription.models import Subscription


@receiver(user_signed_up)
def handle_post_viewed(sender, **kwargs):
    request = kwargs.get('request')
    user = kwargs.get('user')
    profile = user.profile
    Subscription.objects.create(subscriber=profile,
                                subscription_type='weekly_report')
