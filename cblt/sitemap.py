from django.contrib.sitemaps import Sitemap
from mls.models import Listing, Community, City, Area
from django.core.urlresolvers import reverse

class StaticViewSitemap(Sitemap):
    priority = 1
    changefreq = 'daily'

    def items(self):
        return ['home', 'about', 'contact_us']

    def location(self, item):
        return reverse(item)

class StaticViewSitemap2(Sitemap):
    priority = 0.3
    changefreq = 'never'

    def items(self):
        return ['about', 'contact_us']

    def location(self, item):
        return reverse(item)


class CommunitySitemap(Sitemap):
    changefreq = 'daily'
    priority = 0.9

    def items(self):
        return Community.objects.all()

    def location(self, obj):
        return obj.get_absolute_url()

class CitySitemap(Sitemap):
    changefreq = 'daily'
    priority = 0.7


    def items(self):
        return City.objects.all()

    def location(self, obj):
        return obj.get_absolute_url()

class AreaSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.6

    def items(self):
        return Area.objects.all()

    def location(self, obj):
        return obj.get_absolute_url()


class ActiveListingSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.8

    def items(self):
        return Listing.objects.filter(C_Status__name='ACTIVE')

    def location(self, obj):
        return obj.get_detailed_property_url()


class PendingListingSitemap(Sitemap):
    changefreq = 'monthly'
    priority = 0.7

    def items(self):
        return Listing.objects.filter(C_Status__name='PENDING')

    def location(self, obj):
        return obj.get_detailed_property_url()


class SoldListingSitemap(Sitemap):
    changefreq = 'never'
    priority = 0.5

    def items(self):
        return Listing.objects.filter(C_Status__name='SOLD')

    def location(self, obj):
        return obj.get_detailed_property_url()


sitemaps = {
    'static': StaticViewSitemap,
    'static2': StaticViewSitemap2,
    'real_estate': ActiveListingSitemap,
    'community': CommunitySitemap,
    'city': CitySitemap,
    'sold_real_estate': SoldListingSitemap,
    'under_contract_real_estate': PendingListingSitemap,
}
