from cblt.utils import get_map_details, get_years_of_data
from datetime import datetime
from django.core.mail.message import EmailMultiAlternatives
from django.db.models import Q
from django.http import HttpResponse
from django.utils import simplejson
from django.contrib import messages
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.html import strip_tags
import cblt.mdetect as mdetect
from cblt.forms import ContactUsForm
from mls.models import Listing, SellerComment, PriceChange, SiteContent, BigTable,PropertyManagement,BusinessDirectory
from mailer import send_html_mail


today = datetime.today()


def home(request, template='home3.html', context={}):
    '''
        For CBLakeTahoe HOME PAGE 
    '''

    active = Listing.objects.select_related('C_Community', 'C_City', 'img_set',)\
                            .filter(C_Status__name='ACTIVE')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_LstDate')\
                            .only('C_LstDate', 'C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice','C_Address',
                                      'C_MLSNo', 'C_NoBeds', 'C_NoBaths1', 'C_Garage', 'C_SqFeet', 'C_YrBlt',
                                      'C_AcresNo', 'C_LotDims', 'C_City', 'C_GeoLat', 'C_GeoLong')[:5]

    try:
        price_change = BigTable.objects.get(function='price_reduced', scope='All')
    except BigTable.DoesNotExist:
        price_change = BigTable.objects.none()
    lakefronts = Listing.objects.select_related('C_Community', 'C_City', 'img_set',)\
                        .filter(C_Status__name='ACTIVE', C_Setting__slug='lakefront', \
                                C_AskPrice__gte=2000000).order_by('-C_LstDate')[:6]

    context.update({
        'active': active, 'price_change':price_change,
        'lakefronts': lakefronts, 'modal_should_load':False
        })



    if not request.session.get('modal_has_loaded'):
        request.session['modal_has_loaded'] = True
        context['modal_should_load'] = True

    user_agent = request.META.get("HTTP_USER_AGENT")
    http_accept = request.META.get("HTTP_ACCEPT")

    if user_agent and http_accept:
        agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
        is_tablet = agent.detectTierTablet()
        is_phone = agent.detectTierIphone()
        is_mobile = is_tablet or is_phone or agent.detectMobileQuick()

        if is_mobile:
            context['modal_should_load'] = False
    years_of_data = get_years_of_data()
    context['seo_tags'] = {}
    #context['seo_tags']['description'] = "We custom built this website. Our opinions of the homes, more data/stats, "+ years_of_data +" yrs of sales for comps. A very unique Lake Tahoe real estate website"
    context['seo_tags']['description'] = 'CB Lake Tahoe - more real estate data/stats. A unique resource for Truckee and Lake Tahoe real estate information'
    return render_to_response(template, context, context_instance=RequestContext(request))


def home2(request, template='home3.html', context={}):

    active = Listing.objects.select_related('C_Community', 'C_City', 'img_set',)\
                            .filter(C_Status__name='ACTIVE')\
                            .exclude(Q(C_Community__name='SurroundingArea')
                                    |Q(C_Community__name='Sierra County'))\
                            .order_by('-C_LstDate')\
                            .only('C_LstDate', 'C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice','C_Address',
                                      'C_MLSNo', 'C_NoBeds', 'C_NoBaths1', 'C_Garage', 'C_SqFeet', 'C_YrBlt',
                                      'C_AcresNo', 'C_LotDims', 'C_City', 'C_GeoLat', 'C_GeoLong')[:5]

    price_change = BigTable.objects.get(function='price_reduced', scope='All')
    lakefronts = Listing.objects.select_related('C_Community', 'C_City', 'img_set',)\
                        .filter(C_Status__name='ACTIVE', C_Setting__slug='lakefront', \
                                C_AskPrice__gte=2000000).order_by('-C_LstDate')[:6]

    context.update({
        'active': active, 'price_change':price_change,
        'lakefronts': lakefronts, 'modal_should_load':False
        })



    if not request.session.get('modal_has_loaded'):
        request.session['modal_has_loaded'] = True
        context['modal_should_load'] = True

    user_agent = request.META.get("HTTP_USER_AGENT")
    http_accept = request.META.get("HTTP_ACCEPT")

    if user_agent and http_accept:
        agent = mdetect.UAgentInfo(userAgent=user_agent, httpAccept=http_accept)
        is_tablet = agent.detectTierTablet()
        is_phone = agent.detectTierIphone()
        is_mobile = is_tablet or is_phone or agent.detectMobileQuick()

        if is_mobile:
            context['modal_should_load'] = False

    return render_to_response(template, context, context_instance=RequestContext(request))


def contact_us(request, template='contact_us.html', context={}):
    return render_to_response(template, context, context_instance=RequestContext(request))

def ajax_contact_us(request, context={}):
    if request.is_ajax():
        obj = dict(request.POST)
        form = ContactUsForm(request.POST)
        if form.is_valid():
            cleaned_data = form.cleaned_data
            subject = u'[CBLakeTahoe] Contact Us Submission'
            to = 'aardvark5678@gmail.com'
            from_email = settings.EMAIL_HOST_USER

            context['email'] = cleaned_data['email']
            context['name']  = cleaned_data['name']
            context['phone'] = cleaned_data['phone']
            context['message'] = cleaned_data['message']

            template_html = 'email/contact.html'
            template_text = strip_tags('email/contact.html')
            text_content = render_to_string(template_text, context)
            html_content = render_to_string(template_html, context)
            if isinstance(to, list):
                send_html_mail(subject, text_content, html_content, from_email, to, account=0)
            else:
                send_html_mail(subject, text_content, html_content, from_email, settings.EMAIL_RECIEVER, account=0)
            obj['status'] = True
        else:
            obj = dict(form.errors.items())
            obj['status'] = False
        return HttpResponse(simplejson.dumps(obj), mimetype='text/json')


def about(request, template='about.html', context={}):
    """
        GET about us page
    """
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "We built this Lake Tahoe real estate site from the ground up to be different." \
      "We made sure to deliver more property data. We preview the properties for sale and we post our comments and photos." \
       "We\'re eager to earn your real estate business."
    return render_to_response(template, context, context_instance=RequestContext(request))

def our_opinions(request, template='our-opinions.html', context={}):
    """
        GET out opinion page
    """
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "No fluff, active listings and solds, read our opinions and comments on the Lake Tahoe real estate listings."
    context['reviewed_photos'] = Listing.get_reviewed_listing(max_length=15, review_type='comments')
    return render_to_response(template, context, context_instance=RequestContext(request))

def manage_property(request, template='manage-property.html', context={}):
    """
        GET property management page
    """
    management = PropertyManagement.objects.latest('id')    
    context['management'] = management
    return render_to_response(template, context, context_instance=RequestContext(request))

def list_directory(request, template='laketahoe-directory.html', context={}):
    """
        list_directory    
    """
    context['all_directories'] = BusinessDirectory.objects.all()
    context['introduction_text'] = "From coordinating the ongoing seasonal maintenance for small projects like carpet cleaning and pine needles, to on-site caretakers and property managers, we coordinate and oversee the efforts on your behalf. Eyes and ears on your home year round. Great people, excellent service and a reasonable price."    
    return render_to_response(template, context, context_instance=RequestContext(request))


