from django.views.defaults import page_not_found
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView, RedirectView

from .sitemap import sitemaps

from filebrowser.sites import site



# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'cblt.views.home', name='home'),
    url(r'^beta-home/$', 'cblt.views.home2', name='beta-home'),
    # url(r'^', include('debug_toolbar_htmltidy.urls')),
    url(r'^sitemap\.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps': sitemaps}),
    url(r'^robots\.txt$', TemplateView.as_view(template_name="robots.txt", content_type='text/plain')),
    url(r'^newsletter/', include('newsletter.urls')),
    url(r'^coming-soon$', TemplateView.as_view(template_name='coming-soon.html'), name='coming-soon'),
    url(r'^home2$', 'cblt.views.home2',url(r'^admin/filebrowser/', include(site.urls)), name='home2'),
    url(r'^about-our-website$', 'cblt.views.about', name='about'),
    url(r'^our-opinions', 'cblt.views.our_opinions', name='our_opinions'),
    #Manage Property
    url(r'^manage-properties$', 'cblt.views.manage_property', name='manage_property'),
    url(r'^laketahoe-directory$', 'cblt.views.list_directory', name='laketahoe-directory'),
    url(r'^contact-us$', 'cblt.views.contact_us', name='contact_us'),
    url(r'^ajax-contact-us/$', 'cblt.views.ajax_contact_us'),
    #url(r'^bio/', 'cblt.views.bio', name='bio'),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^blog/(?P<content_id>\d+)/(?P<content_slug>[\w-]+)$', 'mls.views.content_page', name='content_page'),
    url(r'^blog/filter/(?P<location_type_id>\d+)/(?P<location_type_slug>[\w-]+)$', 'mls.views.filter_content', name='filter_content'),
    url(r'^blog$', 'mls.views.blog', name='blog'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^mls/', include('mls.urls')),
    #url(r'^search-mls', TemplateView.as_view(template_name='search_all.html'), name='search_all'),
    #url(r'^detailed-property/(?P<mls_no>\d+)/$', 'detailed_property', name='detailed_property'),
    url(r'^photologue/', include('photologue.urls')),

    #realestate new urls
    url(r'^realestate/search$', 'mls.views.search_result', name='search_result'),
    url(r'^realestate/explore$', 'mls.views.explore', name='explore'),
    url(r'^realestate/statistics$', 'mls.views.statistics', name='statistics'),
    url(r'^realestate/price-reductions$', 'mls.views.price_reductions', name='price_reductions'),
    url(r'^realestate/sold', 'mls.views.sold', name='sold'),
    url(r'^realestate/under-contract', 'mls.views.under_contract', name='under_contract'),
    url(r'^realestate/communities$', 'mls.views.communities', name='realestate_communities'),
    url(r'^realestate/city$', 'mls.views.cities', name='realestate_cities'),
    url(r'^realestate/neighborhood$', 'mls.views.neighborhood', name='realestate_neighborhood'),
    url(r'^realestate/latest-listings$', 'mls.views.latest_listings', name='latest_listings'),
    # url(r'^realestate/listing/(?P<mls_no>\d+)/edit$', 'mls.views.edit_redirect', name='detailed_property_edit'),
    url(r'^realestate/listing/(?P<mls_no>\d+)$', 'mls.views.detailed_property_redirect'),
    url(r'^realestate/city/(?P<city_slug>[\w-]+)$', 'mls.views.view_city', name='view_city'),
    url(r'^realestate/neighborhood/(?P<area_slug>[\w-]+)$', 'mls.views.view_area', name='view_area'),
    url(r'^realestate/setting/(?P<setting_slug>[\w-]+)$', 'mls.views.view_setting', name='view_setting'),
    url(r'^realestate/(?P<community_slug>[\w-]+)$', 'mls.views.view_community', name='view_community'),
    url(r'^realestate/(?P<community_slug>[\w-]+)/(?P<area_slug>[\w-]+)/mls-(?P<mls_no>\d+)-(?P<address_slug>[\w-]+)$', 'mls.views.detailed_property', name='detailed_property'),
    url(r'^realestate/(?P<community_slug>[\w-]+)/(?P<area_slug>[\w-]+)/(?P<mls_no>\d+)/edit$', 'mls.views.edit_redirect', name='detailed_property_edit'),
    url(r'^realestate/(?P<community_slug>[\w-]+)/(?P<area_slug>[\w-]+)/(?P<mls_no>\d+)$', 'mls.views.property_redirect'),
    url(r'^lakefronts$', RedirectView.as_view(url='/realestate/setting/lakefront')),
    url(r'^(?P<mls_no>\d+)$', 'mls.views.detailed_property_redirect_2'),
    url(r'^(?P<fuzzy_slug>[\w-]+)$', 'mls.views.fuzzy_redirect', name='fuzzy_redirect'),

    #Vendor 
    url(r'^realestate/vendors/(?P<pk>\d+)$', 'mls.views.detailed_vendor', name="detailed_vendor"),
    url(r'^realestate/related-contents/$', 'mls.views.property_related_content', name="related_contents"),
    url(r'^404/$', 'django.views.defaults.page_not_found', ),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )
