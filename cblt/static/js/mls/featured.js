$(document).ready(function(){



});

var community_map;
function initializeCommunityMap() {
    if($('#map_full_canvas').length === 0)
        return;
    var myOptions = {
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };
    community_map = new google.maps.Map(document.getElementById('map_full_canvas'), myOptions);
    var bounds = new google.maps.LatLngBounds();
    var homeinfowindow = new google.maps.InfoWindow();

    $.each(listing_plots, function(key, value) {
        var latlng = new google.maps.LatLng(value['lat'], value['lng']);
        bounds.extend(latlng);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(value['lat'], value['lng']),
            map: community_map,
            icon: '/static/css/images/marker.png',
            animation: google.maps.Animation.DROP,
            scrollwheel: true,
            streetViewControl:true,
            title: value['title']
        });
        var link = "link";
        google.maps.event.addListener( marker, 'click', function() {
            // Setting the content of the InfoWindow
            var content = '<div id="info" class="span3"><div class="row">' + '<div class="span3"><h5>' + value['title'] + '</h5><h6>' + value['address'] + '</h6>' + '<strong>&dollar;' + value['price'] + '</strong>' + '<br/><a href="/realeastate/listing/' + value['mls_no'] + '">View >></a>' + '</div></div></div>';
            homeinfowindow.setContent(content);
            homeinfowindow.open(community_map, marker);
        });

    });

    community_map.fitBounds(bounds);


}

google.maps.event.addDomListener(window, 'load', initializeCommunityMap);
