var extra_markers = {};

$(document).ready(function(){
    if($('#showcase').length == 1) {
        $('#showcase-loader').hide();
        $('.showcase').show();
        $("#showcase").awShowcase({
            content_width:      620,
            content_height:     410,
            fit_to_parent:      false,
            auto:         false,
            interval:       3000,
            continuous:       false,
            loading:        true,
            tooltip_width:      200,
            tooltip_icon_width:   32,
            tooltip_icon_height:  32,
            tooltip_offsetx:    18,
            tooltip_offsety:    0,
            arrows:         true,
            buttons:        false,
            btn_numbers:      false,
            keybord_keys:     true,
            mousetrace:       false, /* Trace x and y coordinates for the mouse */
            pauseonover:      true,
            stoponclick:      true,
            transition:       'hslide', /* hslide/vslide/fade */
            transition_delay:   300,
            transition_speed:   500,
            show_caption:     'onhover', /* onload/onhover/show */
            thumbnails:       true,
            thumbnails_position:  'outside-last', /* outside-last/outside-first/inside-last/inside-first */
            thumbnails_direction: 'horizontal', /* vertical/horizontal */
            thumbnails_slidex:    0, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
            dynamic_height:     false, /* For dynamic height to work in webkit you need to set the width and height of js/jquery.aw-showcase/images in the source. Usually works to only set the dimension of the first slide in the showcase. */
            speed_change:     true, /* Set to true to prevent users from swithing more then one slide at once. */
            viewline:       false /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of js/jquery.aw-showcase/images in the source. */
        });

        $("#showcase").hover(function(){
          $('.showcase-arrow-previous, .showcase-arrow-next').fadeIn();
        },
        function () {
          $('.showcase-arrow-previous, .showcase-arrow-next').fadeOut();
        });
    }

    if($('#carousel').length == 1) {
        $('#carousel-loader').hide();
        $('.showcase').show();
        $("#carousel").awShowcase({
            content_width:      590,
            content_height:     326,
            fit_to_parent:      false,
            auto:         true,
            interval:       3000,
            continuous:       true,
            loading:        true,
            tooltip_width:      200,
            tooltip_icon_width:   32,
            tooltip_icon_height:  32,
            tooltip_offsetx:    18,
            tooltip_offsety:    0,
            arrows:         true,
            buttons:        false,
            btn_numbers:      false,
            keybord_keys:     true,
            mousetrace:       false, /* Trace x and y coordinates for the mouse */
            pauseonover:      true,
            stoponclick:      true,
            transition:       'hslide', /* hslide/vslide/fade */
            transition_delay:   300,
            transition_speed:   500,
            show_caption:     'show', /* onload/onhover/show */
            thumbnails:       false,
            thumbnails_position:  'outside-last', /* outside-last/outside-first/inside-last/inside-first */
            thumbnails_direction: 'horizontal', /* vertical/horizontal */
            thumbnails_slidex:    0, /* 0 = auto / 1 = slide one thumbnail / 2 = slide two thumbnails / etc. */
            dynamic_height:     false, /* For dynamic height to work in webkit you need to set the width and height of js/jquery.aw-showcase/images in the source. Usually works to only set the dimension of the first slide in the showcase. */
            speed_change:     true, /* Set to true to prevent users from swithing more then one slide at once. */
            viewline:       false /* If set to true content_width, thumbnails, transition and dynamic_height will be disabled. As for dynamic height you need to set the width and height of js/jquery.aw-showcase/images in the source. */
        });

        $("#carousel").hover(function () {
          $('.showcase-arrow-previous, .showcase-arrow-next').fadeIn();
        },
        function () {
          $('.showcase-arrow-previous, .showcase-arrow-next').fadeOut();
        });
    }

});

$('#community').change(function(){
        if($(this).val()!='all'){
            window.location = $(this).val();
        }
        else{
            window.location = '/mls/search/community/';
        }
});

$('#area').change(function(){
        if($(this).val()!='all'){
            window.location = $(this).val();
        }
        else{
            window.location = '/mls/search/area/';
        }
});

$('#city').change(function(){
        if($(this).val()!='all'){
            window.location = $(this).val();
        }
        else{
            window.location = '/mls/search/city/';
        }
});

$('#setting').change(function(){
        if($(this).val()!='all'){
            window.location = $(this).val();
        }
        else{
            window.location = '/mls/search/setting/';
        }
});

$('input[name="type"]').live('click', function(event){
    $('#filter_form').submit();
});

$('input[name="setting"]').live('click', function(event){
    $('input[name="all"]').removeAttr('checked');
    $('#filter_form').submit();
});

$('input[name="all"]').live('click', function(event){
    $('input[name="setting"]').each(function(){
        $(this).prop('checked', false);
    });
    $('#filter_form').submit();
});

$('#pricerangemin, #pricerangemax').live('change', function(event){
    $('#filter_form').submit();
});

try{
    var map_locs = properties_plot;
}
catch(e){
    var map_locs = [];
}
var community_map;
function initializeCommunityMap() {
    if($('#map_canvas').length === 0)
        return;
    var myOptions = {
        mapTypeId: google.maps.MapTypeId.HYBRID
    };
    var markers = [];
    var bounds = new google.maps.LatLngBounds();
    community_map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
    var homeinfowindow = new google.maps.InfoWindow();
    $.each(map_locs, function(key, value) {
        var latlng = new google.maps.LatLng(value['lat'], value['lng']);
        bounds.extend(latlng);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(value['lat'], value['lng']),
            map: community_map,
            icon: '/static/css/images/marker.png',
            animation: google.maps.Animation.DROP,
            scrollwheel: true,
            streetViewControl:true,
            title: value['title']
        });
        var link = "link";
        google.maps.event.addListener( marker, 'click', function() {
            // Setting the content of the InfoWindow
            var content = '<div id="info" class="span4"><div class="row">' +
                            '<div class="span2"><img src="'+ value['image'] + '" class="thumbnail" style="width:135px"/><h5>MLS No.: ' + value['mls_no'] + '</h5><h5> Address: <a style="font-size:12px;" href="/realestate/listing/'+ value['mls_no'] + '">' + value['address']+'</a></h5></div>' +
                            '<div class="span2"><h5>Price: &dollar;' + value['price'] + '</h5>' +
                            '<h5>B/B/G: '+ value['beds']+'/'+value['baths']+ '/' + value['garage']+'</h5>' +
                            '<h5>Sq Ft: '+ value['sqft']+'</h5><h5>Year: '+ value['yr_built']+'</h5>'+
                            '<h5>Acreage: '+ value['acreage'] +'</h5><h5> Lot Size: '+ value['lotsize']+'</h5>'+
                            '<h5><a href="/realestate/listing/' + value['mls_no'] + '">View >></a></h5></div></div></div>';
            homeinfowindow.setContent(content);
            homeinfowindow.open(community_map, marker);
        });
        markers.push(marker);

    });
    if(typeof plottable_content_json != 'undefined'){
        $.each(plottable_content_json, function(key, value) {
            var latlng = new google.maps.LatLng(value['latitude'], value['longitude']);
            bounds.extend(latlng);
            var marker2 = new google.maps.Marker({
                position: new google.maps.LatLng(value['latitude'], value['longitude']),
                map: community_map,
                icon: 'http://www.google.com/mapfiles/dd-start.png',
                animation: google.maps.Animation.DROP,
                scrollwheel: true,
                streetViewControl:true,
                title: value['title']
            });
            var link = "link";
            google.maps.event.addListener( marker2, 'click', function() {
                // Setting the content of the InfoWindow
                var content = '<div id="info" class="span3"><div class="row">' + '<div class="span3"><h5>' + value['title'] + '</h5><h6>' + value['short_desc'] + '</h6>' + '<br/></div></div></div>';
                homeinfowindow.setContent(content);
                homeinfowindow.open(community_map, marker2);
            });
            extra_markers[key] = marker2;
            extra_markers[key].setMap(null);
            //markers.push(marker2);
        });
    }
    var markerCluster = new MarkerClusterer(community_map, markers);
    community_map.fitBounds(bounds);

}

google.maps.event.addDomListener(window, 'load', initializeCommunityMap);

$('#related_content').live('change', function(event){
    event.preventDefault();
    if($(this).is(':checked')){
        $.each(extra_markers, function(key, value){
            value.setMap(community_map);
        });
    }
    else {
        $.each(extra_markers, function(key, value){
            value.setMap(null);
        });
    }
});
