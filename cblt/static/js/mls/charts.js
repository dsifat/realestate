$(document).ready(function(){
    create_active_charts(active_listing);
    create_sales_chart(previous_sales_summary, current_sales_summary);
    /*create_price_range_chart(sales_by_price);*/
    create_pending_sales(pending_sales);
});

function create_pending_sales(pending_sales){
    var pending_residence_data = [];
    var pending_sub_status_data = [];
    var pending_sold_price_range_data = [];
    var pending_community_data = [];
    var pending_sold_dom_data = [];

    $.each(pending_sales['residence_type'], function(key, value){
        pending_residence_data.push([key, value]);
    });

    $.each(pending_sales['sub_status'], function(key, value){
        pending_sub_status_data.push([key, value]);
    });

    $.each(pending_sales['sold_price_range'], function(key, value){
        pending_sold_price_range_data.push([key, value]);
    });

    $.each(pending_sales['community'], function(key, value){
        pending_community_data.push([key, value]);
    });

    $.each(pending_sales['sold_dom'], function(key, value){
        pending_sold_dom_data.push([key, value]);
    });

    var pending_residence_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: pending_residence_data
    }];

    var pending_sub_status_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: pending_sub_status_data
    }];

    var pending_sold_price_range_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: pending_sold_price_range_data
    }];

    var pending_community_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: pending_community_data
    }];

    var pending_sold_dom_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: pending_sold_dom_data
    }];


    create_pie("#pending-sales-residence", 'Pending Sales by Property Type', pending_residence_series);
    create_pie("#pending-substat", 'Pending Sales by Sale Type', pending_sub_status_series);
    create_pie("#pending-sold-price-range", 'Pending Sales by Price Range', pending_sold_price_range_series);
    create_pie("#pending-community", 'Pending Sales by Community', pending_community_series);
    create_pie("#pending-sold-dom", 'Pending Sales by Days on Market', pending_sold_dom_series);
}

function create_price_range_chart(sales_data){
    price_range_data = [];

    $.each(sales_data['data'], function(key, value){
        price_range_data.push([key, value]);
    });

    price_range_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: price_range_data
    }];

    create_pie("#sales-price-range", 'Sales by Price Range', price_range_series);
}


function create_active_charts(json_obj){
    var residential_type_data = [];
    var sub_status_data = [];
    var community_data = [];
    var price_range_data = [];
    var dom_data = [];

    $.each(json_obj.residence_type.data, function(key, value){
        residential_type_data.push([key, value]);
    });

    $.each(json_obj.price_range.data, function(key, value){
        price_range_data.push([key, value]);
    });

    $.each(json_obj.community.data, function(key, value){
        community_data.push([key, value]);
    });

    $.each(json_obj.sub_status.data, function(key, value){
        sub_status_data.push([key, value]);
    });

    $.each(json_obj.dom.data, function(key, value){
        dom_data.push([key, value]);
    });

    residential_type_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: residential_type_data
    }];

    price_range_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: price_range_data
    }];

    community_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: community_data
    }];

    sub_status_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: sub_status_data
    }];

    dom_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: dom_data
    }];

    create_pie("#active-residential", json_obj.residence_type.title, residential_type_series);
    create_pie("#active-price-range", json_obj.price_range.title, price_range_series);
    create_pie("#active-community", json_obj.community.title, community_series);
    create_pie("#active-substat", json_obj.sub_status.title, sub_status_series);
    create_pie("#active-dom", json_obj.dom.title, dom_series);
}

function create_sales_chart(previous_sales, current_sales){
    var series_data = create_series_data(previous_sales, current_sales);
    var categories = [previous_sales.date, current_sales.date];

    var price_range_data = [];
    var community_data = [];
    var dom_data = [];
    var financing_data = [];

    $.each(current_sales.price_range.data, function(key, value){
        price_range_data.push([key, value]);
    });

    $.each(current_sales.community.data, function(key, value){
        community_data.push([key, value]);
    });

    $.each(current_sales.dom.data, function(key, value){
        dom_data.push([key, value]);
    });

    $.each(current_sales.financing_type.data, function(key, value){
        financing_data.push([key, value]);
    });

    price_range_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: price_range_data
    }];

    community_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: community_data
    }];

    dom_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: dom_data
    }];

    financing_series = [{
        type: 'pie',
        name: 'Inventory Share',
        data: financing_data
    }];

    create_comparative_bar("#comparative-sales", "Yearly Sales Report", categories, series_data);
    create_pie("#sold-price-range", current_sales.price_range.title, price_range_series);
    create_pie("#sold-community", current_sales.community.title, community_series);
    create_pie("#sold-dom", current_sales.dom.title, dom_series);
    create_pie("#sold-financing", current_sales.financing_type.title, financing_series);
}

function create_series_data(previous_sales, current_sales){
    var series = [];
    var series_dict = {};

    $.each(previous_sales['residence_type'], function(key, value){
        series_dict[key] = {
            'name':key,
            'data':[value],
            'stack':'residence_type'
        };
    });

    $.each(current_sales['residence_type'], function(key, value){
        series_dict[key]['data'].push(value);
    });

    $.each(previous_sales['sub_status'], function(key, value){
        series_dict[key] = {
            'name':key,
            'data':[value],
            'stack':'sub_status'
        };
    });

    $.each(current_sales['sub_status'], function(key, value){
        series_dict[key]['data'].push(value);
    });

    $.each(series_dict, function(key, value){
        series.push(value);
    });

    return series;

}

function create_pie(div_target, title, series){
    $(div_target).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: title
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                }
            }
        },
        series: series
    });
}

function create_comparative_bar(div_target, title, categories, series_data){

    $(div_target).highcharts({

                chart: {
                    type: 'column'
                },

                title: {
                    text: title
                },

                xAxis: {
                    categories: categories
                },

                yAxis: {
                    allowDecimals: false,
                    min: 0,
                    title: {
                        text: 'Number of Sales'
                    }
                },

                tooltip: {
                    formatter: function() {
                        return '<b>'+ this.x +'</b><br/>'+
                            this.series.name +': '+ this.y +'<br/>'+
                            'Total: '+ this.point.stackTotal;
                    }
                },

                plotOptions: {
                    column: {
                        stacking: 'normal'
                    }
                },
/*                RESIDENCE_TYPES = ['Boat Slip', 'Mobile Home', 'Single Family', 'Condominium/Townhouse',
                                   'Share Ownership']*/
/*                SUB_STATUS = ['Standard', 'REO', 'Short Sale']*/
                series: series_data
            });
}
