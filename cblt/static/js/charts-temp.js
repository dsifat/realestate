;!function($) {
    var charts = project1.utils.namespace('project1.charts');

    charts.Pie = function() {
        this.publicVar = 'x';
        this.series = [];

        this.render = function($targetDiv) {

        };

        this.create_series = function(piedata) {
            
        }


        this.create_series = function(previous_sales, current_sales) {
            var series = []
            var series_dict = {}

            $.each(previous_sales['residence_type'], function(key, value){
                series_dict[key] = {
                    'name':key,
                    'data':[value],
                    'stack':'residence_type'
                };
            });

            $.each(current_sales['residence_type'], function(key, value){
                series_dict[key]['data'].push(value);
            });

            $.each(previous_sales['sub_status'], function(key, value){
                series_dict[key] = {
                    'name':key,
                    'data':[value],
                    'stack':'sub_status'
                };
            });

            $.each(current_sales['sub_status'], function(key, value){
                series_dict[key]['data'].push(value);
            });

            $.each(series_dict, function(key, value){
                series.push(value);
            });

            return series;
        }

    }

    charts.Bar = function(){

    }
}(jQuery);

var PieChartClass = project1.charts.Pie;
var pie = new PieChartClass();