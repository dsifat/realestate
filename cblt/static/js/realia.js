$(document).ready(function() {
	InitCarousel();
    InitPropertyCarousel();
    InitCarouselHome();
	InitOffCanvasNavigation();
	InitMap();
	InitChosen();
	InitEzmark();
/*	InitPriceSlider(); commented out and function definition removed*/
	InitImageSlider();
	InitAccordion();
	InitTabs();
    InitPalette();

/*Contact Form action bindings*/
    $('#contactUsSubmit').on('click', function(event){
        event.preventDefault();
        var form = $('.contact-form');
        var url = '/ajax-contact-us/';
        $.ajax({
            url: url,
            type: 'post',
            data: form.serialize(),
            cache: false,
            beforeSend: function(data){
                form.find('input').removeClass('error');
                form.find('textarea').removeClass('error');
                $('#error_message').html('');
            },
            success: function(data){
                if(data.status===true){
                    $('#error_message').html('');
                    $('#error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                    $.each(data, function(index, value) {
                        $('#id_' + index).val('');
                    });
                }
                else{
                    processErrors(form, data);
                }
            }
        });
    });

    $('#propertySubmit').on('click', function(event){
        event.preventDefault();
        var form = $('#property-contact-form');
        var url = '/mls/ajax-contact-us/';
        $.ajax({
            url: url,
            type: 'post',
            data: form.serialize(),
            cache: false,
            beforeSend: function(data){
                form.find('input').removeClass('error');
                form.find('textarea').removeClass('error');
                $('#error_message').html('');
            },
            success: function(data){
                if(data.status===true){
                    $('#error_message').html('');
                    $('#error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                    $.each(data, function(index, value) {
                        $('#id_' + index).val('');
                    });
                }
                else{
                    processErrors(form, data);
                }
            }
        });
    });

    $('#footerContactSubmit').on('click', function(event){
        event.preventDefault();
        var form = $('#footer-contact');
        var url = '/ajax-contact-us/';
        $.ajax({
            url: url,
            type: 'post',
            data: form.serialize(),
            cache: false,
            beforeSend: function(data){
                form.find('input').removeClass('error');
                form.find('textarea').removeClass('error');
                $('#footer_error_message').html('');
            },
            success: function(data){
                if(data.status===true){
                    $('#footer_error_message').html('');
                    $('#footer_error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                    $.each(data, function(index, value) {
                        $('#id_footer_' + index).val('');
                    });
                }
                else{
                    processFooterErrors(form, data);
                }
            }
        });
    });

});

/* Contact Form Functions*/
function processErrors(form, data){
    var error = "<div class='alert alert-error' id='error_message'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Oh snap!</strong> Change a few things up and try submitting again.";
    $.each(data, function(index, value) {
        $('#id_' + index).addClass('error');
        if (index != 'status'){
            error += '<li>' + index.toString().capFirst() + ' - ' + value + '</li>';
        }
    });
    error += '</ul></div>';
    $('#error_message').html(error);
}

function processFooterErrors(form, data){
    var error = "<div class='alert alert-error' id='footer_error_message'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Oh snap!</strong> Change a few things up and try submitting again.";
    $.each(data, function(index, value) {
        $('#id_footer_' + index).addClass('error');
        if (index != 'status'){
            error += '<li>' + index.toString().capFirst() + ' - ' + value + '</li>';
        }
    });
    error += '</ul></div>';
    $('#footer_error_message').html(error);
}

/*Initializations*/
function InitPalette() {
    if ($.cookie('palette') == 'true') {
        $('.palette').addClass('open');
    }

    $('.palette .toggle a').on({
        click: function(e) {
            e.preventDefault();
            var palette = $(this).closest('.palette');

            if (palette.hasClass('open')) {
                palette.animate({'left': '-195'}, 500, function() {
                    palette.removeClass('open');
                });
                $.cookie('palette', false)
            } else {
                palette.animate({'left': '0'}, 500, function() {
                    palette.addClass('open');
                    $.cookie('palette', true);
                });
            }
        }
    });

    if ($.cookie('color-variant')) {
        var link = $('.palette').find('a.'+ $.cookie('color-variant'));
        var file = link.attr('href');
        var klass = link.attr('class');

        $('#color-variant').attr('href', file);
        $('body').addClass(klass);
    }

    if ($.cookie('pattern')) {
        $('body').addClass($.cookie('pattern'));
    }

    if ($.cookie('header')) {
        $('body').addClass($.cookie('header'));
    }

    $('.palette a').on({
        click: function(e) {
            e.preventDefault();

            // Colors
            if ($(this).closest('div').hasClass('colors')) {
                var file = $(this).attr('href');
                var klass = $(this).attr('class');
                $('#color-variant').attr('href', file);

                if (!$('body').hasClass(klass)) {
                    $('body').removeClass($.cookie('color-variant'));
                    $('body').addClass(klass);
                }
                $.cookie('color-variant', klass)
            }
            // Patterns
            else if ($(this).closest('div').hasClass('patterns')) {
                var klass = $(this).attr('class');

                if (!$('body').hasClass(klass)) {
                    $('body').removeClass($.cookie('pattern'));
                    $('body').addClass(klass);
                    $.cookie('pattern', klass);
                }
            }
            // Headers
            else if ($(this).closest('div').hasClass('headers')) {
                var klass = $(this).attr('class');

                if (!$('body').hasClass(klass)) {
                    $('body').removeClass($.cookie('header'));
                    $('body').addClass(klass);
                    $.cookie('header', klass);
                }
            }
        }
    });
    $('.palette .reset').on({
        click: function() {
            $('body').removeClass($.cookie('color-variant'));
            $('#color-variant').attr('href', null);
            $.removeCookie('color-variant');

            $('body').removeClass($.cookie('pattern'));
            $.removeCookie('pattern');

            $('body').removeClass($.cookie('header'));
            $.removeCookie('header');
        }
    })
}

function InitPropertyCarousel() {
    $('.carousel.property .content li img').on({
        click: function(e) {
            var src = $(this).attr('src');
            var img = $(this).closest('.carousel.property').find('.preview img');
            img.attr('src', src);
            $('.carousel.property .content li').each(function() {
                $(this).removeClass('active');
            });
            $(this).closest('li').addClass('active');
        }
    })
}

function InitTabs() {
	$('.tabs a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show');
	});
}

function InitImageSlider() {
	$('.iosSlider').iosSlider({
		desktopClickDrag: true,
		snapToChildren: true,
		infiniteSlider: true,
		navSlideSelector: '.slider .navigation li',
		onSlideComplete: function(args) {
			if(!args.slideChanged) return false;

			$(args.sliderObject).find('.slider-info').attr('style', '');

			$(args.currentSlideObject).find('.slider-info').animate({
				left: '15px',
				opacity: '.9'
			}, 'easeOutQuint');
		},
		onSliderLoaded: function(args) {
			$(args.sliderObject).find('.slider-info').attr('style', '');

			$(args.currentSlideObject).find('.slider-info').animate({
				left: '15px',
				opacity: '.9'
			}, 'easeOutQuint');
		},
		onSlideChange: function(args) {
			$('.slider .navigation li').removeClass('active');
			$('.slider .navigation li:eq(' + (args.currentSlideNumber - 1) + ')').addClass('active');
		},
		autoSlide: true,
		scrollbar: true,
		scrollbarContainer: '.sliderContainer .scrollbarContainer',
		scrollbarMargin: '0',
		scrollbarBorderRadius: '0',
		keyboardControls: true
	});
}

function InitAccordion() {
    $('.accordion').on('show', function (e) {
        $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('active');
    });

    $('.accordion').on('hide', function (e) {
        $(this).find('.accordion-toggle').not($(e.target)).removeClass('active');
    });
}


function InitEzmark() {
	$('input[type="checkbox"]').ezMark();
	$('input[type="radio"]').ezMark();
}

function InitChosen() {
	$('select').chosen({
		disable_search_threshold: 10,
        max_selected_options: 4
	});
    $('.chzn-search').hide();
}

function InitOffCanvasNavigation() {
	$('#btn-nav').on({
		click: function() {
			$('body').toggleClass('nav-open');
		}
	});
}

function InitCarousel() {
	$('#main .carousel .content ul').carouFredSel({
		scroll: {
			items: 1,
            fx: 'scroll',
            duration: 500
		},
		auto: false,
		next: {
            items: 1,
    		button: '#main .carousel .carousel-next',
    		key: 'right',
            onBefore: function(data){
                current_active = $('#main .carousel .content ul li.active');
                next_active = $(current_active).next();
                $(current_active).removeClass('active');
                $(next_active).addClass('active');
                $(next_active).find('img').trigger('mouseup');
                $('.preview img').fadeOut(500, function() {
                        $('.preview img').attr("src",$(next_active).find('img').attr('src'));
                }).fadeIn(500);
            }
		},
		prev: {
            items: 1,
    		button: '#main .carousel .carousel-prev',
    		key: 'left',
            onBefore: function(data){
                current_active = $('#main .carousel .content ul li.active');
                next_active = $(current_active).prev();
                $(current_active).removeClass('active');
                $(next_active).addClass('active');
                $(next_active).find('img').trigger('mouseup');

                $('.preview img').fadeOut(500, function() {
                        $('.preview img').attr("src",$(next_active).find('img').attr('src'));
                    }).fadeIn(500);
            }
		}
	});

	$('.carousel-wrapper .content ul').carouFredSel({
		scroll: {
			items: 1,
            fx: 'scroll',
            duration: 500
		},
		auto: false,
		next: {
            items: 1,
    		button: '.carousel-wrapper .carousel-next',
    		key: 'right'
		},
		prev: {
            items: 1,
    		button: '.carousel-wrapper .carousel-prev',
    		key: 'left'
		}
	});
}

function InitCarouselHome() {
    $('#main .carousel-home .content ul').carouFredSel({
        scroll: {
            items: 1,
            fx: 'scroll',
            duration: 500
        },
        auto: false,
        next: {
            items: 1,
            button: '#main .carousel-home .carousel-next',
            key: 'right',
            onBefore: function(data){
                current_active = $('#main .carousel-home .content ul li.active');
                next_active = $(current_active).next();
                $(current_active).removeClass('active');
                $(next_active).addClass('active');
                $(next_active).find('img').trigger('mouseup');
                $('.preview img').fadeOut(500, function() {
                        $('.preview img').attr("src",$(next_active).find('img').attr('src'));
                }).fadeIn(500);
            }
        },
        prev: {
            items: 1,
            button: '#main .carousel-home .carousel-prev',
            key: 'left',
            onBefore: function(data){
                current_active = $('#main .carousel-home .content ul li.active');
                next_active = $(current_active).prev();
                $(current_active).removeClass('active');
                $(next_active).addClass('active');
                $(next_active).find('img').trigger('mouseup');

                $('.preview img').fadeOut(500, function() {
                        $('.preview img').attr("src",$(next_active).find('img').attr('src'));
                    }).fadeIn(500);
            }
        }
    });

    $('.carousel-wrapper .content ul').carouFredSel({
        scroll: {
            items: 1,
            fx: 'scroll',
            duration: 500
        },
        auto: false,
        next: {
            items: 1,
            button: '.carousel-wrapper .carousel-next',
            key: 'right'
        },
        prev: {
            items: 1,
            button: '.carousel-wrapper .carousel-prev',
            key: 'left'
        }
    });
}

function InitMap() {
	//google.maps.event.addDomListener(window, 'load', LoadMap);
    //google.maps.event.addDomListener(window, 'load', LoadMapProperty);
}
