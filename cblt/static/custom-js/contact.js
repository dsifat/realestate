function processErrors(form, data){
    var error = "<div class='alert alert-error' id='error_message'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Oh snap!</strong> Change a few things up and try submitting again.";
    $.each(data, function(index, value) {
        $('#id_' + index).addClass('error');
        if (index != 'status'){
            error += '<li>' + index.toString().capFirst() + ' - ' + value + '</li>';
        }
    });
    error += '</ul></div>';
    $('#error_message').html(error);
}

function processFooterErrors(form, data){
    var error = "<div class='alert alert-error' id='footer_error_message'><button type='button' class='close' data-dismiss='alert'>×</button><strong>Oh snap!</strong> Change a few things up and try submitting again.";
    $.each(data, function(index, value) {
        $('#id_footer_' + index).addClass('error');
        if (index != 'status'){
            error += '<li>' + index.toString().capFirst() + ' - ' + value + '</li>';
        }
    });
    error += '</ul></div>';
    $('#footer_error_message').html(error);
}

$('#contactUsSubmit').on('click', function(event){
    event.preventDefault();
    var form = $('.contact-form');
    var url = '/ajax-contact-us/';
    $.ajax({
        url: url,
        type: 'post',
        data: form.serialize(),
        cache: false,
        beforeSend: function(data){
            form.find('input').removeClass('error');
            form.find('textarea').removeClass('error');
            $('#error_message').html('');
        },
        success: function(data){
            if(data.status===true){
                $('#error_message').html('');
                $('#error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                $.each(data, function(index, value) {
                    $('#id_' + index).val('');
                });
            }
            else{
                processErrors(form, data);
            }
        }
    });
});

$('#propertySubmit').on('click', function(event){
    event.preventDefault();
    var form = $('#property-contact-form');
    var url = '/mls/ajax-contact-us/';
    $.ajax({
        url: url,
        type: 'post',
        data: form.serialize(),
        cache: false,
        beforeSend: function(data){
            form.find('input').removeClass('error');
            form.find('textarea').removeClass('error');
            $('#error_message').html('');
        },
        success: function(data){
            if(data.status===true){
                $('#error_message').html('');
                $('#error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                $.each(data, function(index, value) {
                    $('#id_' + index).val('');
                });
            }
            else{
                processErrors(form, data);
            }
        }
    });
});

$('#footerContactSubmit').on('click', function(event){
    event.preventDefault();
    var form = $('#footer-contact');
    var url = '/ajax-contact-us/';
    $.ajax({
        url: url,
        type: 'post',
        data: form.serialize(),
        cache: false,
        beforeSend: function(data){
            form.find('input').removeClass('error');
            form.find('textarea').removeClass('error');
            $('#footer_error_message').html('');
        },
        success: function(data){
            if(data.status===true){
                $('#footer_error_message').html('');
                $('#footer_error_message').html('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><strong>Thank you!</strong> Your request has been sent. We will get in touch with you shortly.</div>');
                $.each(data, function(index, value) {
                    $('#id_footer_' + index).val('');
                });
            }
            else{
                processFooterErrors(form, data);
            }
        }
    });
});
