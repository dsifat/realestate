var intcomma = function(value) {
    // inspired by django.contrib.humanize.intcomma
    var origValue = String(value);
    var newValue = origValue.replace(/^(-?\d+)(\d{3})/, '$1,$2');
    if (origValue == newValue){
        return newValue;
    } else {
        return intcomma(newValue);
    }
};

String.prototype.capFirst = function() {
    return this.charAt(0).toUpperCase() + this.substring(1);
};

function InitPropertyPrice(price_from, price_to){
    jQuery('.price-value .from').text(price_from);
    jQuery('.price-value .from').currency({ region: 'US', thousands: ' ', decimal: ',', decimals: 0 });

    jQuery('.price-value .to').text(price_to);
    jQuery('.price-value .to').currency({ region: 'US', thousands: ' ', decimal: ',', decimals: 0 });

    $('.property-filter .price-slider').slider({
        range: true,
        min: 20000,
        max: 30000000,
        values: [price_from, price_to],
        slide: function(event, ui) {
            jQuery('.property-filter .price-from input').attr('value', ui.values[0]);
            jQuery('.property-filter .price-to input').attr('value', ui.values[1]);

            jQuery('.price-value .from').text(ui.values[0]);
            jQuery('.price-value .from').currency({ region: 'US', thousands: ' ', decimal: ',', decimals: 0 });

            jQuery('.price-value .to').text(ui.values[1]);
            jQuery('.price-value .to').currency({ region: 'US', thousands: ' ', decimal: ',', decimals: 0 });
        }
    });
}

$('#clearForm').on('click', function(event){
    $('option').attr('selected', false);
    $('select').trigger('liszt:updated');
    InitChosen();
});

$(document).ready(function(){

    var url = document.location.toString();
    if (url.match('#')) {
        $('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
    }
    resizeWindow();

    var rtime = new Date(1, 1, 2000, 12, 00, 00);
    var timeout = false;
    var delta = 200;
    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    $('img.lazy1').lazyload();

    function resizeend(){
        if (new Date() - rtime < delta){
            setTimeout(resizeend, delta);
        }
        else{
            timeout = false;
            resizeWindow();
        }
    }

    function resizeWindow(){
        if ($(document).width() < 380){
            $('.nav-tabs').addClass('nav-pills').addClass('nav-stacked').removeClass('nav-tabs');
        }
        else {
            $('.nav-pills').addClass('nav-tabs').removeClass('nav-stacked').removeClass('nav-pills');
        }
    }
});


