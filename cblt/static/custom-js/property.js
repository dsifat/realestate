(function($){

var map = null;
var markers = new Array();
var bounds = new google.maps.LatLngBounds();
var map_json = {};
var house_marker;
var geocoder, location1, location2;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map_settings = {};
var contents = [];
var LatLng = new google.maps.LatLng(property_location[0]['lat'], property_location[0]['lng']);
var locations = [];
var marker_type_icons = []
var marker_content = []
var content_string = '<div class="root-marker-summary" style="height:118px;"><div class="title">'+rootProperty+'</div><div class="root-property-description">Select one of the items (ski resort, trails, golf...) to view the distance and the drive time to the featured locations.</div></div>';
var root_marker_icon = "/static/img/default_marker.png"
contents.push(content_string);
locations.push([property_location[0]['lat'],property_location[0]['lng']])
map_settings = {
    transparentMarkerImage : transperantMarkerImage,
    pixelOffsetX     : -145,
    pixelOffsetY    : -200,
};
marker_content.push(content_string);
marker_type_icons.push(root_marker_icon);
map_settings['locations'] =  locations;
map_settings['location_contents'] = contents;
map_settings['marker_type_icons'] = marker_type_icons;
map_settings['marker_content'] = marker_content;

$(document).ready(function() {

    //Add driving distance in point of interest lists
        //$('.ltg_name').bind('click',function(){
        $(document).on('click', '.ltg_name', function(){            
            $(this).parents('.location-type').each(function(i, items_list){
                $(items_list).find('ul.subrelated li.location').each(function(j, location){
                    var current_latitude = $(location).find('input').data('latitude');
                    var current_longitude = $(location).find('input').data('longitude');
                    // Make Point from latitude and longitude to calculate driving distance
                    fromPoint = new google.maps.LatLng(property_location[0]['lat'], property_location[0]['lng']);
                    toPoint = new google.maps.LatLng(current_latitude,current_longitude);
                    var distance_from_property = calculateDistance(property_location[0]['lat'], property_location[0]['lng'], current_latitude, current_longitude);
                    $(location).find('span.location_distance').text('('+distance_from_property+')');
                });
            });

            /*var li = $(this).parents('.location-type').find('ul.subrelated li.location');
            re = /\((.*)\)/i;
            lis = li.sort(function(a, b) {
                console.log($(a).find('span.location_distance').text().match(re)[1].split(/ +/)[0]);
                if(parseFloat($(a).find('span.location_distance').text().match(re)[1].split(/ +/)[0]) > parseInt($(b).find('span.location_distance').text().match(re)[1].split(/ +/)[0]))
                    return 1;
                else return -1;
            });
            $(this).parents('.location-type').find('ul.subrelated').empty().html(lis);*/
        });

    // Reset location clicking on Reset location button
    //$('#reset-location').bind('click', function(event){
    $(document).on('click', '#reset-location', function(){         
        hideMarkers(markers);
        markers = new Array();
        uncheckPlaces();
        locations = [];
        contents = []
        marker_type_icons = [];
        contents.push(content_string);
        marker_type_icons.push(root_marker_icon);        
        locations.push([property_location[0]['lat'],property_location[0]['lng']]);
        console.log(locations);
        map_settings['locations'] =  locations;
        map_settings['location_contents'] = contents;
        map_settings['marker_type_icons'] = marker_type_icons;
        loadPropertyMap(map_settings);
    });



    $('a[href="#street-view"]').on('shown.bs.tab', function (e) {
        renderStreetMap();
    });

    var hash = window.location.hash;
    if(hash == '#extra-images'){
        $('#images-tab').removeClass('active');
        $('#images').removeClass('active');
        $('#extra-images-tab').addClass('active');
        $('#extra-images-content').addClass('active');

        propertyView();
        $('#extra-images-tab').click();
    }


    $('.subrelated').hide();

    $(function() {
        $("img.lazy1").lazyload();
    });


    // event listener for location type checkbox
    $('.location_type_group').bind('click', function(event){
        if($(event.currentTarget).prop('checked')){

            $.each($(event.currentTarget).parent().parent().find('input.location-chckbx'), function(index, value){
                $(value).prop('checked', true);
                $(value).trigger('change');
            });

            //trigger click event for ltg_name so that the ul will expand
            $(event.currentTarget).parent().parent().find('.ltg_name').trigger('click');
        }
        else;
    });



    function changeClass(is_visible, target){
        if (is_visible){
            $(target).children('span').addClass('expanded');
        }
        else{
            $(target).children('span').removeClass('expanded');
        }
    }

    //$('.ltg_name').bind('click', function(event){
    $(document).on('click', '.ltg_name', function(event){
        toggleSubLocations($(event.currentTarget).parent().find('ul'), $(this).parent(),changeClass);

    });


    $('.galery2 .images-box .images').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Tristan Roberts</small>';
            }
        }
    });

    $('.galery .images-box .images').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title') + '<small>by Tristan Roberts</small>';
            }
        }
    });

    $(document).on('click', '.ez-checkbox', function(){ 
//   $('.ez-checkbox').bind('click', function(event){
            hideMarkers(markers);
            markers = new Array();
            locations = [];
            contents = [];
            marker_content = [];
            marker_type_icons = [];
            marker_content.push(content_string)
            marker_type_icons.push(root_marker_icon);
            contents.push(content_string);
            locations.push([property_location[0]['lat'],property_location[0]['lng']])
            map_settings['locations'] = [];
            map_settings['location_contents'] = [];
           $(".location-chckbx:checked").each(function(){
                var loc_type = $(this).parents('.location-type').find('.location_type_details');
                var info = $(this).parent().parent().find('.poi-details');
                var latitude = info.children('.latitude').text();
                var longitude = info.children('.longitude').text();
                var location_name = info.children('.poi-title').text();
                var link = info.children('.poi-link').text();
                var title = info.children('.poi-title').text();
                var desc = info.children('.poi-desc').text();
                var image = info.children('.poi-image').text();
                var position = new google.maps.LatLng(latitude, longitude);
                //var marker_type_icon = getLocationIcon($(loc_type).children('.marker-type').text());
                var marker_type_icon = "/static/img/default_marker.png"

                // Make Point from latitude and longitude to calculate driving distance
                fromPoint = new google.maps.LatLng(property_location[0]['lat'], property_location[0]['lng']);
                toPoint = new google.maps.LatLng(latitude,longitude);                
                var res= calculateDrivingDistance(fromPoint, toPoint);
                var text_content = '<div class="infobox"><div class="close"><img src="/static/img/close.png" alt=""></div><div class="image"><img src="' + image +'" alt=""></div><div class="title"><a href="'+ link +'">'+ location_name +'</a></div><div class="description">'+ desc + '</div><div class="description"><strong>Distance from property:</strong><br/><span class="value">'+ calculateDistance(property_location[0]['lat'], property_location[0]['lng'], latitude, longitude) +'</span><br/>Driving distance:<span class="driving_distance"></span><br/>Duration:<span class="driving_duration"></span></div>';
                var marker_text_content = '<div class="wrapper_img_title"><div class="image" style="float:left;width:60px;height:42px overflow: hidden;"><img width=50 height=50 src="' + image +'" alt="" style="padding:3px;"></div><div class="summary" style="width:83px;float:right;"><div class="title"><a href="'+ link +'">'+ location_name +'</a></div></div></div><br><div class="description" style="float:left;"><strong>Crow flies: <span class="value">'+ calculateDistance(property_location[0]['lat'], property_location[0]['lng'], latitude, longitude) +'</span><br/>Distance: <span class="driving_distance"></span><br/>Duration: <span class="driving_duration"></span></strong></div>'

                if (latitude !="" && longitude !=""){
                    locations.push([latitude, longitude]);
                    contents.push(text_content);
                    marker_type_icons.push(marker_type_icon);
                    marker_content.push(marker_text_content);
                }

           });
           map_settings['locations'] = locations;
           map_settings['location_contents'] = contents;
           map_settings['marker_type_icons'] = marker_type_icons;
           map_settings['marker_content'] = marker_content;
           loadPropertyMap(map_settings)

        });

});

$(window).load(function() {

     google.maps.Map.prototype.setCenterWithOffset = function (latlng, offsetX, offsetY) {
        var map = this;
        var ov = new google.maps.OverlayView();

        ov.onAdd = function () {
            var proj = this.getProjection();
            var aPoint = proj.fromLatLngToContainerPixel(latlng);
            aPoint.x = aPoint.x + offsetX;
            aPoint.y = aPoint.y + offsetY;
            map.setCenter(proj.fromContainerPixelToLatLng(aPoint));
        }

        ov.draw = function () {
        };
        ov.setMap(this);
    };
    google.maps.LatLng.prototype.distanceFrom = function(newLatLng) {
       // setup our variables
       var lat1 = this.lat();
       var radianLat1 = lat1 * ( Math.PI  / 180 );
       var lng1 = this.lng();
       var radianLng1 = lng1 * ( Math.PI  / 180 );
       var lat2 = newLatLng.lat();
       var radianLat2 = lat2 * ( Math.PI  / 180 );
       var lng2 = newLatLng.lng();
       var radianLng2 = lng2 * ( Math.PI  / 180 );
       // sort out the radius, MILES or KM?
       var earth_radius = 3959; // (km = 6378.1) OR (miles = 3959) - radius of the earth

       // sort our the differences
       var diffLat =  ( radianLat1 - radianLat2 );
       var diffLng =  ( radianLng1 - radianLng2 );
       // put on a wave (hey the earth is round after all)
       var sinLat = Math.sin( diffLat / 2  );
       var sinLng = Math.sin( diffLng / 2  );

       // maths - borrowed from http://www.opensourceconnections.com/wp-content/uploads/2009/02/clientsidehaversinecalculation.html
       var a = Math.pow(sinLat, 2.0) + Math.cos(radianLat1) * Math.cos(radianLat2) * Math.pow(sinLng, 2.0);

       // work out the distance
       var distance = earth_radius * 2 * Math.asin(Math.min(1, Math.sqrt(a)));

       // return the distance
       return distance;
    };



});

function renderElements(map_settings) {
        var bounds = new google.maps.LatLngBounds();
        markers = new Array();
        $.each(map_settings.locations, function (index, location) {
            var latlng = new google.maps.LatLng(location[0], location[1]);
            bounds.extend(latlng);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(location[0], location[1]),
                map     : map,
                icon    : map_settings.transparentMarkerImage,
            });

            marker.infobox = new InfoBox({
                content               : map_settings.location_contents[index],
                disableAutoPan        : false,
                maxWidth              : 0,
                pixelOffset           : new google.maps.Size(map_settings.pixelOffsetX, map_settings.pixelOffsetY),
                zIndex                : null,
                closeBoxURL           : "",
                infoBoxClearance      : new google.maps.Size(1, 1),
                position              : new google.maps.LatLng(location[0], location[1]),
                isHidden              : false,
                pane                  : "floatPane",
                enableEventPropagation: false
            });
            marker.infobox.isOpen = false;
            marker_icon_url = map_settings.marker_type_icons[index];
            marker_content_value = map_settings.marker_content[index];
            marker.marker = new InfoBox({
                draggable             : true,
               //content               : "<div class='infobox' style='height:20px; width:80px;'><div class='residential-marker-new'><div class='marker-inner'><a href='asfl'>go there</a></div></div></div>",
                content               : "<div class='residential-marker' style='background-image:url("+ marker_icon_url +");'><div class='propertymarker-inner'>" +marker_content_value +"</div></div>",
                disableAutoPan        : true,
                pixelOffset           : new google.maps.Size(-21, -58),
                position              : new google.maps.LatLng(location[0], location[1]),
                closeBoxURL           : "",
                isHidden              : false,
                pane                  : "floatPane",
                enableEventPropagation: true
            });
            marker.marker.isHidden = false;
            marker.marker.open(map, marker);
            markers.push(marker);
            map.setCenter(bounds.getCenter());
            map.fitBounds(bounds);

//            google.maps.event.addListener(marker, 'click', function (e) {
//                var curMarker = this;
//                $.each(markers, function (index, marker) {
//                    // if marker is not the clicked marker, close the marker
//                    if (marker !== curMarker) {
//                        marker.infobox.close();
//                        marker.infobox.isOpen = false;
//                    }
//                });
//
//                if (curMarker.infobox.isOpen === false) {
//                    curMarker.infobox.open(map, this);
//                    curMarker.infobox.isOpen = true;
//                    map.setCenterWithOffset(curMarker.getPosition(), 0, -100);
//                } else {
//                    curMarker.infobox.close();
//                    curMarker.infobox.isOpen = false;
//                }
//            });

            zoomChangeBoundsListener = google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
                    if (this.getZoom()){
                        var zoomLevel = 18;
                        if(locations.length >1){
                            zoomLevel = 11;
                        }
                        this.setZoom(zoomLevel);
                    }
            });
//            setTimeout(function(){google.maps.event.removeListener(zoomChangeBoundsListener)}, 1000)
                    });


}


function loadPropertyMap(map_settings) {
        var mapOptions = {
            zoom              : 18,
            mapTypeId         : google.maps.MapTypeId.SATELLITE,
            scrollwheel       : false,
            draggable         : true,
            mapTypeControl    : false,
            panControl        : true,
            zoomControl       : true,
            center: LatLng,
        };
        directionsDisplay = new google.maps.DirectionsRenderer({
           suppressMarkers: true,
           suppressInfoWindows: true
        });
        map = new google.maps.Map(document.getElementById('property-map'), mapOptions);
        directionsDisplay.setMap(map);
        google.maps.event.addListener(map, 'zoom_changed', function () {
            $.each(markers, function (index, marker) {
                marker.infobox.close();
                marker.infobox.isOpen = false;
            });
        });
        renderElements(map_settings);

        $(document).on('click', '.infobox .close',function () {
            $.each(markers, function (index, marker) {
                marker.infobox.close();
                marker.infobox.isOpen = false;
            });
        });


    }


google.maps.event.addDomListener(window, 'load', loadPropertyMap(map_settings));

// Calculate Driving distance from the property to point of interest

function calculateDrivingDistance(fromPoint, toPoint) {
  var request = {
    origin:fromPoint,
    destination:toPoint,
    travelMode: google.maps.DirectionsTravelMode.DRIVING
  };

  directionsService.route(request, function(response, status) {
    if (status == google.maps.DirectionsStatus.OK) {
        
        $(".driving_distance").text(response.routes[0].legs[0].distance.text);
        $(".driving_duration").text(response.routes[0].legs[0].duration.text);
        directionsDisplay.setDirections(response);

    }
  });

}

function hideMarkers(markers) {
    /* Remove All Markers */
    while(markers.length){
        markers.pop().setMap(null);
    }
}


function renderStreetMap(){

    var streetViewCheck = new google.maps.StreetViewService();
    streetViewCheck.getPanoramaByLocation(LatLng, 50, function(result, status) {
        if (status == google.maps.StreetViewStatus.ZERO_RESULTS) {
            streetViewAvailable = 0;
            $('#map-panorama').attr('style', 'display:none');
            $('#nopanorama').attr('style', 'display:block');
        }else{
            streetViewAvailable = 1;
        }
    });

    var panoramaOptions = {
        position : new google.maps.LatLng(property_location[0]['lat'], property_location[0]['lng']),
        pov : {
            heading : 34,
            pitch : 10
        }
    };
    var panorama = new google.maps.StreetViewPanorama(document.getElementById('map-panorama'), panoramaOptions);
    map.setStreetView(panorama);

}

function calculateDistance(from_lat, from_long, to_lat, to_long){
    try
    {
        var glatlng1 = new google.maps.LatLng(from_lat, from_long);
        var glatlng2 = new google.maps.LatLng(to_lat, to_long);
        var miledistance = glatlng1.distanceFrom(glatlng2, 3959).toFixed(1);
        var kmdistance = (miledistance * 1.609344).toFixed(1);
       // return '' + miledistance + ' miles (or ' + kmdistance + ' km)';
       return '' + miledistance + ' mi';
    }
    catch (error)
    {
        return error;
    }
}

function getLocationIcon(locationType){

        switch(locationType) {
            case 'Campground':
                return campgroundMarker;
            case 'Market':
                return marketMarker;
            case 'Museum':
                return museumMarker;
            case 'Restaurant':
                return restaurantMarker;
            case 'Ski Resort':
                return skiMarker;
            case 'Golf Courses':
                return golfMarker;
            case 'Recreation':
                return recreationMarker;
            case 'Business':
                return businessMarker;
            case 'Beach':
                return beachMarker;
            case 'Park':
                return parkMarker;
            case 'Trails':
                return trailsMarker;
            case 'Marina':
                return marinaMarker;
            case 'rootProperty' :
                return rootPropertyMarker;
            default:
                return rootPropertyMarker;
        }
        return rootPropertyMarker;
    }



//function checks the group checkbox if all sub-checkboxes in a group is selected, otherwise it unchecks it
function checkLocationGroup(checked, checkbox_len, group_checkbox){


    if(checked < checkbox_len){
        $(group_checkbox).prop('checked', false);
        $(group_checkbox).trigger('change');
    }

    if(checked == checkbox_len){
        $(group_checkbox).prop('checked', true);
        $(group_checkbox).trigger('change');
    }

    if(checked == 0){
        $(group_checkbox).parent().parent().find('.ltg_name').trigger('click');
    }

}

function toggleSubLocations(el, target, changeClass){
    if ($(el).find("input:checked").length > 0){
        if($(el).is(":visible")){
        }
        else{
            $(el).slideToggle('fast', function(){
                changeClass($(el).is(':visible'), target);
            });
        }
    }
    else{
        $(el).slideToggle('fast',function(){
            changeClass($(el).is(':visible'), target);
        });
    }
}

function uncheckPlaces(){
    //unchecks all places of interest checkboxes
    var checked = $('.location-chckbx:checked');
    $.each(checked, function(index, element){
            $(element).prop('checked', false);    
            $(element).trigger('change');
    });

    //trigger click to close opened sublocations
    $('.ltg_name.expanded').trigger('click');
}






})(jQuery);

