(function($){
	$(document).ready(function () {
		var currentUrl = window.location.href ;
		var curentMLSNo = currentUrl.split("mls-")[1].split('-')[0];
		var siteContentToken = $(".sitecontent-token").val();
		//console.log(siteContentToken);
		$.ajax({
	        type: "POST",
	        url: "/realestate/related-contents/", 
	        data: {'mls_no': curentMLSNo, 'csrfmiddlewaretoken': siteContentToken},
	        beforeSend : function() {
                 $('#preloader').show()
                },
	        success : function(data) {
	             $('#pin-view .related-contents').append(data);
	             $('input[type="checkbox"]').ezMark();
  				 $('input[type="radio"]').ezMark();
	         },
	        complete : function() {
	                $('#preloader').hide();                      
	            }
		});

	});

})(jQuery);