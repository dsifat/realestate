//Sidebar Carousel
function carousel() {
    var $ = jQuery;
    $('.carousel-box .carousel').each(function () {
        var widget = $(this).parents('.carousel-box');

        if ($(widget).hasClass('features-first') && $('body').width() <= 979) {
            $(this).closest('.carousel-box').find('.next').hide();
            $(this).closest('.carousel-box').find('.prev').hide();
            $('.carousel-box.features-first .carousel').trigger("destroy", true);
        } else {
            $(this).carouFredSel({
                auto     : false,
                infinite : false,
                scroll   : 1,
                height   : 'auto',
                next     : $(this).closest('.carousel-box').find('.next'),
                prev     : $(this).closest('.carousel-box').find('.prev'),
                swipe : {
                    onMouse: true,
                    onTouch: true,
                    easing: 'linear'
                }
            });
        }
    });
}

//Property View Carousel
function propertyView() {
    var $ = jQuery,
        thumbsDirection,
        thumbsVisible;

    if ($('body').width() >= 980) {
        thumbsDirection = 'up';
        thumbsVisible = 4;
    } else {
        thumbsDirection = 'left';
        thumbsVisible = null;
    }

    if ( $('body').width() >= 980 ) {
        $('#thumbs > div:odd').each(function(){
            if ($(this).prev().find('a').length < 2) {
                $(this).children('a').addClass('two');
                $(this).children('a').appendTo($(this).prev());
                $(this).remove();
            }
        });

    }
    else
    {
        $('#thumbs > div').each(function(){
            if ($(this).find('a').length > 1) {
                $(this).children('a').removeClass('two');
                $(this).before('<div></div>');
                $(this).children('a:first').appendTo($(this).prev());
            }
        });
    }

    $('#thumbs a:first').addClass('selected');

    $('#thumbs a').click(function() {
        $('.property-view .galery .images').trigger('slideTo', '#' + this.href.split('#').pop() );
        $('#thumbs a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });

    $('.property-view .galery .images').each(function () {
        $(this).carouFredSel({
            responsive: true,
            circular: false,
            auto: false,
            items: {
                visible: 1,
                width: 650,
                height: '58%'
            },
            scroll: {
                fx: 'crossfade',
                duration: 750
            },
            swipe: {
                onMouse: true,
                onTouch: true,
                easing: 'linear'
            },
            next: {
                button: $('.thumbs-box .next'),
                key: 'right'
            },
            prev: {
                button: $('.thumbs-box .prev'),
                key: 'left'
            }

        }).parents('.galery').addClass('onload');
    });

    $('#thumbs').each(function () {
        $(this).carouFredSel({
            circular: false,
            infinite: false,
            auto: false,
            prev: {
                button: $('.thumbs-box .prev')
            },
            next: {
                button: $('.thumbs-box .next')
            },
            direction : thumbsDirection,
            items: {
                visible: thumbsVisible,
            },
            swipe: {
                onMouse: true,
                onTouch: true,
                easing: 'linear'
            }
        }).parents('.thumbs-box').addClass('onload');
    });


    $('#images-tab').on('click', function(){
        if ($('body').width() >= 980) {
            thumbsDirection = 'up';
            thumbsVisible = 4;
        } else {
            thumbsDirection = 'left';
            thumbsVisible = null;
        }

        if ( $('body').width() >= 980 ) {
            $('#thumbs > div:odd').each(function(){
                if ($(this).prev().find('a').length < 2) {
                    $(this).children('a').addClass('two');
                    $(this).children('a').appendTo($(this).prev());
                    $(this).remove();
                }
            });

        }
        else
        {
            $('#thumbs > div').each(function(){
                if ($(this).find('a').length > 1) {
                    $(this).children('a').removeClass('two');
                    $(this).before('<div></div>');
                    $(this).children('a:first').appendTo($(this).prev());
                }
            });
        }

        $('#thumbs a:first').addClass('selected');

        $('#thumbs a').click(function() {
            $('.property-view .galery .images').trigger('slideTo', '#' + this.href.split('#').pop() );
            $('#thumbs a').removeClass('selected');
            $(this).addClass('selected');
            return false;
        });

        $('.property-view .galery .images').each(function () {
            $(this).carouFredSel({
                responsive: true,
                circular: false,
                auto: false,
                items: {
                    visible: 1,
                    width: 650,
                    height: '58%'
                },
                scroll: {
                    fx: 'crossfade',
                    duration: 750
                },
                swipe: {
                    excludedElements:"button, input, select, textarea, .noSwipe",
                    onMouse: true,
                    onTouch: true,
                    easing: 'linear'
                },
                next: {
                    button: $('.thumbs-box .next'),
                    key: 'right'
                },
                prev: {
                    button: $('.thumbs-box .prev'),
                    key: 'left'
                }

            }).parents('.galery').addClass('onload');
        });

        $('#thumbs').each(function () {
            $(this).carouFredSel({
                circular: false,
                infinite: false,
                auto: false,
                prev: {
                    button: $('.thumbs-box .prev')
                },
                next: {
                    button: $('.thumbs-box .next')
                },
                direction : thumbsDirection,
                items: {
                    visible: thumbsVisible
                },
                swipe: {
                    onMouse: true,
                    onTouch: true,
                    easing: 'linear'
                }
            }).parents('.thumbs-box').addClass('onload');
        });
    });

    $('#extra-images-tab').on('click', function(){
        if ( $('body').width() >= 980 ) {
            $('#thumbs2 > div:odd').each(function(){
                if ($(this).prev().find('a').length < 2) {
                    $(this).children('a').addClass('two');
                    $(this).children('a').appendTo($(this).prev());
                    $(this).remove();
                }
            });

        }
        else
        {
            $('#thumbs2 > div').each(function(){
                if ($(this).find('a').length > 1) {
                    $(this).children('a').removeClass('two');
                    $(this).before('<div></div>');
                    $(this).children('a:first').appendTo($(this).prev());
                }
            });
        }

        $('#thumbs2 a:first').addClass('selected');

        $('#thumbs2 a').click(function() {
            $('.property-view .galery2 .images').trigger('slideTo', '#' + this.href.split('#').pop() );
            $('#thumbs2 a').removeClass('selected');
            $(this).addClass('selected');
            return false;
        });

        $('.property-view .galery2 .images').each(function () {
            $(this).carouFredSel({
                responsive: true,
                circular: false,
                auto: false,
                items: {
                    visible: 1,
                    width: 650,
                    height: '58%'
                },
                scroll: {
                    fx: 'crossfade',
                    duration: 750
                },
                next: {
                    button: $('.thumbs-box2 .next'),
                    key: 'right'
                },
                prev: {
                    button: $('.thumbs-box2 .prev'),
                    key: 'left'
                },
                swipe: {
                    excludedElements:"button, input, select, textarea, .noSwipe",
                    onMouse: true,
                    onTouch: true,
                    easing: 'linear'
                }

            }).parents('.galery2').addClass('onload');
        });

        $('#thumbs2').each(function () {
            $(this).carouFredSel({
                circular: false,
                infinite: false,
                auto: false,
                prev: {
                    button: $('.thumbs-box2 .prev')
                },
                next: {
                    button: $('.thumbs-box2 .next')
                },
                direction : thumbsDirection,
                items: {
                    visible: thumbsVisible,
                },
                swipe: {
                    onMouse: true,
                    onTouch: true,
                    easing: 'linear'
                }
            }).parents('.thumbs-box2').addClass('onload');
        });
    });


$('#hoa-images-tab').on('click', function(){
    if ( $('body').width() >= 980 ) {
        $('#thumbs3 > div:odd').each(function(){
            if ($(this).prev().find('a').length < 2) {
                $(this).children('a').addClass('two');
                $(this).children('a').appendTo($(this).prev());
                $(this).remove();
            }
        });

    }
    else
    {
        $('#thumbs3 > div').each(function(){
            if ($(this).find('a').length > 1) {
                $(this).children('a').removeClass('two');
                $(this).before('<div></div>');
                $(this).children('a:first').appendTo($(this).prev());
            }
        });
    }

    $('#thumbs3 a:first').addClass('selected');

    $('#thumbs3 a').click(function() {
        $('.property-view .galery3 .images').trigger('slideTo', '#' + this.href.split('#').pop() );
        $('#thumbs3 a').removeClass('selected');
        $(this).addClass('selected');
        return false;
    });

    $('.property-view .galery3 .images').each(function () {
        $(this).carouFredSel({
            responsive: true,
            circular: false,
            auto: false,
            items: {
                visible: 1,
                width: 650,
                height: '58%'
            },
            scroll: {
                fx: 'crossfade',
                duration: 750
            },
            next: {
                button: $('.thumbs-box3 .next'),
                key: 'right'
            },
            prev: {
                button: $('.thumbs-box3 .prev'),
                key: 'left'
            },
            swipe: {
                excludedElements:"button, input, select, textarea, .noSwipe",
                onMouse: true,
                onTouch: true,
                easing: 'linear'
            }

        }).parents('.galery3').addClass('onload');
    });

    $('#thumbs3').each(function (){
        $(this).carouFredSel({
            circular: false,
            infinite: false,
            auto: false,
            prev: {
                button: $('.thumbs-box3 .prev')
            },
            next: {
                button: $('.thumbs-box3 .next')
            },
            direction : thumbsDirection,
            items: {
                visible: thumbsVisible,
            },
            swipe: {
                onMouse: true,
                onTouch: true,
                easing: 'linear'
            }
        }).parents('.thumbs-box3').addClass('onload');
    });
});



}



jQuery(document).ready(function() {
    "use strict";
    var $ = jQuery;

    /* Carousel */
    $(window).bind({
        load : function() {
            carousel();
            propertyView();
        }
    });

    $('a[data-toggle="tab"]').on('shown', function(e) {
        if ($(e.target).attr('href') == '#images'){
            console.log('images');
            propertyView();
        }
        else if ($(e.target).attr('href') == '#extra-images-content'){
            console.log('property-view');
            propertyView();
        }
    });


    $('.disabled').click(function () {
        return false;
    });


    //Carousel next/prev button (not active)
    $('.prev-next').bind('touchstart', function(){
        $(this).removeClass('not-hover');
    });
    $('.prev-next').bind('touchend', function(){
        $(this).addClass('not-hover');
    });



    //Resize Window
    $( window ).resize(function() {
        delay( function() {
            if (document.width > 768) {
                $('.viewport').remove();
            } else {
                $('head').append('<meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">');
            }

            //Carousel
            carousel();
            propertyView();

        }, 'resize' );

    });

    var delay = ( function() {
        var timeout = { };

        return function( callback, id, time ) {
            if( id !== null ) {
                time = ( time !== null ) ? time : 100;
                clearTimeout( timeout[ id ] );
                timeout[ id ] = setTimeout( callback, time );
            }
        };
    })();
});
