$('#inputCommunityType').change(function(){
    var subdivisions = community_map[$(this).val()];
    var choices = '<select id="inputSubdivisionType" name="area"><option value="All">All</option>';
    subdivisions.sort();
    $.each(subdivisions, function(key, value){
        choices += "<option name='"+ value +"'>"+ value +"</option>";
    });
    choices += '</select>';
    $('#inputSubdivisionType').replaceWith(choices);
    $('#inputSubdivisionType_chzn').remove();
    $("option[value='None']").remove();

    $.each($('.bedrooms .inner'), function(index, value){
        if($(value).text()='None'){
            $(value).text('0');
        }
    });

    $.each($('.bathrooms .inner'), function(index, value){
        if($(value).text()='None'){
            $(value).text('0');
        }
    });

    $.each($('.area .inner'), function(index, value){
        if($(value).text()='None'){
            $(value).text('N/A');
        }
    });

    $.each($('.bedrooms .content'), function(index, value){
        if($(value).text()='None'){
            $(value).text('0');
        }
    });

    $.each($('.bathrooms .content'), function(index, value){
        if($(value).text()='None'){
            $(value).text('0');
        }
    });

    $.each($('.area .content'), function(index, value){
        if($(value).text()='None'){
            $(value).text('N/A');
        }
    });

    InitChosen();
});
