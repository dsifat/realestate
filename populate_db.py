
from cblt import settings
from django.core.management import setup_environ

setup_environ(settings)

from django.db import IntegrityError
from mls.models import Listing, City, Area, Setting, View


cities = Listing.objects.values('C_City').distinct()
areas = Listing.objects.values('C_Area').distinct()
views = Listing.objects.values('C_VIEW').distinct()
settings = Listing.objects.values('C_Setting').distinct()


for city in cities:
    new_city = City(name=city['C_City'])
    print new_city.name

    try:
        new_city.save()
    except IntegrityError, e:
        print "interity error"

for area in areas:
    new_area = Area(name=area['C_Area'])
    print new_area.name

    try:
        new_area.save()
    except IntegrityError, e:
        print "interity error"


for view in views:
    new_view = View(name=view['C_VIEW'])
    print new_view.name

    try:
        new_view.save()
    except IntegrityError, e:
        print "interity error"

for setting in settings:
    new_setting = Setting(name=setting['C_Setting'])
    print new_setting.name

    try:
        new_setting.save()
    except IntegrityError, e:
        print "interity error"
