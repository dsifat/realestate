from cblt import settings
from django.core.management import setup_environ

setup_environ(settings)

from cblaketahoe.models import Users as CBLTUsers
from django.contrib.auth.models import User
from subscription.models import Subscription


cblt_users = CBLTUsers.objects.all()

for user in cblt_users:
    new_user = User.objects.create_user(username=user.name, email=user.mail, password=user.pass_field)
    profile = new_user.profile
    Subscription.objects.create(subscriber=profile,
                                subscription_type='weekly_report')
