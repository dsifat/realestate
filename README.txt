Requirements are on the requirements.txt file

DATABASE Notes:

* vielerets uses the mls_listingraw table
* django uses the mls_listing table
* during setup, django creates a mls_listingraw;
  this table should be deleted so that vielerets can create a new table
* initialize rets, download listing for the first time; this step will create the mls_listingraw table
* add primary key to the mls_listingraw table
  alter table mls_listingraw add column `id` int(10) unsigned primary KEY AUTO_INCREMENT;
* previous step will allow the unprocessedlisting table to be migrated
* add MYSQL triggers for the mls_listingraw
* the stated trigger below will add a row data to the unprocessedlisting table representing the mls_listingraw object


Search Notes:


