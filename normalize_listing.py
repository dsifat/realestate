from cblt import settings
from django.core.management import setup_environ

setup_environ(settings)

from mls.models import UnprocessedListing, ListingRaw2,\
                        Area, Community, City, Setting, create_price_change_set, create_statistics

unprocessed_listings = UnprocessedListing.objects.all()
for unprocessed_listing in unprocessed_listings:
    try:
        mls_id = unprocessed_listing.listing_mls_id
        print "normalizing " + str(mls_id)
        listing_raw = ListingRaw.objects.get(C_MLSNo=mls_id)
        listing_raw.normalize_listing()
        unprocessed_listing.delete()
    except Exception as e:
        print e

for city in City.objects.all():
    city.available_property()

for area in Area.objects.all():
    area.available_property()

for community in Community.objects.all():
    community.available_property()

for setting in Setting.objects.all():
    setting.available_property()

create_price_change_set()
create_statistics()