from cblt import settings
from django.core.management import setup_environ

setup_environ(settings)

from django.conf.settings import PROJECT_ROOT
from mls.models import Listing
import os, sys
import Image


size_270 = 270, 270
size_100 = 100, 100

def create_thumbnails(listing):
    print listing
    infile = os.path.join(PROJECT_ROOT, listing.get_picture_url())
    print infile
    if os.path.isfile(infile):
        outfile_270 = os.path.splitext(infile)[0] + "_s270.jpg"
        outfile_100 = os.path.splitext(infile)[0] + "_s100.jpg"    
        try:
            print "creating thumbnail : " + str(listing)
            im = Image.open(infile)
            im.thumbnail(size_270, Image.ANTIALIAS)
            im.save(outfile_270, "JPEG")

            im = Image.open(infile)
            im.thumbnail(size_100, Image.ANTIALIAS)
            im.save(outfile_100, "JPEG")

            listing.image_thumbnail_270.name = outfile_270
            listing.image_thumbnail_100.name = outfile_100

            listing.save()

        except Exception as e:
            print e
            print "cannot create thumbnail for '%s'" % infile    


listings = Listing.objects.all()

for listing in listings:
    print listing
    create_thumbnails(listing)

