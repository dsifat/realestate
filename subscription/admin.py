from django.contrib import admin
from subscription.models import SubscriberProfile, Subscription


class SubscriberProfileAdmin(admin.ModelAdmin):
    class Meta:
        model = SubscriberProfile
    list_display = ('user',)
    raw_id_fields = ('user',)
    search_fields = ('user',)


class SubscriptionAdmin(admin.ModelAdmin):
    class Meta:
        model = Subscription

    list_display = ('subscriber', 'subscription_start', 'subscription_type', 'active', )
    raw_id_fields = ('subscriber',)

#class SubscriptionQueryParams(admin.ModelAdmin):

admin.site.register(SubscriberProfile, SubscriberProfileAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
