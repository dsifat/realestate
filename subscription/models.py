from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class SubscriberProfile(models.Model):
    #user = models.ForeignKey(User)
    user = models.OneToOneField(User, related_name='SubscriberProfile')
    phone = models.CharField(max_length=150, null=True, blank=True)
    address = models.TextField(max_length=250, null=True, blank=True)
    last_login = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '%s' % (self.user.username,)

    def get_full_name(self):
        return ("%s %s" % (self.user.first_name, self.user.last_name))
    get_full_name.short_description = 'Name'

User.profile = property(lambda u: SubscriberProfile.objects.get_or_create(user=u)[0])

class Subscription(models.Model):
    SUBSCRIPTION_TYPE = (
        ('W', 'Weekly'),
        ('M', 'Monthly'),
        ('Q', 'Quarterly')
    )

    subscriber = models.ForeignKey(SubscriberProfile)
    subscription_type = models.CharField(max_length=50, choices=SUBSCRIPTION_TYPE, blank=True, null=True)
    subscription_start = models.DateField(auto_now_add=True)
    subscription_end = models.DateField(blank=True, null=True)
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s - %s' % (self.subscriber, self.subscription_type,)
