import re

from django import template
from django.core.urlresolvers import reverse

tag_end_re = re.compile(r'(\w+)[^>]*>')
entity_end_re = re.compile(r'(\w+;)')

register = template.Library()


def reduce_len(value, arg):
    """
    reduces length of the given string based on argument given.
    characters from right are cut first.
    """
    return value[:arg]

register.filter('reduce_len', reduce_len)

def custom_paginator(pages, current_page, adjacent_pages=1):
    startPage = max(current_page - adjacent_pages, 1)
    if startPage <= 3: startPage = 1
    endPage = current_page + adjacent_pages + 1
    if endPage >= pages - 1: endPage = pages + 1
    page_numbers = [n for n in range(startPage, endPage) \
            if n > 0 and n <= pages]
    return page_numbers

register.filter('custom_paginator', custom_paginator)

def create_address_link(listing, arg):
    """
    returns a link of an mls address
    """
    address_string = str(listing.C_Address)[:arg] + "..."
    url = listing.get_detailed_property_url()
    link = "<a href=" + url + " title='" + listing.C_Address + "'>"+ address_string +"</a>"

    return link

register.filter('create_address_link', create_address_link)

def format_address_h3(listing, string_len):
    """
    returns a link of an mls address
    """

    #address = str(listing.C_Address) + " " + str(listing.C_City) + ", " + str(listing.C_State) + " " + str(listing.C_Zip)
    address = u'%s %s %s %s' % (listing.C_Address, listing.C_City, listing.C_State, listing.C_Zip,)

    if len(address) > string_len:
        #address = "<h3>" + str(listing.C_Address) + " " +str(listing.C_City) + ",<br/>" + str(listing.C_State) + " " + str(listing.C_Zip) + "</h3>"
        address = u'%s %s,<br/>%s %s' % (listing.C_Address, listing.C_City, listing.C_State, listing.C_Zip,)
    else:
        address = '%s' % (address,)

    return address

register.filter('format_address_h3', format_address_h3)

@register.simple_tag
def nav_active(request, urls):
    if request.path in (reverse(url) for url in urls.split()):
        return 'nav-active'
    return ''

@register.filter
def truncatehtml(string, length, ellipsis='...'):
    """Truncate HTML string, preserving tag structure and character entities."""
    output_length = 0
    i = 0
    pending_close_tags = {}

    while output_length < length and i < len(string):
        c = string[i]
        if c == '<':
            # probably some kind of tag
            if i in pending_close_tags:
                # just pop and skip if it's closing tag we already knew about
                i += len(pending_close_tags.pop(i))
            else:
                # else maybe add tag

                i += 1
                match = tag_end_re.match(string[i:])
                if match:
                    tag = match.groups()[0]
                    i += match.end()

                    # save the end tag for possible later use if there is one
                    match = re.search(r'(</' + tag + '[^>]*>)', string[i:], re.IGNORECASE)
                    if match:
                        pending_close_tags[i + match.start()] = match.groups()[0]
                else:
                    output_length += 1 # some kind of garbage, but count it in

        elif c == '&':
            # possible character entity, we need to skip it
            i += 1
            match = entity_end_re.match(string[i:])
            if match:
                i += match.end()

            # this is either a weird character or just '&', both count as 1
            output_length += 1
        else:
            # plain old characters
            skip_to = string.find('<', i, i + length)
            if skip_to == -1:
                skip_to = string.find('&', i, i + length)
            if skip_to == -1:
                skip_to = i + length

            # clamp
            delta = min(skip_to - i,
                        length - output_length,
                        len(string) - i)

            output_length += delta
            i += delta

    output = [string[:i]]
    if output_length == length:
        output.append(ellipsis)

    for k in sorted(pending_close_tags.keys()):
        output.append(pending_close_tags[k])

    return "".join(output)
