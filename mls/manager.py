from caching.base import CachingManager
from caching.base import CachingQuerySet

class CachedManager(CachingManager):
    """
    A custom manager for mls models.
    """
    def __init__(self, *args, **kwargs):        
        super(CachedManager, self).__init__(*args, **kwargs)
    
    def get_query_set(self):
        return CachingQuerySet(self.model)
