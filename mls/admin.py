from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from mls.models import (Listing, FeaturedListing, CustomImage,
                        SellerComment,  Area, City, Setting, View,
                        SiteContent, LocationType, PriceChange, ListingStatus,
                        Community, HOA, HOAAmenities, WaterAmenities, Type, Img, Vendor,VendorType,
                        PropertyManagement, PropertyManagementImage,BusinessType,BusinessDirectory)
from django_extensions.admin import ForeignKeyAutocompleteAdmin
from tinymce.widgets import TinyMCE


class SellerCommentAdminForm(forms.ModelForm):
    comment = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}))

    class Meta:
        model = SellerComment


class SellerCommentInlineForm(forms.ModelForm):
    comment = forms.CharField(widget=TinyMCE(attrs={'cols': 70, 'rows': 10}))

    class Meta:
        model = SellerComment


class CommunityAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)

    class Meta:
        model = Community

class SettingAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)

    class Meta:
        model = Setting

class CityAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = City


class AreaAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = Area

class VendorAdminForm(forms.ModelForm):
    address = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = Vendor

class VendorTypeAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = VendorType


class SiteContentAdminForm(forms.ModelForm):
    content = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)

    class Meta:
        model = SiteContent


class PriceChangeAdmin(admin.ModelAdmin):
    list_display = ('listing', 'prev_price', 'new_price', 'timestamp')
    raw_id_fields = ('listing',)
    search_fields = ['listing__C_MLSNo',]

    class Meta:
        model = PriceChange


# class ListingRawAdmin(admin.ModelAdmin):
#     search_fields = ['C_MLSNo', 'C_Address']
#
#     model = ListingRaw2
#     class Meta:
#         pass

class VendorAdmin(admin.ModelAdmin):
    list_display = ('name_of_organization', 'type_of_business', 'phone','areas','cities','communities')
    search_fields = ['type_of_business__name']    
    list_filter = ('city', 'community', 'area', 'type_of_business')
    filter_horizontal = ('city', 'community', 'area', )
    form = VendorAdminForm

    def areas(self, obj):
        return ",\n".join([p.name for p in obj.area.all()])

    def cities(self, obj):
        return ",\n".join([p.name for p in obj.city.all()])

    def communities(self, obj):
        return ",\n".join([p.name for p in obj.community.all()])


class VendorTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ['name']    
    form = VendorTypeAdminForm


class AreaAdmin(admin.ModelAdmin):
    list_display = ('name', 'shortened_name', 'show_on_list')
    readonly_fields = ('name',)
    list_editable = ['show_on_list',]
    form = AreaAdminForm


class CommunityAdmin(admin.ModelAdmin):
    list_display = ('name', 'shortened_name', 'show_on_list')
    readonly_fields = ('name',)
    list_editable = ['show_on_list',]
    form = CommunityAdminForm


class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'show_on_list')
    list_editable = ['show_on_list',]
    form = CityAdminForm


class SettingAdmin(admin.ModelAdmin):
    list_display = ('name', 'order', 'description')
    list_editable = ('order',)
    form = SettingAdminForm


class ViewAdmin(admin.ModelAdmin):
    list_display = ('name',)


class FeaturedListingAdmin(admin.ModelAdmin):
    list_display = ('listing',)
    search_fields = ['listing__C_MLSNo']
    raw_id_fields = ['listing']


class CustomImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'image', 'listing')


class SellerCommentInline(admin.TabularInline):
    model = SellerComment
    form = SellerCommentInlineForm
    extra = 1


class CustomImageInline(admin.TabularInline):
    model = CustomImage

#
# class UnprocessedListingAdmin(admin.ModelAdmin):
#     model = UnprocessedListing


class SiteContentAdmin(admin.ModelAdmin):
    form = SiteContentAdminForm
    filter_horizontal = ('city', 'community', 'area', 'location_type')
    list_display = ('title', 'location_types', 'order', 'timestamp')
    list_filter = ('location_type', 'community', 'area')

    def location_types(self, instance):
        location_types = [i.name for i in instance.location_type.all()]
        return ', '.join(location_types)


class LocationTypeAdmin(admin.ModelAdmin):
    model = LocationType


class SellerCommentAdmin(ForeignKeyAutocompleteAdmin):
    form = SellerCommentAdminForm
    related_search_fields = {
       'listing': ('C_MLSNo',),
    }
    search_fields = ['listing__C_MLSNo',]
    list_display = ('listing', 'comment', 'selected_review',)
    fields = ('listing', 'comment', 'selected_review')

class ListingAdmin(admin.ModelAdmin):
    def changelist_view(self, request, extra_context=None):
        try:
            active = ListingStatus.objects.get(name='ACTIVE')
            if not request.GET.has_key('C_Status__id__exact'):
                q = request.GET.copy()
                q['C_Status__id__exact'] = str(active.id)
                request.GET = q
                request.META['QUERY_STRING'] = request.GET.urlencode()
        except:
            pass
        return super(ListingAdmin,self).changelist_view(request, extra_context=extra_context)

    list_display = ('C_MLSNo', 'C_Address', 'C_Area', 'C_AskPrice')
    search_fields = ['C_MLSNo', 'C_Address']
    filter_horizontal = ('C_HOAMENITIS', 'C_WTRFRNTMNT', 'C_VIEW',)
    list_filter = ('C_Community', 'C_Status',)
    inlines = [SellerCommentInline]
    fieldsets = [('Common Features', {'fields': ['C_NoBaths1', 'C_NoBeds', 'C_APPLIANCES',
                                                 'C_Architect', 'C_ApprxAcre', 'C_ApprxSqFt',
                                                 'C_DECKS', 'C_FIREPLACE', 'C_FLORCVRNGS',
                                                 'C_FOUNDATION', 'C_Furnished', 'C_GARAGPRKNG',
                                                 'C_GAS', 'C_GATED', 'C_GRENFEATRS', 'C_Garage',
                                                 'C_HEATING', 'C_LAUNDRY', 'C_LType', 'C_LotDims',
                                                 'C_AcresNo', 'C_NoShrOrUn', 'C_PRPRTYCNDT', 'C_BOrU',
                                                 'C_ROOF', 'C_Setting', 'C_SqFeet', 'C_Status',
                                                 'C_StatusCat', 'C_Stories', 'C_TOPOGRAPHY',
                                                 'C_VIEW', 'C_WATER', 'C_WTRFRNTMNT', 'C_WOODSTOVE',
                                                 'C_L_Type',
                                                 ],
                                      'classes': ['collapse']}),

                 ('Location', {'fields': ['C_Address', 'C_AddrDrctn', 'C_AddrNumber', 'C_AddrStreet',
                                          'C_Area', 'C_City', 'C_Community', 'C_County', 'C_LotNo',
                                          'C_State', 'C_SubArea', 'C_Address2', 'C_Zip', 'C_Zoning',
                                          'C_GeoLat', 'C_GeoLong', ],
                               'classes': ['collapse']}),

                 ('HOA', {'fields': ['C_HOAMENITIS', 'C_HOAAmt', 'C_HOAOpt', 'C_HOAPer', 'C_HOAMbr', ],
                          'classes': ['collapse']}),

                 ('Contact', {'fields': ['C_EMail', 'C_Agent', 'C_LA1Email', 'C_LA1FirstNm', 'C_LA1LastNam',
                                         'C_LA1Phon1Ar', 'C_LA1Phn1Nbr', 'C_LA1Phon2Ar', 'C_LA1Phn2Nbr',
                                         'C_LA1Phon3Ar', 'C_LA1Phn3Nbr', 'C_LA1Phon4Ar', 'C_LA1Phn4Nbr',
                                         'C_LA1Phon5Ar', 'C_LA1Phn5Nbr', 'C_LA1Phn1Dsc', 'C_LA1Phn2Dsc',
                                         'C_LA1Phn4Dsc', 'C_LA1Phn5Dsc', 'C_LA1Phn3Dsc', 'C_LA1WorksFr',
                                         'C_LO1OfcName', 'C_LO1Phn1Nbr', ],
                              'classes': ['collapse']}),

                 ('Finances', {'fields': ['C_N1stLnAmt', 'C_AskPrice', 'C_OrigPrice', 'C_OthDues',
                                          'C_OthDues2', 'C_PrcDate', 'C_SoldPrice', 'C_TaxDBID',
                                          'C_TaxPropID', 'C_InitFee', ],
                               'classes': ['collapse']}),

                 ('Others', {'fields': ['C_APN', 'C_Addendum', 'C_Age', 'C_AssocDoc', 'C_BONUSROOMS',
                                        'C_BldgName', 'C_Show1', 'C_Class', 'C_CloseDate', 'C_ConstPhse',
                                        'C_ContDate', 'C_DOM', 'C_DOMLS', 'C_DocDate', 'C_HNDCPDFRND', 'C_Show',
                                        'C_HowSold', 'C_IDXIncl', 'C_IDXvt', 'C_LstDate', 'C_MLSNo',
                                        'C_PhotoCount', 'C_PhotoDate', 'C_Remarks', 'C_StatDate',
                                        'C_StatusDetl', 'C_SubStatus', 'C_UpdtDate', 'C_WtrCo',
                                        'C_YrBlt', 'C_SaleOrRent', 'video_review', 'extra_photos',
                                        'image_thumbnail_270', 'image_thumbnail_100'],
                             'classes': ['collapse']})]


class WaterAmenitiesAdmin(admin.ModelAdmin):
    list_display = ('name', 'active',)


class HOAAmenitiesAdmin(admin.ModelAdmin):
    list_display = ('name', 'active',)

class ImgAdmin(admin.ModelAdmin):
    list_display = ('C_L_ListingID', 'has_thumbnail', 'has_large_thumbnail',)
    search_fields = ('C_L_ListingID',)

    def has_thumbnail(self, instance):
        if instance.C_THUMB:
            return True
        return False
    has_thumbnail.boolean = True
    has_thumbnail.allow_tags = True
    has_thumbnail.short_description = 'Thumbnail?'

    def has_large_thumbnail(self, instance):
        if instance.C_LARGE_THUMB:
            return True
        return False
    has_large_thumbnail.boolean = True
    has_large_thumbnail.allow_tags = True
    has_large_thumbnail.short_description = 'Large Thumbnail?'


class PropertyManagementAdminForm(forms.ModelForm):
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = PropertyManagement

class PropertyManagementImageInline(admin.TabularInline):
    """
      Property Management Image Inline
    """
    model = PropertyManagementImage    
    extra = 1

class PropertyManagementAdmin(admin.ModelAdmin):
  """
    Property Management Admin
  """
  list_display = ['description']
  form = PropertyManagementAdminForm
  inlines = [ PropertyManagementImageInline, ]


class BusinessDirectoryAdminForm(forms.ModelForm):
    '''
      Admin form for BusinessDirectory model
    '''
    address = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = BusinessDirectory

class BusinessTypeAdminForm(forms.ModelForm):
    '''
      Admin form for BusinessType model
    '''
    description = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 15}), required=False)
    class Meta:
        model = BusinessType

class BusinessTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    search_fields = ['name']    
    form = BusinessTypeAdminForm

class BusinessDirectoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'type_of_business', 'phone','email','website', 'areas','cities','communities')
    search_fields = ['type_of_business__name']    
    list_filter = ('type_of_business', 'city', 'community', 'area',)   
    filter_horizontal = ('city', 'community', 'area', )
    form = BusinessDirectoryAdminForm

    def areas(self, obj):
        return ",\n".join([p.name for p in obj.area.all()])

    def cities(self, obj):
        return ",\n".join([p.name for p in obj.city.all()])

    def communities(self, obj):
        return ",\n".join([p.name for p in obj.community.all()])


admin.site.register(Listing, ListingAdmin)
admin.site.register(FeaturedListing, FeaturedListingAdmin)
admin.site.register(CustomImage, CustomImageAdmin)
admin.site.register(PropertyManagement, PropertyManagementAdmin)
admin.site.register(Area, AreaAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Setting, SettingAdmin)
admin.site.register(View, ViewAdmin)
admin.site.register(LocationType, LocationTypeAdmin)
admin.site.register(SiteContent, SiteContentAdmin)
admin.site.register(PriceChange, PriceChangeAdmin)
admin.site.register(VendorType, VendorTypeAdmin)
admin.site.register(Vendor, VendorAdmin)
admin.site.register(BusinessType, BusinessTypeAdmin)
admin.site.register(BusinessDirectory, BusinessDirectoryAdmin)
# admin.site.register(ListingRaw2, ListingRawAdmin)
admin.site.register(ListingStatus)
admin.site.register(Community, CommunityAdmin)
admin.site.register(SellerComment, SellerCommentAdmin)
admin.site.register(HOAAmenities, HOAAmenitiesAdmin)
admin.site.register(WaterAmenities, WaterAmenitiesAdmin)
admin.site.register(Type)
admin.site.register(Img, ImgAdmin)
admin.site.register(HOA)
