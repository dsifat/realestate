import numpy

from decimal import Decimal
from datetime import datetime, timedelta, date

from django.db.models import Q, Avg, Max, Min, get_model


def get_new_listings(obj):
    listings = obj.listing_set.prefetch_related('C_City', 'C_Community', 'C_Area', 'C_Setting',)\
                    .filter(C_Status__name='ACTIVE')\
                    .exclude(Q(C_Community__name='SurroundingArea')
                            |Q(C_Community__name='Sierra County'))\
                    .only('C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice',
                         'C_Address', 'C_MLSNo', 'C_NoBeds', 'C_NoBaths1',
                         'C_Garage', 'C_SqFeet', 'C_YrBlt', 'C_LstDate', 'C_GeoLat', 'C_GeoLong')\
                    .order_by('-C_LstDate')[:15]
    return listings

def get_pending(obj):
    listings = obj.listing_set.prefetch_related('C_City', 'C_Community', 'C_Area', 'C_Setting',)\
                    .filter(C_Status__name='PENDING')\
                    .exclude(Q(C_Community__name='SurroundingArea')
                            |Q(C_Community__name='Sierra County'))\
                    .only('C_Community', 'C_NoBeds', 'C_SqFeet', 'C_AskPrice',
                         'C_Address', 'C_MLSNo', 'C_NoBeds', 'C_NoBaths1',
                         'C_Garage', 'C_SqFeet', 'C_YrBlt', 'C_LstDate', 'C_GeoLat', 'C_GeoLong')\
                    .order_by('-C_LstDate')[:15]
    return listings

def get_recently_sold(obj):
    listings = obj.listing_set.prefetch_related('C_City', 'C_Community', 'C_Area', 'C_Setting',)\
                    .filter(C_Status__name='SOLD')\
                    .exclude(Q(C_Community__name='SurroundingArea')
                            |Q(C_Community__name='Sierra County'))\
                    .only('C_Community', 'C_NoBeds', 'C_SqFeet', 'C_SoldPrice',
                         'C_Address', 'C_MLSNo', 'C_NoBeds', 'C_NoBaths1',
                         'C_Garage', 'C_SqFeet', 'C_YrBlt', 'C_CloseDate', 'C_GeoLat', 'C_GeoLong')\
                    .order_by('-C_CloseDate')[:15]
    return listings

def get_statistics_data(obj):
    from mls.models import PriceChange
    def median_value(queryset,term):
        count = queryset.count()
        return queryset.values_list(term, flat=True).order_by(term)[int(round(count/2))]

    active_properties = obj.listing_set.filter(C_Status__name='ACTIVE')
    distressed_sales = active_properties.filter(C_SubStatus__in=['REO', 'Short Sale',])
    prices = active_properties.aggregate(Min('C_AskPrice'), Max('C_AskPrice'), Avg('C_AskPrice'))
    try:
        median_price = median_value(active_properties,'C_AskPrice')
    except IndexError:
        median_price = 0
    market_days = [(datetime.now().date() - listing.C_LstDate.date()).days for listing in active_properties]

    sold_start_date = date.today() - timedelta(days=180)
    sold_properties = obj.listing_set\
                    .filter(C_Status__name='SOLD', C_CloseDate__range=[sold_start_date, date.today()])\
                    .exclude(Q(C_Community__name='SurroundingArea')|Q(C_Community__name='Sierra County'))
    sold_properties_prices = sold_properties.aggregate(Min('C_AskPrice'), Max('C_AskPrice'))
    sold_market_days = [(listing.C_CloseDate.date() - listing.C_LstDate.date()).days for listing in sold_properties]
    try:
        ask_price = 0
        sold_price = 0
        for sprice in sold_properties.values_list('C_AskPrice', 'C_SoldPrice'):
            ask_price += sprice[0]
            sold_price += Decimal(sprice[1])
        sold_price_percentage = float(sold_price/ask_price) * 100
    except:
        sold_price_percentage = 0

    try:
        cash_sales = float(sold_properties.filter(C_HowSold__icontains='CASH').count()/float(sold_properties.count())) * 100
    except:
        cash_sales = 0

    price_changed = PriceChange.objects.filter(listing__id__in=active_properties.values_list('id', flat=True))

    total_price_change = 0
    for pc in price_changed:
        total_price_change += (pc.prev_price - pc.new_price)
    try:
        price_change_avg = float(total_price_change/price_changed.count())
    except:
        price_change_avg = 0

    price_per_sqr_feet = []
    for listing in active_properties:
        try:
            price_per_sqr_feet.append(listing.price_per_sqr_feet())
        except:
            pass
    if price_per_sqr_feet:
        max_price_per_sqr_feet = max(price_per_sqr_feet)
        min_price_per_sqr_feet = min(price_per_sqr_feet)
    else:
        max_price_per_sqr_feet = 0
        min_price_per_sqr_feet = 0


    return {
        'prices':prices, 'median_price': median_price, 'avg_market_days': numpy.mean(market_days),
        'sold_properties_count': sold_properties.count(), 'sold_properties_prices': sold_properties_prices,
        'avg_sold_market_days': numpy.mean(sold_market_days), 'sold_price_percentage': sold_price_percentage,
        'cash_sales': cash_sales, 'price_change_count': price_changed.count(),
        'price_change_avg': price_change_avg, 'distressed_sales': distressed_sales.count(),
        'max_price_per_sqr_feet': max_price_per_sqr_feet, 'min_price_per_sqr_feet': min_price_per_sqr_feet,
    }



