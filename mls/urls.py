from django.conf.urls import *


urlpatterns = patterns('mls.views',
    #url(r'^$', 'listing_index', name='listing_index'),
    url(r'^ajax-contact-us/$', 'ajax_contact_us'),
    #url(r'^detailed-property/(?P<mls_no>\d+)/$', 'detailed_property', name='detailed_property'),
    #url(r'^featured-property$', 'featured_property', name='featured_property'),
    #url(r'^compare-property$', 'compare_property', name='compare_property'),

    #url(r'^search/luxury$', 'luxury_properties', name='luxury_properties'),
    #community
    #url(r'^search/community$', 'community', name='community'),
    #url(r'^search/community/(?P<community_slug>[\w-]+)$', 'view_community', name='view_community'),
    url(r'^search/community/(?P<community_slug>[\w-]+)$', 'redirect_community', name='redirect_community'),
    #city
    #url(r'^search/city$', 'city', name='city'),
    url(r'^search/city/(?P<city_slug>[\w-]+)$', 'redirect_city', name='redirect_city'),
    #area
    #url(r'^search/area$', 'area', name='area'),
    #url(r'^search/area/(?P<area_slug>[\w-]+)$', 'view_area', name='view_area'),
    #setting
    #url(r'^search/setting$', 'setting', name='setting'),
    url(r'^search/setting/(?P<setting_slug>[\w-]+)$', 'redirect_setting', name='redirect_setting'),
    #search
    #url(r'^search/', include('haystack.urls')),
    #url(r'^search$', 'search_result', name='search_result'),
    #url(r'^request-info/(?P<mls_no>\d+)$', 'request_info', name='request_info'),
    #statistics
    # url(r'^statistics$', 'statistics', name='statistics'),
    #url(r'^price-reductions$', 'price_reductions', name='price_reductions'),
    #url(r'^sold$', 'sold', name='realestate_sold'),

)
