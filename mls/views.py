
import pickle
import redis
import json
from datetime import datetime, timedelta, date
from itertools import chain
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.validators import email_re
from django.core.mail.message import EmailMultiAlternatives
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.db.models import Q
from django.db.models.sql.datastructures import EmptyResultSet
from django.http import Http404, HttpResponseRedirect, HttpResponse, HttpResponsePermanentRedirect
from django.shortcuts import get_list_or_404, get_object_or_404
from django.utils import simplejson as json
from django.utils.translation import ugettext_lazy as _
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from common.utils import render_response, get_or_none, has_full_content
from django.contrib.sites.models import Site
from cblt.forms import ContactUsForm
from django.template.loader import render_to_string
from photologue.models import Gallery
from haystack.query import SearchQuerySet
from cblt.utils import get_map_data, get_plottable_content, get_map_details, get_years_of_data,get_number_of_sales_last_days
from mls.models import FeaturedListing, Listing, Img, SiteContent, \
                        Community, Setting, Area, City, PriceChange, LocationType, BigTable, HOA, Vendor

from mailer import send_html_mail
from filebrowser.base import FileListing, FileObject
from filebrowser.settings import MEDIA_ROOT




today = datetime.today()


def listing_index(request, template='mls/property_listing.html'):
    """
    returns the most current listing on the professional listing page
    """
    context = {}
    property_listing = Listing.objects.filter(C_Status__name='ACTIVE').order_by('-C_AskPrice')
    paginator = Paginator(property_listing, 10)

    context['related_properties'] = get_related_content()

    page = request.GET.get('page')

    try:
        listings = paginator.page(page)
    except PageNotAnInteger:
        listings = paginator.page(1)
    except EmptyPage:
        listings = paginator.page(paginator.num_pages)

    context['listings'] = listings

    return render_response(request, template, context)


def detailed_property(request, community_slug, area_slug, mls_no, address_slug , template='mls/detailed_property.html', context={}):
    """
    returns a detailed view of the listing
    """
    r = redis.StrictRedis(host='localhost', port=6379, db=0)    
    
    if not r.get(mls_no):
        listing_instance = get_object_or_404(Listing.objects.prefetch_related('C_WTRFRNTMNT','C_HOAMENITIS').select_related('C_Community','C_City'), \
                                C_MLSNo=mls_no, C_Community__slug=community_slug, C_Area__slug=area_slug)
        pickled_object = pickle.dumps(listing_instance)        
        r.set(mls_no, pickled_object)    
    else:
        print 'Yes , Got the cache !!'
        unpacked_object = pickle.loads(r.get(mls_no))                
        listing_instance = unpacked_object  


    context = {}        
    listing = listing_instance
    context['listing'] = listing

    context['bt_statistics'] = BigTable.objects.get(function='statistics', scope='Community_'+str(listing.C_Community))
    context['bt_price_reduced'] = BigTable.objects.get(function='price_reduced', scope='Community_'+str(listing.C_Community))
    context['bt_recent_sold'] = BigTable.objects.get(function='recent_sold', scope='Community_'+str(listing.C_Community))
    context['recent_sold'] = listing.C_Community.recently_sold()
    context['hoa_amenities'] = [hoa.name for hoa in context['listing'].C_HOAMENITIS.all()]
    context['waterfront_amenities'] = [waterfront.name for waterfront in context['listing'].C_WTRFRNTMNT.all()]
    context['mls_imgs'] = []
    context['prop_loc'] = get_map_data([context['listing']])
    context['extra_img'] = []
    context['extra_img_thumbs'] = []
    context['hoa_photos'] = []
    context['hoa_photos_thumbs'] = []
    context['price_reductions'] = listing.price_reductions()
    context['similar_properties'] = listing.similar_properties()
    context['bt_area_statistics'] = BigTable.objects.get(function='statistics', scope='Area_'+str(listing.C_Area))

    if listing.extra_photos:
        filelisting = FileListing(listing.extra_photos.path_full)

        for image in filelisting.listing():
            fo = FileObject('/media/' + listing.extra_photos.path + '/' + image)
            context['extra_img'].append(fo.folder.replace('uploads','versions') + '/' + fo.version_name("large"))
            context['extra_img_thumbs'].append(fo.folder.replace('uploads','versions') + '/' + fo.version_name("admin_thumbnail"))

        context['extra_img'].sort()
        context['extra_img_thumbs'].sort()

    context['mls_imgs'] = Img.objects.filter(C_L_ListingID=context['listing'].C_MLSNo)
    #context['related_content'] =  SiteContent.get_content_by_loctype(community=context['listing'].C_Community, area = context['listing'].C_Area)

    #Get Vendor list in community/area
    #context['vendor_list'] = Vendor.get_vendorlist_by_type_in_community_area(community=context['listing'].C_Community, area = context['listing'].C_Area)


    # Recent sales also used for similar sales in our comments tab
    similar_sales = None
    if context['listing'].C_Community.name.lower() != 'surroundingarea':
        context['recent_sales'] = context['listing'].get_recent_sales()
        similar_sales = context['recent_sales'][:2]
    context['reviewed_comments'] = Listing.get_reviewed_listing(review_type='comments')
    context['reviewed_photos'] = Listing.get_reviewed_listing(review_type='photos', listing=context['listing'])
    
    if context['price_reductions']:
        price_reduction = context['listing'].C_OrigPrice - context['price_reductions'][-1].new_price         
        meta_description_text = u"%s listed on %s at %s.Price reduced by %s.Sales comps include %s sold for %s on %s and %s sold for %s on %s"
        meta_description = meta_description_text %(context['listing'].get_property_full_address()+ " "+ context['listing'].C_Zip,
                                            context['listing'].C_LstDate.strftime('%m/%d/%y'),
                                            '${:,d}'.format(int(context['listing'].C_OrigPrice)) if context['listing'].C_OrigPrice else 0,                                            
                                            '${:,d}'.format(int(price_reduction)),
                                            similar_sales[0].C_Address if similar_sales else '',
                                            '${:,d}'.format(int(similar_sales[0].C_AskPrice)) if similar_sales else '',
                                            similar_sales[0].C_CloseDate.strftime('%m/%d/%y') if similar_sales else '',
                                            similar_sales[1].C_Address if similar_sales else '',
                                            '${:,d}'.format(int(similar_sales[1].C_AskPrice)) if similar_sales else '',
                                            similar_sales[1].C_CloseDate.strftime('%m/%d/%y') if similar_sales else '',)
    else:
        meta_description_text = u"%s listed on %s at %s.No price reduced yet.Sales comps include %s sold for %s on %s and %s sold for %s on %s"
        meta_description = meta_description_text %(context['listing'].get_property_full_address() + " "+ context['listing'].C_Zip,
                                            context['listing'].C_LstDate.strftime('%m/%d/%y'),
                                            '${:,d}'.format(int(context['listing'].C_OrigPrice)) if context['listing'].C_OrigPrice else 0,                                           
                                            similar_sales[0].C_Address if similar_sales else '',
                                            '${:,d}'.format(int(similar_sales[0].C_AskPrice)) if similar_sales else '',
                                            similar_sales[0].C_CloseDate.strftime('%m/%d/%y') if similar_sales else '',
                                            similar_sales[1].C_Address if similar_sales else '',
                                            '${:,d}'.format(int(similar_sales[1].C_AskPrice)) if similar_sales else '',
                                            similar_sales[1].C_CloseDate.strftime('%m/%d/%y') if similar_sales else '',) 
    
    context['seo_tags'] = {}
    context['seo_tags']['description'] = meta_description 
    context['seo_tags']['title'] = context['listing'].get_property_full_address()+ " "+ context['listing'].C_Zip + " real estate listing " + context['listing'].C_MLSNo

    
    


    return render_response(request, template, context)


def property_related_content(request):  
    """
        Generate All Site content on the fly using AJAX CALL 
        @param : request
        @return : html 
    """  
    html = ""
    r = redis.StrictRedis(host='localhost', port=6379, db=0)    
    if request.method == "POST":
        mls_no = request.POST['mls_no']
        if r.get(mls_no):
            print 'Yes , Got the cache !!'
            unpacked_object = pickle.loads(r.get(mls_no))                
            listing_instance = unpacked_object         
            data = SiteContent.get_content_by_loctype(community=listing_instance.C_Community, area = listing_instance.C_Area)   
            html = render_to_string('mls/related-contents.html', {'related_content': data})
    return HttpResponse(html)
    

def property_redirect(request, community_slug, area_slug, mls_no):
    '''
        Redirect property page to newly created property page url 
    '''
    obj = get_object_or_404(Listing, C_MLSNo=mls_no)
    return HttpResponsePermanentRedirect(obj.get_detailed_property_url())


def detailed_property_redirect(request, mls_no):
    obj = get_object_or_404(Listing, C_MLSNo=mls_no)
    return HttpResponsePermanentRedirect(obj.get_detailed_property_url())

def detailed_property_redirect_2(request, mls_no):
    obj = get_object_or_404(Listing, C_MLSNo=mls_no)
    return HttpResponsePermanentRedirect(obj.get_detailed_property_url())

def redirect_community(request, community_slug):
    return HttpResponseRedirect(reverse('view_community', args=(community_slug,)))

def view_community(request, community_slug, template='mls/community.html', context={}):
    """
    returns a plotted map of listings
    """
    try:
        community = Community.objects.get(slug=community_slug)
        bt_statistics = BigTable.objects.get(function='statistics', scope='Community_'+str(community))
        bt_price_reduced = BigTable.objects.get(function='price_reduced', scope='Community_'+str(community))
        query_dict = request.GET
        sqs = SearchQuerySet()
        bedroom_val = query_dict.get('bedroom', 'Any')
        bathroom_val = query_dict.get('bathroom', 'Any')
        area_val = query_dict.getlist('area', '')
        community_val = community.name
        price_range_max = query_dict.get('pricerangemax','nomax')
        price_range_min = query_dict.get('pricerangemin','nomin')
        status_val = query_dict.getlist('status', ['active',])
        property_type_val = query_dict.getlist('property_type','')

        sqs = sqs.filter(C_Status__iexact__in=status_val).exclude(C_Address__isnull=True).order_by('-C_AskPrice')

        sqs = sqs.filter(C_Community=community_val)

        if len(property_type_val) > 0 and not 'All' in property_type_val:
            sqs = sqs.filter(C_L_Type__in=property_type_val)

        if len(area_val) > 0 and not 'All' in area_val:
            sqs = sqs.filter(C_Area__in=area_val)

        if bedroom_val != 'Any':
            sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

        if bathroom_val != 'Any':
            sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

        price_max = price_range_max
        price_min = price_range_min
        if price_range_max != 'nomax'\
        or price_range_min != 'nomin':

            if price_range_max == 'nomax':
                price_max = 100000000
            if price_range_min == 'nomin':
                price_min = 0
            try:
                sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
            except ValueError:
                pass

        queries_without_page = request.GET.copy()
        if queries_without_page.has_key('page'):
            del queries_without_page['page']

        paginator = Paginator(sqs, 12)
        page = request.GET.get('page')

        try:
            sqs = paginator.page(page)
        except PageNotAnInteger:
            sqs = paginator.page(1)
        except EmptyPage:
            sqs = paginator.page(paginator.num_pages)

        context.update({
            'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
            'area_val': area_val, 'community_val': community_val,
            'price_range_max': price_range_max, 'price_range_min': price_range_min,
            'status_val': status_val, 'queries': queries_without_page,
            'property_type_val': property_type_val, 'properties': sqs,
            'community': community, 'bt_statistics': bt_statistics,
            'bt_price_reduced':bt_price_reduced
        })

        # Vendor list in community pages
        
        number_of_sales_six_month = get_number_of_sales_last_days(6, community = community)
        number_of_sales_total = get_number_of_sales_last_days(int(get_years_of_data())*12, community = community)

        
        context['vendor_list'] = Vendor.get_vendorlist_by_type_in_community_area(community = community)
        context['seo_tags'] = {}
        #context['seo_tags']['description'] = bt_statistics.content
        context['seo_tags']['description'] = "More data, %s yrs of comps, our opinions of the homes. %s active %s real estate listings. %s sales past 180 days. %s years / %s sales"%\
                                                (get_years_of_data(),str(sqs.paginator.count),community.name , number_of_sales_six_month, get_years_of_data(), number_of_sales_total)
        context['seo_tags']['title'] = community.name + ' Real Estate +'+ str(sqs.paginator.count) +' listings'

        return render_response(request, template, context)
    except Community.DoesNotExist:
        raise Http404

def view_area(request, area_slug, template='mls/area.html', context={}):
    """
    returns a plotted map of listings
    """
    try:
        area = Area.objects.get(slug=area_slug)
        bt_statistics = BigTable.objects.get(function='statistics', scope='Area_'+str(area))
        bt_price_reduced = BigTable.objects.get(function='price_reduced', scope='Area_'+str(area))
        query_dict = request.GET
        sqs = SearchQuerySet()

        bedroom_val = query_dict.get('bedroom', '')
        bathroom_val = query_dict.get('bathroom', '')
        acreage_val = query_dict.get('acreage','Any')
        setting_val = query_dict.getlist('setting', '')
        area_val = area.name
        hoa_amenities_val = query_dict.getlist('hoa_amenities','')
        waterfront_amenities_val = query_dict.getlist('waterfront_amenities','')
        views_val = query_dict.getlist('views','')
        price_range_max = query_dict.get('pricerangemax','nomax')
        price_range_min = query_dict.get('pricerangemin','nomin')
        mlsno_val = query_dict.get('mlsno','')
        status_val = query_dict.getlist('status',['active',])
        property_type_val = query_dict.getlist('property_type','')

        sqs = sqs.filter(C_Status__iexact__in=status_val).exclude(C_Address__isnull=True).order_by('-C_AskPrice')

        sqs = sqs.filter(C_Area=area_val)

        if len(property_type_val) > 0 and not 'All' in property_type_val:
            sqs = sqs.filter(C_L_Type__in=property_type_val)

        if bedroom_val != 'Any':
            sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

        if acreage_val != 'Any':
            sqs = sqs.filter(C_Acreage__gte=acreage_val)

        if len(hoa_amenities_val) > 0 and not 'Any' in hoa_amenities_val:
            sqs = sqs.filter(C_HOAMENITIS__in=hoa_amenities_val)

        if len(waterfront_amenities_val) > 0 and not 'Any' in waterfront_amenities_val:
            sqs = sqs.filter(C_WTRFRNTMNT__in=waterfront_amenities_val)

        if len(views_val) > 0 and not 'Any' in views_val:
            sqs = sqs.filter(C_VIEW__in=views_val)

        if bathroom_val != 'Any':
            sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

        if len(setting_val) > 0 and not 'All' in setting_val:
            sqs = sqs.filter(C_Setting__in=setting_val)

        price_max = price_range_max
        price_min = price_range_min
        if price_range_max != 'nomax'\
        or price_range_min != 'nomin':

            if price_range_max == 'nomax':
                price_max = 100000000
            if price_range_min == 'nomin':
                price_min = 0
            try:
                sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
            except ValueError:
                pass

        if len(mlsno_val) > 0:
            sqs = sqs.filter(C_MLSNo=mlsno_val)

        queries_without_page = request.GET.copy()
        if queries_without_page.has_key('page'):
            del queries_without_page['page']

        paginator = Paginator(sqs, 12)
        page = request.GET.get('page')

        try:
            sqs = paginator.page(page)
        except PageNotAnInteger:
            sqs = paginator.page(1)
        except EmptyPage:
            sqs = paginator.page(paginator.num_pages)

        context.update({
            'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
            'setting_val': setting_val, 'area_val': area_val,
            'hoa_amenities_val': hoa_amenities_val, 'area': area,
            'waterfront_amenities_val': waterfront_amenities_val, 'views_val': views_val,
            'price_range_max': price_range_max, 'price_range_min': price_range_min,
            'mlsno_val': mlsno_val, 'status_val': status_val,
            'property_type_val': property_type_val, 'properties': sqs,
            'area_selected': area_slug, 'queries': queries_without_page,
            'acreage_val': acreage_val,
            'bt_statistics': bt_statistics, 'bt_price_reduced': bt_price_reduced,
        })

        days_six_month = 6*30
        days_total = int(get_years_of_data())*12*30
        number_of_sales_six_month = Listing.objects.filter(C_CloseDate__range = (today-timedelta(days=days_six_month), today), 
                                      C_Status__name__iexact="SOLD", C_Area_id=area.id).count()
        number_of_sales_total = Listing.objects.filter(C_CloseDate__range = (today-timedelta(days=days_total), today), 
                                      C_Status__name__iexact="SOLD", C_Area_id=area.id).count()
        # Vendor list in area pages
        context['vendor_list'] = Vendor.get_vendorlist_by_type_in_community_area(area = area)

        context['seo_tags'] = {}
        context['seo_tags']['description'] = "More data, %s yrs of comps, our opinions of the homes. %s active %s real estate listings. %s sales past 180 days. %s years / %s sales"%\
                                                (get_years_of_data(),str(sqs.paginator.count),area.name , number_of_sales_six_month, get_years_of_data(), number_of_sales_total)
        context['seo_tags']['title'] = str(area.name).title() + ' Real Estate +'+ str(sqs.paginator.count) +' listings'
        return render_response(request, template, context)
    except Area.DoesNotExist:
        raise Http404

def redirect_city(request, city_slug):
    return HttpResponseRedirect(reverse('view_city', args=(city_slug,)))

def view_city(request, city_slug, template='mls/city.html', context={}):
    """
    returns a plotted map of listings
    """
    days_six_month = 6*30
    days_total = int(get_years_of_data())*12*30
    try:
        city = City.objects.exclude(slug='none').get(slug=city_slug)        
        number_of_sales_six_month = Listing.objects.filter(C_CloseDate__range = (today-timedelta(days=days_six_month), today), 
                                      C_Status__name__iexact="SOLD", C_City_id=city.id).count()
        number_of_sales_total = Listing.objects.filter(C_CloseDate__range = (today-timedelta(days=days_total), today), 
                                      C_Status__name__iexact="SOLD", C_City_id=city.id).count()
        bt_statistics = BigTable.objects.get(function='statistics', scope='City_'+str(city))
        bt_price_reduced = BigTable.objects.get(function='price_reduced', scope='City_'+str(city))
        query_dict = request.GET
        sqs = SearchQuerySet()
        bedroom_val = query_dict.get('bedroom', 'Any')
        bathroom_val = query_dict.get('bathroom', 'Any')
        area_val = query_dict.getlist('area', '')
        city_val = city.name
        price_range_max = query_dict.get('pricerangemax','nomax')
        price_range_min = query_dict.get('pricerangemin','nomin')
        status_val = query_dict.getlist('status', ['active',])
        property_type_val = query_dict.getlist('property_type','')

        sqs = sqs.filter(C_Status__iexact__in=status_val).exclude(C_Address__isnull=True).order_by('-C_AskPrice')

        sqs = sqs.filter(C_City=city_val)

        if len(property_type_val) > 0 and not 'All' in property_type_val:
            sqs = sqs.filter(C_L_Type__in=property_type_val)

        if len(area_val) > 0 and not 'All' in area_val:
            sqs = sqs.filter(C_Area__in=area_val)

        if bedroom_val != 'Any':
            sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

        if bathroom_val != 'Any':
            sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

        price_max = price_range_max
        price_min = price_range_min
        if price_range_max != 'nomax'\
        or price_range_min != 'nomin':

            if price_range_max == 'nomax':
                price_max = 100000000
            if price_range_min == 'nomin':
                price_min = 0
            try:
                sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
            except ValueError:
                pass

        queries_without_page = request.GET.copy()
        if queries_without_page.has_key('page'):
            del queries_without_page['page']

        paginator = Paginator(sqs, 12)
        page = request.GET.get('page')

        try:
            sqs = paginator.page(page)
        except PageNotAnInteger:
            sqs = paginator.page(1)
        except EmptyPage:
            sqs = paginator.page(paginator.num_pages)

        context.update({
            'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
            'area_val': area_val, 'city_val': city_val,
            'price_range_max': price_range_max, 'price_range_min': price_range_min,
            'status_val': status_val, 'queries': queries_without_page,
            'property_type_val': property_type_val, 'properties': sqs,
            'city': city, 'bt_statistics': bt_statistics, 'bt_price_reduced': bt_price_reduced
        })

        # Vendor list in City pages
        

        context['vendor_list'] = Vendor.get_vendorlist_by_type_in_community_area(city = city)

        context['seo_tags'] = {}
        context['seo_tags']['description'] = "More data, %s yrs of comps, our opinions of the homes. %s active %s real estate listings. %s sales past 180 days. %s years / %s sales"%\
                                                (get_years_of_data(),str(sqs.paginator.count),city.name , number_of_sales_six_month, get_years_of_data(), number_of_sales_total)

        context['seo_tags']['title'] = city.name + ' Real Estate +'+ str(sqs.paginator.count) +' listings'

        return render_response(request, template, context)
    except City.DoesNotExist:
        raise Http404

def redirect_setting(request, setting_slug):
    return HttpResponseRedirect(reverse('view_setting', args=(setting_slug,)))


def view_setting(request, setting_slug, template='mls/setting.html', context={}):
    """
    returns a plotted map of listings
    """
    try:
        lakefront_setting_title = None
        setting = Setting.objects.get(slug=setting_slug)
        bt_statistics = BigTable.objects.get(function='statistics', scope__iexact='Setting_'+str(setting))
        bt_price_reduced = BigTable.objects.get(function='price_reduced', scope='Setting_'+str(setting))
        query_dict = request.GET
        sqs = SearchQuerySet()
        bedroom_val = query_dict.get('bedroom', 'Any')
        bathroom_val = query_dict.get('bathroom', 'Any')
        community_val = query_dict.getlist('community', '')
        area_val = query_dict.getlist('area', '')
        setting_val = setting.name
        price_range_max = query_dict.get('pricerangemax','nomax')
        price_range_min = query_dict.get('pricerangemin','nomin')
        status_val = query_dict.getlist('status', ['active',])
        property_type_val = query_dict.getlist('property_type','')
        waterfront_amenities_val = query_dict.getlist('waterfront_amenities','')

        sqs = sqs.filter(C_Status__iexact__in=status_val).exclude(C_Address__isnull=True).order_by('-C_AskPrice')

        sqs = sqs.filter(C_Setting=setting_val)

        if len(property_type_val) > 0 and not 'All' in property_type_val:
            sqs = sqs.filter(C_L_Type__in=property_type_val)

        if len(community_val) > 0 and not 'All' in community_val:
            sqs = sqs.filter(C_Community__in=community_val)

        if len(area_val) > 0 and not 'All' in area_val:
            sqs = sqs.filter(C_Area__in=area_val)

        if bedroom_val != 'Any':
            sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

        if bathroom_val != 'Any':
            sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

        if len(waterfront_amenities_val) > 0 and not 'All' in waterfront_amenities_val:
            sqs = sqs.filter(C_WTRFRNTMNT__in=waterfront_amenities_val)

        price_max = price_range_max
        price_min = price_range_min
        if price_range_max != 'nomax'\
        or price_range_min != 'nomin':

            if price_range_max == 'nomax':
                price_max = 100000000
            if price_range_min == 'nomin':
                price_min = 0
            try:
                sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
            except ValueError:
                pass

        queries_without_page = request.GET.copy()
        if queries_without_page.has_key('page'):
            del queries_without_page['page']

        paginator = Paginator(sqs, 12)
        page = request.GET.get('page')

        try:
            sqs = paginator.page(page)
        except PageNotAnInteger:
            sqs = paginator.page(1)
        except EmptyPage:
            sqs = paginator.page(paginator.num_pages)

        if setting.name=="Lakefront":
            lakefront_setting_title = "Lake Tahoe "+str(setting)

        context.update({
            'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
            'area_val': area_val, 'community_val': community_val,
            'price_range_max': price_range_max, 'price_range_min': price_range_min,
            'status_val': status_val, 'queries': queries_without_page,
            'property_type_val': property_type_val, 'properties': sqs,
            'lakefront_setting_title' : lakefront_setting_title,
            'setting': setting, 'waterfront_amenities_val': waterfront_amenities_val,
            'bt_statistics': bt_statistics, 'bt_price_reduced':bt_price_reduced
        })
        return render_response(request, template, context)
    except Setting.DoesNotExist:
        raise Http404

def featured_property(request, template='mls/featured_properties.html', context={}):
    """
    returns a list of featured properties
    """
    fl = FeaturedListing.objects.select_related('Listing').filter()
    property_listing = []

    premium = Listing.objects.filter(C_Status__name='ACTIVE')[:10]

    for featured_listing in fl:
        property_listing.append(featured_listing.listing)

    # combine premium and featured properties
    property_listing = property_listing + list(premium)

    paginator = Paginator(property_listing, 12)
    plot_pagination = Paginator(get_map_data(property_listing), 12)

    page = request.GET.get('page')

    try:
        listings = paginator.page(page)
        listing_plots = plot_pagination.page(page)

    except PageNotAnInteger:
        listings = paginator.page(1)
        listing_plots = plot_pagination.page(1)
    except EmptyPage:
        listings = paginator.page(paginator.num_pages)
        listing_plots = plot_pagination.page(plot_pagination.num_pages)

    context['listings'] = listings
    context['listing_plots'] = listing_plots

    return render_response(request, template, context)


def compare_property(request, template='mls/compare_property.html', context={}):
    """
    returns a list of featured properties
    """
    return render_response(request, template, context)


def search_result(request, template='mls/advanced-search.html'):
    """
    returns a list of featured properties
    """
    query_dict = request.GET
    sqs = SearchQuerySet()
    #featured_listing = FeaturedListing.objects.select_related('Listing').filter()

    bedroom_val = query_dict.get('bedroom', 'Any')
    bathroom_val = query_dict.get('bathroom', 'Any')
    acreage_val = query_dict.get('acreage', 'Any')
    setting_val = query_dict.getlist('setting', '')
    area_val = query_dict.getlist('area', '')
    community_val = query_dict.getlist('community', '')
    hoa_amenities_val = query_dict.getlist('hoa_amenities','')
    waterfront_amenities_val = query_dict.getlist('waterfront_amenities','')
    settings_val = query_dict.getlist('settings','')
    views_val = query_dict.getlist('views','')
    price_range_max = query_dict.get('pricerangemax','nomax')
    price_range_min = query_dict.get('pricerangemin','nomin')
    keyword_val = query_dict.get('keyword','')
    status_val = query_dict.getlist('status', ['active',])
    property_type_val = query_dict.getlist('property_type','')
    query_json = json.dumps(dict(query_dict.lists()))
    print property_type_val
    print len(sqs)

    sqs = sqs.filter(C_Status__iexact__in=status_val).order_by('-C_AskPrice')
    print 'status'
   # print len()

    if len(property_type_val) > 0 and not 'All' in property_type_val:
        sqs = sqs.filter(C_L_Type__in=property_type_val)
        
    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__in=community_val)

    if len(area_val) > 0 and not 'All' in area_val:
        sqs = sqs.filter(C_Area__in=area_val)

    if bedroom_val != 'Any':
        sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

    if acreage_val != 'Any':
        sqs = sqs.filter(C_Acreage__gte=acreage_val)

    if len(hoa_amenities_val) > 0 and not 'All' in hoa_amenities_val:
        sqs = sqs.filter(C_HOAMENITIS__in=hoa_amenities_val)

    if len(waterfront_amenities_val) > 0 and not 'All' in waterfront_amenities_val:
        sqs = sqs.filter(C_WTRFRNTMNT__in=waterfront_amenities_val)

    if len(settings_val) > 0 and not 'All' in settings_val:
        sqs = sqs.filter(C_Setting__in=settings_val)

    if len(views_val) > 0 and not 'All' in views_val:
        sqs = sqs.filter(C_VIEW__in=views_val)

    if bathroom_val != 'Any':
        sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

    if len(setting_val) > 0 and not 'All' in setting_val:
        sqs = sqs.filter(C_Setting__in=setting_val)

    if price_range_max != 'nomax'\
    or price_range_min != 'nomin':

        if price_range_max == 'nomax':
            price_range_max = 100000000
        if price_range_min == 'nomin':
            price_range_min = 0
        try:
            sqs = sqs.filter(C_AskPrice__range=[float(price_range_min), float(price_range_max)])
        except ValueError:
            pass

    if len(keyword_val) > 0:
        try:
            val = int(keyword_val)
            mls_results = sqs.filter(C_MLSNo=keyword_val)
            if len(mls_results) < 1:
                sqs = sqs.filter(C_Address__icontains=keyword_val)
            else:
                sqs = sqs.filter(C_MLSNo=keyword_val)
        except ValueError:
            sqs = sqs.filter(C_Address__icontains=keyword_val)

    paginator = Paginator(sqs, 15)
    page = request.GET.get('page')

    try:
        listings = paginator.page(page)
    except PageNotAnInteger:
        listings = paginator.page(1)
    except EmptyPage:
        listings = paginator.page(paginator.num_pages)

    
    return render_response(request, template, locals())


def blog(request, template='blog.html', context={}):

    blog_content_ordered = SiteContent.objects.filter(is_blog=True, order__isnull=False).order_by('order')
    blog_content_timestamp = SiteContent.objects.filter(is_blog=True, order__isnull=True).order_by('timestamp')
    blog_content = list(chain(blog_content_ordered,blog_content_timestamp))
    context['location_types'] = LocationType.objects.all().order_by('name')

    paginator = Paginator(blog_content, 10)

    page = request.GET.get('page')

    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        blogs = paginator.page(1)
    except EmptyPage:
        blogs = paginator.page(paginator.num_pages)

    context['blog_content'] = blogs
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "Get to know Tristan and hear about life in the mountains. From Lake Tahoe real estate stats to local events and Tristan's Facebook, Twitter, and Instagram feeds."

    return render_response(request, template, context)

def filter_content(request, location_type_id, location_type_slug, template='blog.html', context={}):
    loc_content_through = SiteContent.location_type.through

    through_objects = loc_content_through.objects.filter(locationtype__id=location_type_id, sitecontent__is_blog=True)
    blog_content = [loc_content.sitecontent for loc_content in through_objects]
    location_types = LocationType.objects.all()

    paginator = Paginator(blog_content, 10)

    page = request.GET.get('page')

    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        blogs = paginator.page(1)
    except EmptyPage:
        blogs = paginator.page(paginator.num_pages)

    context['blog_content'] = blogs
    context['location_types'] = location_types
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "Get to know Tristan and hear about life in the mountains. From Lake Tahoe real estate stats to local events and Tristan's Facebook, Twitter, and Instagram feeds."


    return render_response(request, template, context)

def content_page(request, content_id, content_slug, template='content_page.html', context={}):
    site_content = get_or_none(SiteContent, id=content_id)
    location_types = LocationType.objects.all().order_by('name')

    context['content'] = site_content
    context['location_types'] = location_types
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "Get to know Tristan and hear about life in the mountains. From Lake Tahoe real estate stats to local events and Tristan's Facebook, Twitter, and Instagram feeds."

    return render_response(request, template, context)


def get_related_content():
    """
    provides related properties
    """
    #TODO : Fix filter to current active features
    featured_prop = FeaturedListing.objects.filter()[:10]

    featured_prop = [x.listing for x in featured_prop]

    featured_len = len(featured_prop)

    if featured_len < 10:
        premium_listing = Listing.objects.filter(C_Status__name='ACTIVE').order_by('-C_AskPrice')[:10-featured_len]
    else:
        premium_listing = []

    related_properties = list(featured_prop) + list(premium_listing)

    return related_properties

def luxury_properties(request, template='mls/property_listing.html'):
    """
    returns the most expensive properties
    """
    context = {}
    property_listing = Listing.objects.filter(C_Status__name='ACTIVE', C_AskPrice__gt=1000000).order_by('-C_AskPrice')
    paginator = Paginator(property_listing, 10)

    context['related_properties'] = get_related_content()

    page = request.GET.get('page')

    try:
        listings = paginator.page(page)
    except PageNotAnInteger:
        listings = paginator.page(1)
    except EmptyPage:
        listings = paginator.page(paginator.num_pages)

    context['listings'] = listings

    return render_response(request, template, context)

def request_info(request, mls_no, template='contact_us.html', context={}):
    """
    Sends an email inquiry to the admin email address
    """
    listing = get_or_none(Listing, C_MLSNo=mls_no)
    type_of_request = request.GET.get('request','info')

    if type_of_request == 'info':
        request_message = 'Inquiry'
    elif type_of_request == 'extra_photos':
        request_message = 'Request preview of extra photos'
    elif type_of_request == 'seller_comment':
        request_message = 'Request preview of seller comments'
    else:
        request_message = 'Inquiry'
    context['request_message'] = request_message

    context['listing'] = listing
    context['related_sold_properties'] = Listing.objects.filter(C_Community=listing.C_Community, C_Status__name='SOLD')[:15]

    if request.method == 'POST':
        email = request.POST.get('email')
        name = request.POST.get('name')
        text = request.POST.get('text')
        phone = request.POST.get('phone')

        is_valid = bool(email_re.match(email))

        if is_valid:
            subject = _(u'[CBLakeTahoe] Listing ' + request_message)
            to = 'aardvark5678@gmail.com'
            from_email = settings.EMAIL_HOST_USER

            context['email'] = email
            context['name']  = name
            context['text'] = text
            context['phone'] = phone

            site = Site.objects.get(id=1)
            site_url = site.domain
            if not site_url.startswith('http://'):
                site_url = 'http://%s' % site_url

            context.update({'site_url':site_url})

            template_html = 'email/listing_request.html'
            template_text = strip_tags('email/listing_request.html')
            text_content = render_to_string(template_text, context)
            html_content = render_to_string(template_html, context)

            if isinstance(to, list):
                send_html_mail(subject, text_content, html_content, from_email, to, account=0)
            else:
                send_html_mail(subject, text_content, html_content, from_email, settings.EMAIL_RECIEVER, account=0)

            messages.add_message(request, messages.INFO, 'Your request regarding this listing : ' + str(listing.C_MLSNo) + ' has been sent.')
            return HttpResponseRedirect(listing.get_detailed_property_url())

        else:
            context['email'] = email
            context['name'] = name
            context['phone'] = phone
            context['text'] = text
            context['error_message'] = 'Email is not valid.'
            return render_response(request, template, context)

    context['email'] = ''
    context['name'] = ''
    context['text'] = ''
    context['phone'] = ''
    return render_response(request, template, context)


def edit_redirect(request, community_slug, area_slug, mls_no):

    listing = get_object_or_404(Listing, C_MLSNo=mls_no)
    info = (listing._meta.app_label, listing._meta.module_name)
    admin_url = reverse('admin:%s_%s_change' % info, args=(listing.pk,))

    return HttpResponseRedirect(admin_url)

def statistics(request, template='mls/statistics.html', context={}):
    year_delta = timedelta(days=365)
    year_delta_5 = timedelta(days=365*5)
    today = datetime.today()
    prev_year = today - year_delta
    start_year = today - year_delta_5

    current_date_range = [date(today.year, 1, 1),today]

    context['active_inventory'] = Listing.active_inventory()
    context['current_sold'] = Listing.sales_summary(date_range=current_date_range)
    context['prev_sold'] = Listing.sales_summary(date_range=[date(prev_year.year, 1, 1) ,prev_year])
    context['sales_price'] = Listing.sales_price_range(date_range=current_date_range, price_ranges=[[0, 500000],[500000, 1000000]])
    context['current_median_avg'] = Listing.median_average(date_range=current_date_range)
    context['prev_median_avg'] = Listing.median_average(date_range=[date(prev_year.year, 1, 1) ,prev_year])
    context['pending_sales'] = Listing.inventory_summary(status='PENDING', date_range=[start_year, today])

    return render_response(request, template, context)


def price_reductions(request, template='mls/price_reduced.html', context={}):
    query_dict = request.GET
    sqs = SearchQuerySet()
    bedroom_val = query_dict.get('bedroom', 'Any')
    bathroom_val = query_dict.get('bathroom', 'Any')
    area_val = query_dict.getlist('area', '')
    city_val = query_dict.getlist('city', '')
    community_val = query_dict.getlist('community', '')
    price_range_max = query_dict.get('pricerangemax','nomax')
    price_range_min = query_dict.get('pricerangemin','nomin')
    property_type_val = query_dict.getlist('property_type','')

    sqs = sqs.filter(C_Status__iexact='active').exclude(C_Address__isnull=True).order_by('-C_LstDate')

    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__in=community_val)

    if len(property_type_val) > 0 and not 'All' in property_type_val:
        sqs = sqs.filter(C_L_Type__in=property_type_val)

    if len(city_val) > 0 and not 'All' in city_val:
        sqs = sqs.filter(C_City__in=city_val)

    if len(area_val) > 0 and not 'All' in area_val:
        sqs = sqs.filter(C_Area__in=area_val)

    if bedroom_val != 'Any':
        sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

    if bathroom_val != 'Any':
        sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

    price_max = price_range_max
    price_min = price_range_min
    if price_range_max != 'nomax'\
    or price_range_min != 'nomin':

        if price_range_max == 'nomax':
            price_max = 100000000
        if price_range_min == 'nomin':
            price_min = 0
        try:
            sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
        except ValueError:
            pass

    queries_without_page = request.GET.copy()
    if queries_without_page.has_key('page'):
        del queries_without_page['page']

    properties_id = []
    for property in sqs:
        try:
            has_address = property.object.C_Address
            properties_id.append(property.object.id)
        except:
            pass

    properties = PriceChange.objects.select_related('listing__C_Community')\
                                                .filter(listing__id__in=properties_id).order_by('-timestamp')
    properties = list(properties)

    paginator = Paginator(properties, 12)
    page = request.GET.get('page')

    try:
        properties = paginator.page(page)
    except PageNotAnInteger:
        properties = paginator.page(1)
    except EmptyPage:
        properties = paginator.page(paginator.num_pages)

    context.update({
        'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
        'area_val': area_val, 'community_val': community_val,
        'price_range_max': price_range_max, 'price_range_min': price_range_min,
        'queries': queries_without_page, 'property_type_val': property_type_val,
        'properties': properties,'city_val': city_val,
    })

    context['seo_tags'] = {}
    context['seo_tags']['description'] = str(len(properties)) + \
                                         ' available price reduced real estate properties'+ \
                                         '. View market data, detailed information for each listing, photos, videos, and realtor reviews.'

    context['seo_tags']['title'] = 'Price Reduced Real Estate Properties'

    return render_response(request, template, context)

def sold(request, template='mls/sold.html', context={}):
    query_dict = request.GET
    sqs = SearchQuerySet()
    bedroom_val = query_dict.get('bedroom', 'Any')
    bathroom_val = query_dict.get('bathroom', 'Any')
    city_val = query_dict.getlist('city', '')
    area_val = query_dict.getlist('area', '')
    community_val = query_dict.getlist('community', '')
    price_range_max = query_dict.get('pricerangemax','nomax')
    price_range_min = query_dict.get('pricerangemin','nomin')
    property_type_val = query_dict.getlist('property_type','')

    sqs = sqs.filter(C_Status__iexact='sold').exclude(C_Address__isnull=True).order_by('-C_CloseDate')

    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__in=community_val)

    if len(property_type_val) > 0 and not 'All' in property_type_val:
        sqs = sqs.filter(C_L_Type__in=property_type_val)

    if len(city_val) > 0 and not 'All' in city_val:
        sqs = sqs.filter(C_City__in=city_val)

    if len(area_val) > 0 and not 'All' in area_val:
        sqs = sqs.filter(C_Area__in=area_val)

    if bedroom_val != 'Any':
        sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

    if bathroom_val != 'Any':
        sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

    price_max = price_range_max
    price_min = price_range_min
    if price_range_max != 'nomax'\
    or price_range_min != 'nomin':

        if price_range_max == 'nomax':
            price_max = 100000000
        if price_range_min == 'nomin':
            price_min = 0
        try:
            sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
        except ValueError:
            pass

    queries_without_page = request.GET.copy()
    if queries_without_page.has_key('page'):
        del queries_without_page['page']

    paginator = Paginator(sqs, 12)
    page = request.GET.get('page')

    try:
        sqs = paginator.page(page)
    except PageNotAnInteger:
        sqs = paginator.page(1)
    except EmptyPage:
        sqs = paginator.page(paginator.num_pages)

    context.update({
        'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
        'area_val': area_val, 'community_val': community_val,
        'price_range_max': price_range_max, 'price_range_min': price_range_min,
        'queries': queries_without_page, 'property_type_val': property_type_val,
        'properties': sqs, 'city_val': city_val,
    })

    context['seo_tags'] = {}
    context['seo_tags']['description'] = 'Recently sold real estate properties on Lake Tahoe and surrounding area'
    context['seo_tags']['title'] = 'Recently Sold Real Estate Properties Lake Tahoe | Tristan Roberts and Associates'

    return render_response(request, template, context)


def under_contract(request, template='mls/under-contract.html', context={}):
    query_dict = request.GET
    sqs = SearchQuerySet()
    bedroom_val = query_dict.get('bedroom', 'Any')
    bathroom_val = query_dict.get('bathroom', 'Any')
    city_val = query_dict.getlist('city', '')
    area_val = query_dict.getlist('area', '')
    community_val = query_dict.getlist('community', '')
    price_range_max = query_dict.get('pricerangemax','nomax')
    price_range_min = query_dict.get('pricerangemin','nomin')
    property_type_val = query_dict.getlist('property_type','')

    sqs = sqs.filter(C_Status__iexact__in=['PENDING', 'CONTINGENT', 'CONTINGENT RELEASE CLAUSE'])\
                .exclude(C_Address__isnull=True).order_by('-C_ContDate')

    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__in=community_val)

    if len(property_type_val) > 0 and not 'All' in property_type_val:
        sqs = sqs.filter(C_L_Type__in=property_type_val)

    if len(city_val) > 0 and not 'All' in city_val:
        sqs = sqs.filter(C_City__in=city_val)

    if len(area_val) > 0 and not 'All' in area_val:
        sqs = sqs.filter(C_Area__in=area_val)

    if bedroom_val != 'Any':
        sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

    if bathroom_val != 'Any':
        sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

    price_max = price_range_max
    price_min = price_range_min
    if price_range_max != 'nomax'\
    or price_range_min != 'nomin':

        if price_range_max == 'nomax':
            price_max = 100000000
        if price_range_min == 'nomin':
            price_min = 0
        try:
            sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
        except ValueError:
            pass

    queries_without_page = request.GET.copy()
    if queries_without_page.has_key('page'):
        del queries_without_page['page']

    paginator = Paginator(sqs, 12)
    page = request.GET.get('page')

    try:
        sqs = paginator.page(page)
    except PageNotAnInteger:
        sqs = paginator.page(1)
    except EmptyPage:
        sqs = paginator.page(paginator.num_pages)

    context.update({
        'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
        'area_val': area_val, 'community_val': community_val,
        'price_range_max': price_range_max, 'price_range_min': price_range_min,
        'queries': queries_without_page, 'property_type_val': property_type_val,
        'properties': sqs, 'city_val': city_val,
    })

    context['seo_tags'] = {}
    context['seo_tags']['description'] = 'Real estate properties under contract on Lake Tahoe and surrounding area'
    context['seo_tags']['title'] = 'Lake Tahoe Under Contract Real Estate Properties'
    return render_response(request, template, context)


def communities(request, template='mls/community_list.html', context={}):

    community_list = Community.objects.exclude(Q(name__in=['Surroundingarea', 'Sierra County',])|Q(show_on_list=False)).order_by('name')
    communities = []

    for community in community_list:
        if community.available_property() > 0:
            communities.append(community)

    paginator = Paginator(communities, 10)
    page = request.GET.get('page')

    try:
        communities = paginator.page(page)
    except PageNotAnInteger:
        communities = paginator.page(1)
    except EmptyPage:
        communities = paginator.page(paginator.num_pages)

    context['places'] = communities
    context['seo_tags'] = {}
    context['seo_tags']['description'] = 'From Truckee to the shores of Lake Tahoe, get to know the communities that we call home.  Real estate listings are updated daily.'
    

    return render_response(request, template, context)


def cities(request, template='mls/city_list.html', context={}):

    city_list = City.objects.filter(name__isnull=False, show_on_list=True).order_by('name')
    cities = []

    for city in city_list:
        if city.available_property() > 0:
            cities.append(city)

    paginator = Paginator(cities, 10)
    page = request.GET.get('page')

    try:
        cities = paginator.page(page)
    except PageNotAnInteger:
        cities = paginator.page(1)
    except EmptyPage:
        cities = paginator.page(paginator.num_pages)

    context['places'] = cities
    context['seo_tags'] = {}
    context['seo_tags']['description'] = "You\'re far from the typical city when you own real estate in the Lake Tahoe / Truckee region.  It's life in the mountains."
    context['seo_tags']['title'] = 'Lake Tahoe Cities and active real estate listings'

    return render_response(request, template, context)


def neighborhood(request, template='mls/neighborhood_list.html', context={}):

    sqs = SearchQuerySet().filter(C_Status__name='ACTIVE').order_by('C_Area')
    community_val = request.GET.getlist('community', [])
    hoa_amenities_val = request.GET.getlist('hoa_amenities', [])
    waterfront_amenities_val = request.GET.getlist('waterfront_amenities', [])

    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__name__in=community_val)

    if len(hoa_amenities_val) > 0 and not 'All' in hoa_amenities_val:
        sqs = sqs.filter(C_HOAMENITIS__name__in=hoa_amenities_val)

    if len(waterfront_amenities_val) > 0 and not 'All' in waterfront_amenities_val:
            sqs = sqs.filter(C_WTRFRNTMNT__name__in=waterfront_amenities_val)
    area_list = []
    listed = []
    for prop in sqs:
         areas = Area.objects.filter(name=prop.C_Area , available_properties__gt = 0, show_on_list=True).distinct()
         if len(areas) > 0 and prop.C_Area not in listed:
            area_list.append(areas[0])
            listed.append(prop.C_Area)

    paginator = Paginator(area_list, 10)
    page = request.GET.get('page')

    try:
        area_list = paginator.page(page)
    except PageNotAnInteger:
        area_list = paginator.page(1)
    except EmptyPage:
        area_list = paginator.page(paginator.num_pages)

    context['places'] = area_list
    context['community_val'] = community_val
    context['hoa_amenities_val'] = hoa_amenities_val
    context['waterfront_amenities_val'] = waterfront_amenities_val
    context['seo_tags'] = {}
    context['seo_tags']['description'] = 'Neighborhood in Lake Tahoe and surrounding area'
    context['seo_tags']['title'] = 'Lake Tahoe Neighborhood Subdivision'

    return render_response(request, template, context)

def explore(request, template='mls/explore.html', context={}):

    context['latest_listings'] = Listing.objects.filter(C_Status__name__iexact='active').order_by('-C_LstDate')[:8]

    return render_response(request, template, context)


def latest_listings(request, template='mls/latest.html', context={}):
    query_dict = request.GET
    sqs = SearchQuerySet()
    bedroom_val = query_dict.get('bedroom', 'Any')
    bathroom_val = query_dict.get('bathroom', 'Any')
    city_val = query_dict.getlist('city', '')
    area_val = query_dict.getlist('area', '')
    community_val = query_dict.getlist('community', '')
    price_range_max = query_dict.get('pricerangemax','nomax')
    price_range_min = query_dict.get('pricerangemin','nomin')
    property_type_val = query_dict.getlist('property_type','')

    sqs = sqs.filter(C_Status__iexact='active').exclude(C_Address__isnull=True).order_by('-C_LstDate')

    if len(community_val) > 0 and not 'All' in community_val:
        sqs = sqs.filter(C_Community__in=community_val)

    if len(property_type_val) > 0 and not 'All' in property_type_val:
        sqs = sqs.filter(C_L_Type__in=property_type_val)

    if len(city_val) > 0 and not 'All' in city_val:
        sqs = sqs.filter(C_City__in=city_val)

    if len(area_val) > 0 and not 'All' in area_val:
        sqs = sqs.filter(C_Area__in=area_val)

    if bedroom_val != 'Any':
        sqs = sqs.filter(C_NoBeds__gte=bedroom_val)

    if bathroom_val != 'Any':
        sqs = sqs.filter(C_NoBaths1__gte=bathroom_val)

    price_max = price_range_max
    price_min = price_range_min
    if price_range_max != 'nomax'\
    or price_range_min != 'nomin':

        if price_range_max == 'nomax':
            price_max = 100000000
        if price_range_min == 'nomin':
            price_min = 0
        try:
            sqs = sqs.filter(C_AskPrice__range=[float(price_min), float(price_max)])
        except ValueError:
            pass

    queries_without_page = request.GET.copy()
    if queries_without_page.has_key('page'):
        del queries_without_page['page']

    paginator = Paginator(sqs, 12)
    page = request.GET.get('page')

    try:
        sqs = paginator.page(page)
    except PageNotAnInteger:
        sqs = paginator.page(1)
    except EmptyPage:
        sqs = paginator.page(paginator.num_pages)

    context.update({
        'bathroom_val': bathroom_val, 'bedroom_val':bedroom_val,
        'area_val': area_val, 'community_val': community_val,
        'price_range_max': price_range_max, 'price_range_min': price_range_min,
        'queries': queries_without_page, 'property_type_val': property_type_val,
        'properties': sqs, 'city_val': city_val,
    })

    context['seo_tags'] = {}
    context['seo_tags']['description'] = str(len(sqs)) + \
                                         ' available new real estate properties'+ \
                                         '. View market data, detailed information for each listing, photos, videos, and realtor reviews.'

    context['seo_tags']['title'] = 'New Real Estate Properties'
    return render_response(request, template, context)


def ajax_contact_us(request, context={}):
    if request.is_ajax():
        obj = dict(request.POST)
        form = ContactUsForm(request.POST)
        mls_no = request.POST.get('mls_number')
        if form.is_valid():
            cleaned_data = form.cleaned_data
            listing = Listing.objects.get(C_MLSNo=mls_no)
            subject = _(u'[CBLakeTahoe] Listing Inquiry (MLS No: %s - %s, %s, %s)' % \
                            (str(listing.C_MLSNo), str(listing.C_Address), str(listing.C_Community), str(listing.C_State)))
            to = 'aardvark5678@gmail.com'
            from_email = settings.EMAIL_HOST_USER

            context['email'] = cleaned_data['email']
            context['name']  = cleaned_data['name']
            context['message'] = cleaned_data['message']
            context['phone'] = cleaned_data['phone']
            context['listing'] = listing
            context['related_sold_properties'] = Listing.objects.filter(C_Community=listing.C_Community, C_Status__name='SOLD')[:15]
            site = Site.objects.get(id=1)
            site_url = site.domain
            if not site_url.startswith('http://'):
                site_url = 'http://%s' % site_url

            context.update({'site_url':site_url})

            template_html = 'email/listing_request.html'
            template_text = strip_tags('email/listing_request.html')
            text_content = render_to_string(template_text, context)
            html_content = render_to_string(template_html, context)

            if isinstance(to, list):
                send_html_mail(subject, text_content, html_content, from_email, to, account=0)
            else:
                send_html_mail(subject, text_content, html_content, from_email, settings.EMAIL_RECIEVER, account=0)

            subject_reply = _(u'Re: [CBLakeTahoe] Listing Inquiry (MLS No: %s)' % (str(listing.C_MLSNo),))
            template_html_reply = 'email/listing_request_autoreply.html'
            template_text_reply = strip_tags('email/listing_request_autoreply.html')
            text_content_reply = render_to_string(template_text_reply, context)
            html_content_reply = render_to_string(template_html_reply, context)

            if isinstance(to, list):
                send_html_mail(subject_reply, text_content_reply, html_content_reply, from_email, context['email'], account=0)
            else:
                send_html_mail(subject_reply, text_content_reply, html_content_reply, from_email, [context['email'],], account=0)

            obj['status'] = True
        else:
            obj = dict(form.errors.items())
            obj['status'] = False
        return HttpResponse(json.dumps(obj), mimetype='text/json')


def fuzzy_redirect(request, fuzzy_slug=None):
    from mls.diff_match_patch import diff_match_patch as DMP
    from operator import itemgetter

    area_list = Area.objects.values_list('slug', flat=True)
    community_list = Community.objects.values_list('slug', flat=True)
    community_levenshtein = []
    area_levenshtein = []
    dmp = DMP()

    for community in community_list:
        diffs = dmp.diff_main(community, fuzzy_slug)
        lev_distance = dmp.diff_levenshtein(diffs)
        community_levenshtein.append(lev_distance)

    community_list = sorted(zip(community_levenshtein, community_list), key=itemgetter(0))
    community_match = community_list[0] if community_list[0][0] < 5 else None

    for area in area_list:
        diffs = dmp.diff_main(area, fuzzy_slug)
        lev_distance = dmp.diff_levenshtein(diffs)
        area_levenshtein.append(lev_distance)

    area_list = sorted(zip(area_levenshtein, area_list), key=itemgetter(1))
    area_match = area_list[0] if area_list[0][0] < 5 else None

    if not (community_match or area_match):
        raise Http404

    if community_match and area_match:
        if community_match[0] == area_match[0] or community_match[0] < area_match[0]:
            return HttpResponseRedirect(reverse('view_community', kwargs={'community_slug':community_match[1]}))
        else:
            return HttpResponseRedirect(reverse('view_area', kwargs={'area_slug':area_match[1]}))


    elif community_match:
        return HttpResponseRedirect(reverse('view_community', kwargs={'community_slug':community_match[1]}))

    elif area_match:
        return HttpResponseRedirect(reverse('view_area', kwargs={'area_slug':area_match[1]}))





def detailed_vendor(request, pk, template='mls/detailed_vendor.html', context={}):
    '''
        Detailed Vendor by PK
    '''
    vendor = get_object_or_404(Vendor, pk=pk)
    context['vendor'] = vendor
    context['seo_tags'] = {}
    context['seo_tags']['title'] = 'Vendor-'+ vendor.name_of_organization + ' |' 
    return render_response(request, template, context)