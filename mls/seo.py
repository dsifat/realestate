from rollyourown import seo
from rollyourown.seo.admin import register_seo_admin
from django.contrib import admin



class MyMetadata(seo.Metadata):
    title       = seo.Tag(head=True, max_length=68)
    description = seo.MetaTag(max_length=155)
    keywords    = seo.KeywordTag()
    heading     = seo.Tag(name="h1")

register_seo_admin(admin.site, MyMetadata)