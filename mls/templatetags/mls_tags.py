from django import template
from django.db.models import get_model
from mls.models import Img
from django.conf import settings


register = template.Library()


@register.filter
def get_photo(mls_no):
    """Get the photo using mls no."""
    
    try:
        picture = Img.objects.filter(C_L_ListingID=mls_no)[0]
        if picture.C_PATH == 'REFERENCE_ONLY':
            picture = picture.C_URL
        else:
            picture = '/media' +  picture.C_PATH.split('media')[1]
    except:
        picture = '/static/css/images/houses/house_1.jpg'
    return picture

@register.filter
def get_percentage(base, value):
    """
    returns percentage of the value based on the base
    """
    percentage = 0
    try:
        percentage = float(value)*100/(float(base))
    except ZeroDivisionError:
        return percentage
    except Exception as e:
        return e

    return round(float(percentage), 2)

@register.filter
def get_delta_percentage(initial_value, final_value):
    """
    returns delta percentage between the two values
    """

    delta_percentage = 0

    try:
        delta_percentage = ((float(final_value) - float(initial_value))/float(initial_value))*100
    except ZeroDivisionError:
        return delta_percentage
    except Exception as e:
        return e

    return round(float(delta_percentage),2)

@register.filter
def get_key(obj, key):
    """
    returns the value of the object specified by the key
    """
    try:
        value = obj.get(key)
    except Exception as e:
        value = e
    return value
