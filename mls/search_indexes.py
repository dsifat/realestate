import datetime
from haystack import indexes
from mls.models import Listing

class ListingIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    C_NoBeds = indexes.CharField(model_attr='C_NoBeds', faceted=True)
    C_NoBaths1 = indexes.CharField(model_attr='C_NoBaths1', faceted=True)
    C_Acreage = indexes.FloatField(model_attr='C_Acreage', null=True)
    C_Address = indexes.CharField(model_attr='C_Address', faceted=True)
    C_Community = indexes.CharField(model_attr='C_Community', faceted=True, null=True)
    C_Area = indexes.CharField(model_attr='C_Area', faceted=True, null=True)
    C_City = indexes.CharField(model_attr='C_City', faceted=True, null=True)
    C_MLSNo = indexes.CharField(model_attr='C_MLSNo', null=True)
    C_LType = indexes.CharField(model_attr='C_LType', null=True)
    C_AskPrice = indexes.FloatField(model_attr='C_AskPrice', null=True)
    C_Garage = indexes.CharField(model_attr='C_Garage', null=True)
    C_SqFeet = indexes.CharField(model_attr='C_SqFeet', null=True)
    C_AcresNo = indexes.CharField(model_attr='C_AcresNo', null=True)
    C_YrBlt = indexes.CharField(model_attr='C_YrBlt', null=True)
    C_LotDims = indexes.CharField(model_attr='C_LotDims', null=True)
    C_Remarks = indexes.CharField(model_attr='C_Remarks', null=True)
    C_Addendum = indexes.CharField(model_attr='C_Addendum', null=True)
    C_VIEW = indexes.MultiValueField()
    C_Setting = indexes.CharField(model_attr='C_Setting', null=True)
    C_Status = indexes.CharField(model_attr='C_Status', null=True)
    C_HOAMENITIS = indexes.MultiValueField()
    C_WTRFRNTMNT = indexes.MultiValueField()
    C_L_Type = indexes.CharField(model_attr='C_L_Type', null=True)
    C_LstDate = indexes.DateField(model_attr='C_LstDate', null=True)
    C_CloseDate = indexes.DateField(model_attr='C_CloseDate', null=True)
    C_ContDate = indexes.DateField(model_attr='C_ContDate', null=True)
    C_GeoLat = indexes.CharField(model_attr='C_GeoLat', null=True)
    C_GeoLong = indexes.CharField(model_attr='C_GeoLong', null=True)

    def prepare_C_HOAMENITIS(self, object):
        if object.C_HOAMENITIS.all():
            return [s.name for s in object.C_HOAMENITIS.all()]
        return None

    def prepare_C_WTRFRNTMNT(self, object):
        if object.C_WTRFRNTMNT.all():
            return [s.name for s in object.C_WTRFRNTMNT.all()]
        return None

    def prepare_C_VIEW(self, object):
        if object.C_VIEW.all():
            return [s.name for s in object.C_VIEW.all()]
        return None

    def prepare_C_L_Type(self, object):
        if object.C_L_Type:
            return object.C_L_Type.name
        return None

    def prepare_C_Area(self, object):
        if object.C_Area:
            return object.C_Area.name
        return None

    def prepare_C_Community(self, object):
        if object.C_Community:
            return object.C_Community.name
        return None

    def get_model(self):
        return Listing

    def read_queryset(self):
        return self.get_model().objects.select_related()

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.all().prefetch_related('C_WTRFRNTMNT','C_HOAMENITIS').select_related('C_L_Type','C_Community','C_City')
