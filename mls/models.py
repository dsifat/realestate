import random, os
import numpy
import datetime as dateTime
import commands
import tempfile

import logging
logger = logging.getLogger(__name__)

from decimal import Decimal
from datetime import datetime, timedelta, date
from common.utils import get_or_none
from collections import OrderedDict
from django.conf import settings
from django.contrib.humanize.templatetags.humanize import intcomma
from django.core.urlresolvers import reverse
from django.db import models, IntegrityError
from django.db import IntegrityError
from django.template.defaultfilters import slugify
from django.db.models import Q, Avg, Max, Min, F
from django.utils import simplejson as json
from django.utils.timezone import utc
from django.conf import settings
from cStringIO import StringIO
from django.core.files.uploadedfile import SimpleUploadedFile
from django.template.defaultfilters import slugify

from mls.utils import get_new_listings, get_pending, get_recently_sold,\
                        get_statistics_data
from photologue.models import Gallery
from PIL import Image
#from mls.utils import ( get_create_city, get_create_area, get_create_neighborhood,
#                        get_create_setting, get_create_view )

from caching.base import CachingManager, CachingMixin
from filebrowser.fields import FileBrowseField
from dateutil.relativedelta import relativedelta
from mls.manager import CachedManager

size_270 = 270, 270
size_100 = 100, 100
size_540 = 540, 360


class City(CachingMixin, models.Model):
    """
    stores city names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True, \
                            help_text='Auto-generated from name if blank.')
    order = models.PositiveIntegerField(null=True, blank=True)
    description = models.TextField(max_length=5000, null=True, blank=True)
    image = models.ImageField(upload_to='area_images', null=True, blank=True)
    show_on_list = models.BooleanField(default=False)
    available_properties = models.PositiveIntegerField(default=0)
    objects = CachingManager()

    class Meta:
        verbose_name_plural = 'Cities'
        ordering = ('order',)

    def __unicode__(self):
        return "%s" % self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            # if self.shortened_name:
            #     self.slug = slugify(self.shortened_name)
            # else:
            self.slug = slugify(self.name)
        super(City, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('view_city', args=(self.slug,))

    def available_property(self):
        return self.available_properties

    def recently_sold(self):
        return get_recently_sold(self)

    def pending(self):
        return get_pending(self)

    def new_listings(self):
        return get_new_listings(self)

    def price_reduced(self):
        listings = PriceChange.objects.select_related('listing__C_City')\
                    .filter(listing__C_Status__name='ACTIVE', listing__C_City=self,)\
                    .exclude(Q(listing__C_Community__name='SurroundingArea')
                       |Q(listing__C_Community__name='Sierra County'))\
                    .only('listing__C_City', 'listing__C_NoBeds', 'listing__C_SqFeet', 'listing__C_AskPrice',
                     'listing__C_Address', 'listing__C_MLSNo', 'listing__C_NoBeds', 'listing__C_NoBaths1',
                     'listing__C_Garage', 'listing__C_SqFeet', 'listing__C_YrBlt', 'listing__C_LstDate')\
                    .order_by('-timestamp')[:15]
        return listings

    def statistics_data(self):
        return get_statistics_data(self)


class Area(CachingMixin, models.Model):
    """
    stores area names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True, \
                            help_text='Auto-generated from name if blank.')
    shortened_name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    order = models.PositiveIntegerField(null=True, blank=True)
    description = models.TextField(max_length=5000, null=True, blank=True)
    image = models.ImageField(upload_to='area_images', null=True, blank=True)
    show_on_list = models.BooleanField(default=False)
    available_properties = models.PositiveIntegerField(default=0)
    objects = CachingManager()

    class Meta:
        ordering = ('name',)

    def __unicode__(self):

        if self.shortened_name:
            return "%s" % str(self.shortened_name).title()

        return "%s" % str(self.name).title()

    def save(self, *args, **kwargs):

        if not self.shortened_name:
            self.shortened_name = str(self.name).title()

        if not self.slug:
            if self.shortened_name:
                self.slug = slugify(self.shortened_name)
            else:
                self.slug = slugify(self.name)

        super(Area, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('view_area', args=(self.slug,))

    def available_property(self):
        return self.available_properties

    def recently_sold(self):
        return get_recently_sold(self)

    def pending(self):
        return get_pending(self)

    def new_listings(self):
        return get_new_listings(self)

    def price_reduced(self, limit=10):
        listings = PriceChange.objects.select_related('listing__C_Area')\
                    .filter(listing__C_Status__name='ACTIVE', listing__C_Area=self,)\
                    .exclude(Q(listing__C_Community__name='SurroundingArea')
                       |Q(listing__C_Community__name='Sierra County'))\
                    .only('listing__C_Area', 'listing__C_NoBeds', 'listing__C_SqFeet', 'listing__C_AskPrice',
                     'listing__C_Address', 'listing__C_MLSNo', 'listing__C_NoBeds', 'listing__C_NoBaths1',
                     'listing__C_Garage', 'listing__C_SqFeet', 'listing__C_YrBlt', 'listing__C_LstDate')\
                    .order_by('-timestamp')[:limit]
        return listings

    def statistics_data(self):
        return get_statistics_data(self)

class Community(CachingMixin, models.Model):
    """
    Stores community names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True, \
                            help_text='Auto-generated from name if blank.')
    city = models.ManyToManyField(City, blank=True, null=True)
    area = models.ManyToManyField(Area, blank=True, null=True)
    shortened_name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    order = models.PositiveIntegerField(null=True, blank=True)
    description = models.TextField(max_length=5000, null=True, blank=True)
    image = models.ImageField(upload_to='community_image', null=True, blank=True)
    show_on_list = models.BooleanField(default=True)
    available_properties = models.PositiveIntegerField(default=0)
    objects = CachingManager()

    class Meta:
        verbose_name_plural = 'Communities'
        ordering = ('order',)

    def __unicode__(self):

        if self.shortened_name:
            return "%s" % self.shortened_name

        return "%s" % self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            if self.shortened_name:
                self.slug = slugify(self.shortened_name)
            else:
                self.slug = slugify(self.name)
        try:
            super(Community, self).save(*args, **kwargs)
        except IntegrityError, e:
            print e
            self.slug = slugify(self.name) + str(random.random())[:8]
            super(Community, self).save(*args, **kwargs)
        except Exception as e:
            print "Exception occured :"
            print e
            print "Please check related listing"

    def get_absolute_url(self):
        return reverse('view_community', args=(self.slug,))

    def available_property(self):
        return self.available_properties

    def recently_sold(self):
        return get_recently_sold(self)

    def pending(self):
        return get_pending(self)

    def new_listings(self):
        return get_new_listings(self)

    def price_reduced(self):
        listings = PriceChange.get_recent_reduced(max_length=15).filter(listing__C_Community=self,)\
                                .exclude(Q(listing__C_Community__name='SurroundingArea')|\
                                            Q(listing__C_Community__name='Sierra County')).order_by('-timestamp')[:15]
        return listings

    def statistics_data(self):
        return get_statistics_data(self)


class ListingStatus(models.Model):
    """
    stores listing status names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    order = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        verbose_name_plural = 'Listing Status'

    def __unicode__(self):
        return "%s" % self.name


class Neighborhood(CachingMixin, models.Model):
    """
    stores neighborhood names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    order = models.PositiveIntegerField(null=True, blank=True)
    show_on_list = models.BooleanField(default=False)
    objects = CachingManager()

    def __unicode__(self):
        return "%s" % self.name

class HOA(models.Model):
    """
    Relates the HOA Images and amenities to the area/community
    """
    area = models.ForeignKey(Area, unique=True, null=True, blank=True)
    community = models.ForeignKey(Community, unique=True, null=True, blank=True)
    hoa_photos = FileBrowseField('HOA Photos', help_text='Folder where the photos of the HOA is located.',
                                    max_length=255, null=True, blank=True)

    def __unicode__(self):
        if self.area:
            hoa_name = "%s" % (self.area)
        elif self.community:
            hoa_name = "%s" % (self.community)
        else:
            return "Unlinked HOA"

        if self.area and self.community:
            hoa_name = "%s %s" % (self.area, self.community)

        return hoa_name


class HOAAmenities(models.Model):
    """
    stores HOA Amenities
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'HOA Amenities'
        ordering = ('name',)

    def __unicode__(self):
        return u'%s' % (self.name,)


class WaterAmenities(models.Model):
    """
    stores Water Amenities
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name_plural = 'Waterfront Amenities'
        verbose_name = 'Waterfront Amenities'
        ordering = ('name',)

    def __unicode__(self):
        return u'%s' % (self.name,)


class Setting(models.Model):
    """
    stores property setting names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True, \
                            help_text='Auto-generated from name if blank.')
    order = models.PositiveIntegerField(null=True, blank=True)
    available_properties = models.PositiveIntegerField(default=0)
    description = models.TextField(max_length=5000, null=True, blank=True)

    class Meta:
        ordering = ('order',)

    def __unicode__(self):
        return "%s" % self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Setting, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('view_setting', args=(self.slug,))

    def available_property(self):
        return self.available_properties

    def recently_sold(self):
        return get_recently_sold(self)

    def pending(self):
        return get_pending(self)

    def new_listings(self):
        return get_new_listings(self)

    def price_reduced(self):
        listings = PriceChange.objects.select_related('listing__C_Setting')\
                    .filter(listing__C_Status__name='ACTIVE', listing__C_Setting=self,)\
                    .exclude(Q(listing__C_Community__name='SurroundingArea')
                       |Q(listing__C_Community__name='Sierra County'))\
                    .only('listing__C_Setting', 'listing__C_NoBeds', 'listing__C_SqFeet', 'listing__C_AskPrice',
                     'listing__C_Address', 'listing__C_MLSNo', 'listing__C_NoBeds', 'listing__C_NoBaths1',
                     'listing__C_Garage', 'listing__C_SqFeet', 'listing__C_YrBlt', 'listing__C_LstDate')\
                    .order_by('-timestamp')[:15]
        return listings

    def statistics_data(self):
        return get_statistics_data(self)


class View(models.Model):
    """
    stores property setting names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    active = models.BooleanField(default=True)
    order = models.PositiveIntegerField(null=True, blank=True)

    def __unicode__(self):
        return "%s" % self.name


class Type(models.Model):
    """
    stores property type names
    """
    name = models.CharField(max_length=100, unique=True, null=True, blank=True)
    slug = models.SlugField(unique=True, blank=True, null=True, \
                            help_text='Auto-generated from name if blank.')
    order = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ('order',)

    def __unicode__(self):
        return "%s" % self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Type, self).save(*args, **kwargs)


class Listing(CachingMixin, models.Model):
    #TODO 10/3/13 @philip : add RESIDENCE_TYPES and SUB_STATUS as global variables
    C_NoBaths1 = models.CharField(max_length=105, help_text="# of Baths", null=True, blank=True)
    C_NoBeds = models.CharField(max_length=105, help_text="# of Bedrooms", null=True, blank=True)
    C_N1stLnAmt = models.CharField(max_length=105, help_text="1st Loan Amount", null=True, blank=True)
    C_APN = models.CharField(max_length=105, help_text="APN", null=True, blank=True)
    C_APPLIANCES = models.CharField(max_length=105, help_text="APPLIANCES", null=True, blank=True)
    C_Addendum = models.TextField(null=True, blank=True)
    C_Address = models.CharField(max_length=105, help_text="Address", null=True, blank=True)
    C_AddrDrctn = models.CharField(max_length=105, help_text="Address Direction", null=True, blank=True)
    C_AddrNumber = models.CharField(max_length=105, help_text="Address Number", null=True, blank=True)
    C_AddrStreet = models.CharField(max_length=105, help_text="Address Street", null=True, blank=True)
    C_Age = models.CharField(max_length=105, help_text="Age", null=True, blank=True)
    C_EMail = models.CharField(max_length=105, help_text="Agent E-Mail Address", null=True, blank=True)
    C_Agent = models.CharField(max_length=105, help_text="Agent Name", null=True, blank=True)
    C_ApprxAcre = models.CharField(max_length=105, help_text="Approx. Acreage", null=True, blank=True)
    C_Acreage = models.DecimalField(max_digits=10, decimal_places=2, help_text="Acreage", null=True, blank=True)
    C_ApprxSqFt = models.CharField(max_length=105, help_text="Approx. SqFt", null=True, blank=True)
    C_Architect = models.CharField(max_length=105, help_text="Architecture", null=True, blank=True)
    C_Area = models.ForeignKey(Area, null=True, blank=True)
    C_AskPrice = models.DecimalField(max_digits=10, decimal_places=2, help_text="Asking Price", null=True, blank=True)
    C_AssocDoc = models.CharField(max_length=105, help_text="Associated Document Count", null=True, blank=True)
    C_BONUSROOMS = models.CharField(max_length=300, help_text="BONUS ROOMS", null=True, blank=True)
    C_BldgName = models.CharField(max_length=105, help_text="Building Name", null=True, blank=True)
    C_Show1 = models.CharField(max_length=105, help_text="Call to Show", null=True, blank=True)
    C_City = models.ForeignKey(City, null=True, blank=True)
    C_Class = models.CharField(max_length=105, help_text="Class", null=True, blank=True)
    C_CloseDate = models.DateTimeField(help_text="Closing Date", null=True, blank=True)
    C_Community = models.ForeignKey(Community, null=True, blank=True)
    C_ConstPhse = models.CharField(max_length=105, help_text="Construction Phase", null=True, blank=True)
    C_ContDate = models.DateTimeField(help_text="Contract Date", null=True, blank=True)
    C_County = models.CharField(max_length=105, help_text="County", null=True, blank=True)
    C_DECKS = models.CharField(max_length=105, help_text="DECKS", null=True, blank=True)
    C_DOM = models.CharField(max_length=105, help_text="DOM", null=True, blank=True)
    C_DOMLS = models.CharField(max_length=105, help_text="DOMLS", null=True, blank=True)
    C_DocDate = models.DateTimeField(help_text="Doc Timestamp", null=True, blank=True)
    C_FIREPLACE = models.TextField(null=True, blank=True)
    C_FLORCVRNGS = models.CharField(max_length=105, help_text="FLOOR COVERINGS", null=True, blank=True)
    C_FOUNDATION = models.CharField(max_length=105, help_text="FOUNDATION", null=True, blank=True)
    C_Furnished = models.CharField(max_length=105, help_text="Furnished", null=True, blank=True)
    C_GARAGPRKNG = models.TextField(null=True, blank=True)
    C_GAS = models.CharField(max_length=105, help_text="GAS", null=True, blank=True)
    C_GATED = models.CharField(max_length=105, help_text="GATED", null=True, blank=True)
    C_GRENFEATRS = models.TextField(null=True, blank=True)
    C_Garage = models.CharField(max_length=105, help_text="Garage", null=True, blank=True)
    C_HNDCPDFRND = models.CharField(max_length=105, help_text="HANDICAPPED FRIENDLY", null=True, blank=True)
    C_HEATING = models.CharField(max_length=105, help_text="HEATING", null=True, blank=True)
    C_HOAMENITIS = models.ManyToManyField(HOAAmenities, null=True, blank=True)
    C_HOAAmt = models.CharField(max_length=105, help_text="HOA Dues Amount", null=True, blank=True)
    C_HOAOpt = models.CharField(max_length=105, help_text="HOA Dues Optional Y/N", null=True, blank=True)
    C_HOAPer = models.CharField(max_length=105, help_text="HOA Dues Per/", null=True, blank=True)
    C_HOAMbr = models.CharField(max_length=105, help_text="HOA Membership Y/N", null=True, blank=True)
    C_Show = models.CharField(max_length=105, help_text="How Shown", null=True, blank=True)
    C_HowSold = models.CharField(max_length=105, help_text="How Sold", null=True, blank=True)
    C_IDXIncl = models.CharField(max_length=105, help_text="IDX Include", null=True, blank=True)
    C_IDXvt = models.TextField(null=True, blank=True)
    C_LA1Email = models.CharField(max_length=105, help_text="LA1Agent Email", null=True, blank=True)
    C_LA1FirstNm = models.CharField(max_length=105, help_text="LA1Agent First Name", null=True, blank=True)
    C_LA1LastNam = models.CharField(max_length=105, help_text="LA1Agent Last Name", null=True, blank=True)
    C_LA1Phon1Ar = models.CharField(max_length=105, help_text="LA1Agent Phone1 Area Code", null=True, blank=True)
    C_LA1Phn1Nbr = models.CharField(max_length=105, help_text="LA1Agent Phone1 Number", null=True, blank=True)
    C_LA1Phon2Ar = models.CharField(max_length=105, help_text="LA1Agent Phone2 Area Code", null=True, blank=True)
    C_LA1Phn2Nbr = models.CharField(max_length=105, help_text="LA1Agent Phone2 Number", null=True, blank=True)
    C_LA1Phon3Ar = models.CharField(max_length=105, help_text="LA1Agent Phone3 Area Code", null=True, blank=True)
    C_LA1Phn3Nbr = models.CharField(max_length=105, help_text="LA1Agent Phone3 Number", null=True, blank=True)
    C_LA1Phon4Ar = models.CharField(max_length=105, help_text="LA1Agent Phone4 Area Code", null=True, blank=True)
    C_LA1Phn4Nbr = models.CharField(max_length=105, help_text="LA1Agent Phone4 Number", null=True, blank=True)
    C_LA1Phon5Ar = models.CharField(max_length=105, help_text="LA1Agent Phone5 Area Code", null=True, blank=True)
    C_LA1Phn5Nbr = models.CharField(max_length=105, help_text="LA1Agent Phone5 Number", null=True, blank=True)
    C_LA1Phn1Dsc = models.CharField(max_length=105, help_text="LA1AgentPhone1Description", null=True, blank=True)
    C_LA1Phn2Dsc = models.CharField(max_length=105, help_text="LA1AgentPhone2Description", null=True, blank=True)
    C_LA1Phn4Dsc = models.CharField(max_length=105, help_text="LA1AgentPhone4Description", null=True, blank=True)
    C_LA1Phn5Dsc = models.CharField(max_length=105, help_text="LA1AgentPhone5Description", null=True, blank=True)
    C_LA1Phn3Dsc = models.CharField(max_length=105, help_text="LA1AgentPnone3Description", null=True, blank=True)
    C_LA1WorksFr = models.CharField(max_length=105, help_text="LA1Works For", null=True, blank=True)
    C_LAUNDRY = models.CharField(max_length=105, help_text="LAUNDRY", null=True, blank=True)
    C_LO1OfcName = models.CharField(max_length=105, help_text="LO1Office Name", null=True, blank=True)
    C_LO1Phn1Nbr = models.CharField(max_length=105, help_text="LO1Office Phone1 Number", null=True, blank=True)
    C_LstDate = models.DateTimeField(help_text="Listing Date", null=True, blank=True)
    C_LType = models.CharField(max_length=105, help_text="Listing Type", null=True, blank=True)
    C_LotNo = models.CharField(max_length=105, help_text="Lot #", null=True, blank=True)
    C_LotDims = models.CharField(max_length=105, help_text="Lot Dimensions", null=True, blank=True)
    C_MLSNo = models.CharField(max_length=105, help_text="MLS #", null=True, blank=True)
    C_AcresNo = models.CharField(max_length=105, help_text="Number of Acres", null=True, blank=True)
    C_NoShrOrUn = models.CharField(max_length=105, help_text="Number of Shares Per Unit", null=True, blank=True)
    C_OrigPrice = models.DecimalField(max_digits=10, decimal_places=2, help_text="Original Price", null=True, blank=True)
    C_OthDues = models.CharField(max_length=105, help_text="Other Dues Amt", null=True, blank=True)
    C_OthDues2 = models.CharField(max_length=105, help_text="Other Dues Per/", null=True, blank=True)
    C_PRPRTYCNDT = models.CharField(max_length=105, help_text="PROPERTY CONDITION", null=True, blank=True)
    C_BOrU = models.CharField(max_length=105, help_text="Phase/Unit", null=True, blank=True)
    C_PhotoCount = models.CharField(max_length=105, help_text="Photo Count", null=True, blank=True)
    C_PhotoDate = models.DateTimeField(help_text="PhotoTimestamp", null=True, blank=True)
    C_PrcDate = models.DateTimeField(help_text="Price Date", null=True, blank=True)
    C_Remarks = models.TextField(null=True, blank=True)
    C_ROOF = models.CharField(max_length=105, help_text="ROOF", null=True, blank=True)
    C_Setting = models.ForeignKey(Setting, null=True, blank=True)
    C_SoldPrice = models.DecimalField(max_digits=10, decimal_places=2, help_text="Sold Price", null=True, blank=True)
    C_SqFeet = models.CharField(max_length=105, help_text="Square Feet", null=True, blank=True)
    C_State = models.CharField(max_length=105, help_text="State", null=True, blank=True)
    C_Status = models.ForeignKey(ListingStatus, null=True, blank=True)
    C_StatusCat = models.CharField(max_length=105, help_text="Status Category", null=True, blank=True)
    C_StatDate = models.DateTimeField(help_text="Status Date", null=True, blank=True)
    C_StatusDetl = models.CharField(max_length=105, help_text="Status Detail", null=True, blank=True)
    C_Stories = models.CharField(max_length=105, help_text="Stories", null=True, blank=True)
    C_SubStatus = models.CharField(max_length=105, help_text="Sub Status", null=True, blank=True)
    C_SubArea = models.CharField(max_length=105, help_text="SubArea", null=True, blank=True)
    C_TOPOGRAPHY = models.CharField(max_length=105, help_text="TOPOGRAPHY", null=True, blank=True)
    C_TaxDBID = models.CharField(max_length=105, help_text="Tax Database ID", null=True, blank=True)
    C_TaxPropID = models.CharField(max_length=105, help_text="Tax Property ID", null=True, blank=True)
    C_InitFee = models.CharField(max_length=105, help_text="Transfer Fee", null=True, blank=True)
    C_Address2 = models.CharField(max_length=105, help_text="Unit#", null=True, blank=True)
    C_UpdtDate = models.DateTimeField(help_text="Update Date", null=True, blank=True)
    C_VIEW = models.ManyToManyField(View, null=True, blank=True)
    C_VIEW2 = models.CharField(max_length=105, help_text="VIEW", null=True, blank=True)
    C_WATER = models.CharField(max_length=105, help_text="WATER", null=True, blank=True)
    C_WTRFRNTMNT = models.ManyToManyField(WaterAmenities, null=True, blank=True)
    C_WOODSTOVE = models.CharField(max_length=105, help_text="WOODSTOVE", null=True, blank=True)
    C_WtrCo = models.CharField(max_length=105, help_text="Water Company", null=True, blank=True)
    C_YrBlt = models.CharField(max_length=105, help_text="Year Built", null=True, blank=True)
    C_Zip = models.CharField(max_length=105, help_text="Zip", null=True, blank=True)
    C_Zoning = models.CharField(max_length=105, help_text="Zoning", null=True, blank=True)

    C_GeoLat = models.CharField(max_length=105, help_text="Latitude", null=True, blank=True)
    C_GeoLong = models.CharField(max_length=105, help_text="Longitude", null=True, blank=True)
    C_SaleOrRent = models.CharField(max_length=105, help_text="Sale/Rent", null=True, blank=True)
    C_MRFee = models.CharField(max_length=105, help_text="Mello-Roos Fees Y/N", null=True, blank=True)
    C_L_Type = models.ForeignKey(Type, null=True, blank=True)

    image_thumbnail_270 = models.ImageField(upload_to='thumbnail_270', null=True, blank=True)
    image_thumbnail_100 = models.ImageField(upload_to='thumbnail_100', null=True, blank=True)
    video_review = models.CharField(help_text="youtube embed link", max_length=355, null=True, blank=True)
    extra_photos = FileBrowseField('Extra Photos', help_text='Folder where the extra photos of the listing is located.',\
                                     max_length=255, null=True, blank=True)
    objects = CachingManager()
    #objects = CachedManager
    #objects = CachedManager()


    class Meta:
        db_table = 'mls_listing'

    def __unicode__(self):
        return "%s" % self.C_MLSNo

    @property
    def get_admin_url(self):
        info = (self._meta.app_label, self._meta.module_name)
        admin_url = reverse('admin:%s_%s_change' % info, args=(self.pk,))
        return admin_url


    @property
    def get_badge(self):
        """
        returns a string that will be used as a badge/mark on the front end.
        returns False if there's no applicable badge.
        Badge string can be: featured, sold, reduced, reviewed
        """

        try:
            featured = self.featuredlisting
        except:
            featured = False

        if self.C_Status.name == 'SOLD':
            return 'badge_Sold'

        #if present in featured listing
        elif featured:
            return 'badge_Featured'

        #if extra comments and/or media are present
        elif SellerComment.objects.filter(listing=self):
            return 'badge_Reviewed'

        #if present in changed prices table and reduced
        #elif self:
        #    return 'Reduced'
        return 'no_badge'

    @classmethod
    def get_reviewed_listing(cls, max_length=5, review_type='all', listing=None):
        #improve code
        #add type parameter: 'all', 'comments', 'photos'
        #improve filtering
        reviewed_listing = Listing.objects.none()

        if review_type == 'all':
            seller_comment = SellerComment.objects.all()[:max_length]

            for sc in seller_comment:
                reviewed_listing = reviewed_listing|Listing.objects.filter(pk=sc.listing.pk)

            if len(seller_comment) < max_length:
                listing_gallery = Gallery.objects.filter(listing__isnull=False)[:max_length-len(seller_comment)]
                for g in listing_gallery:
                    reviewed_listing = reviewed_listing|Listing.objects.filter(pk=g.listing.pk)

        elif review_type == 'photos':
            reviewed_listing = reviewed_listing|Listing.objects.filter(extra_photos__isnull=False)

        elif review_type == 'comments':
            seller_comment = SellerComment.objects.filter(selected_review=True)
            for sc in seller_comment:
                reviewed_listing = reviewed_listing|Listing.objects.filter(pk=sc.listing.pk).order_by('-C_AskPrice')

        return reviewed_listing.order_by('-C_LstDate')[:max_length]

    @classmethod
    def get_biweekly_average(cls, status, date_range):
        start_date = date_range[0]
        end_date = date_range[1]
        biweekly_averages = []
        sales_count = 0
        date_ctr = end_date

        while date_ctr > start_date:
            tmp_range = [date_ctr]
            date_ctr = date_ctr + relativedelta(days=-14)
            tmp_range.insert(0, date_ctr)
            date_ctr = date_ctr + relativedelta(days=-1)
            count = cls.objects.filter(C_Status__name=status, C_CloseDate__range=tmp_range).count()
            biweekly_averages.append((tmp_range, count))
            sales_count += count

        try:
            biweekly_avg = sales_count/len(biweekly_averages)
        except ZeroDivisionError:
            biweekly_avg = 0

        return {'avg_details':biweekly_averages, 'biweekly_avg':biweekly_avg}

    @classmethod
    def get_stat_aggregate(cls, status, date_range, **kwargs):
        sold_items = cls.objects.filter(C_Status__name=status, C_CloseDate__range=date_range, **kwargs).order_by('C_SoldPrice')
        try:
            median = sold_items[len(sold_items)/2].C_SoldPrice
        except IndexError:
            median = 0
        average = cls.objects.filter(C_Status__name=status, C_CloseDate__range=date_range, **kwargs).aggregate(Avg('C_SoldPrice'))
        if average.get('C_SoldPrice__avg') == None:
            avg_sale_price = 0
        else:
            avg_sale_price = average.get('C_SoldPrice__avg')


        return {'median_sale_price': median, 'average_sale_price':avg_sale_price}

    @classmethod
    def get_market_segment_report(cls, segment_field='C_AskPrice', market_segments=[(300000, 499999),(500000, 999999), (1000000,)], **kwargs):
        segment_report = {}

        for segment in market_segments:
            #Use range function for segments with 2 values, use gte function for segments with 1 value
            if len(segment) == 2:
                kwargs.update({segment_field+'__range':[segment[0], segment[1]]})
                report_tmp = cls.objects.filter(**kwargs).count()
                segment_report[str(segment[0])+'_'+str(segment[1])] = report_tmp

            elif len(segment) == 1:
                kwargs.update({segment_field+'__gte':segment[0]})
                report_tmp = cls.objects.filter(**kwargs).count()
                segment_report[str(segment[0])] = report_tmp

        return segment_report


    @classmethod
    def market_report(cls, sub_type, context={}):

        def get_inventory_change(prev_count, new_count):
            difference = new_count - prev_count
            try:
                percent = float(difference) / float(new_count) * 100
            except ZeroDivisionError:
                percent = 0
            if percent > 0:
                change = 'increase'
            else:
                change = 'decrease'
            return '%.2f' % (percent), change


        def count_listing_type(query, ltype):
            return query.filter(C_L_Type__name=ltype).count()

        today = datetime.today().date()
        year_start = date(today.year, 1, 1)

        day_before = today + relativedelta(days=-1)
        active_listings = cls.objects.filter(C_Status__name='ACTIVE')
        day_before_previous_year =  day_before + relativedelta(years=-1)

        current_start_two_week_range = today + relativedelta(days=-14)
        two_week_range = [current_start_two_week_range, today]

        prev_week_end = current_start_two_week_range + relativedelta(days=-1)
        prev_week_start = prev_week_end + relativedelta(days=-14)

        prior_prev_week_end = prev_week_start + relativedelta(days=-1)
        prior_prev_week_start = prior_prev_week_end + relativedelta(days=-14)

        prev_year_date = today + relativedelta(years=-1)
        prev_year_start_week = prev_year_date + relativedelta(days=-14)
        prev_year_start = date(prev_year_date.year, 1, 1)


        #active listings
        context['single_family_count'] = count_listing_type(active_listings, 'Single Family')
        context['condo_count'] = count_listing_type(active_listings, 'Condominium/Townhouse')
        context['active_count'] = active_listings.count()
        context['short_sale_count'] = active_listings.filter(C_SubStatus__iexact='Short Sale').count()
        context['reo_count'] = active_listings.filter(C_SubStatus__iexact='REO').count()

        sold_prop_previous = cls.objects.filter(C_Status__name='SOLD', C_LstDate__lte=day_before_previous_year,\
                                                    C_CloseDate__gte=day_before_previous_year).count()
        active_prop_previous = cls.objects.filter(C_Status__name='ACTIVE', C_LstDate__lte=day_before_previous_year).count()

        context['previous_year_active'] = sold_prop_previous + active_prop_previous
        context['previous_year'] = day_before_previous_year.year
        context['current_year'] = today.year
        context['year_to_year_change'], context['yty_change'] = get_inventory_change(context['previous_year_active'], context['active_count'])


        context['report_range'] = 'week'
        context['new_listings_count'] = active_listings.filter(C_LstDate__range=[today + relativedelta(weeks=-1), \
                                                                                    day_before]).count()
        prev_listings_count = active_listings.filter(C_LstDate__range=[today + relativedelta(weeks=-2), \
                                                                        day_before + relativedelta(weeks=-1)]).count()
        context['inventory_change_percent'], context['icp_change'] = get_inventory_change(prev_listings_count, context['new_listings_count'])

        #sold listings
        context['current_properties_sold'] = cls.sales_summary([current_start_two_week_range, today])
        context['prev_year_properties_sold'] = cls.sales_summary([prev_year_start_week, prev_year_date])
        context['prev_properties_sold'] = cls.sales_summary([prev_week_start, prev_week_end])
        context['prior_prev_prop_sold'] = cls.sales_summary([prior_prev_week_start, prior_prev_week_start])
        context['segment_report'] = cls.get_market_segment_report(segment_field='C_SoldPrice', market_segments=[(0, 499999),(500000, 999999), (1000000,)],
                                                                    C_Status__name='SOLD', C_CloseDate__range=two_week_range)

        context['current_biweekly_avg'] = cls.get_biweekly_average(status='SOLD', date_range=[year_start, today])
        context['prev_year_biweekly_avg'] = cls.get_biweekly_average(status='SOLD', date_range=[prev_year_start, prev_year_date])
        context['current_stat_aggregate'] = cls.get_stat_aggregate(status='SOLD', date_range=[year_start, today])
        context['prev_year_stat_aggregate'] = cls.get_stat_aggregate(status='SOLD', date_range=[prev_year_start, prev_year_date])
        context['ytd_segment_report'] = cls.get_market_segment_report(segment_field='C_SoldPrice', market_segments=[(0, 499999),(500000, 999999), (1000000,)],
                                                                    C_Status__name='SOLD', C_CloseDate__range=[year_start, today])
        context['prev_ytd_segment_report'] = cls.get_market_segment_report(segment_field='C_SoldPrice', market_segments=[(0, 499999),(500000, 999999), (1000000,)],
                                                                    C_Status__name='SOLD', C_CloseDate__range=[prev_year_start, prev_year_date])
        context['current_luxury_stat_aggregate'] = cls.get_stat_aggregate(status='SOLD', date_range=[year_start, today])
        context['prev_year_luxury_stat_aggregate'] = cls.get_stat_aggregate(status='SOLD', date_range=[prev_year_start, prev_year_date])


        return context


    @classmethod
    def active_inventory(cls):
        """
        returns summary of current active inventory
        """
        def days_hours_minutes(td):
            return td.days, td.seconds//3600, (td.seconds//60)%60

        RESIDENCE_TYPES = ['Boat Slip', 'Mobile Home', 'Single Family', 'Condominium/Townhouse',
                           'Share Ownership']

        PRICE_RANGE = [('<','299999'), ('300000','499999'), ('500000','699999'), \
                        ('700000','799999'), ('800000','999999'), ('1000000','2999999'), ('>','3000000')]

        SUB_STATUS = ['Standard', 'REO', 'Short Sale']

        COMMUNITIES = [community.name for community in Community.objects.exclude(name__in=\
                                                        ['SurroundingArea', 'Sierra County', 'None']).order_by('name')]

        DOM = [('<','29'), ('30','59'), ('60','89'), ('90','119'), ('120','159'), ('>','160')]

        stat_data = {}

        active_properties = cls.objects.filter(C_Status__name='ACTIVE')

        stat_data['active_listing'] = active_properties.count()

        stat_data['residence_type'] = {'title':'Active Listings by Property Type'}
        stat_data['residence_type']['data'] = OrderedDict()

        stat_data['price_range'] = {'title': 'Active Listings by Price Range'}
        stat_data['price_range']['data'] = OrderedDict()

        stat_data['community'] = {'title': 'Active Listings by Community'}
        stat_data['community']['data'] = OrderedDict()

        stat_data['sub_status'] = {'title': 'Active Listings by Sale Type'}
        stat_data['sub_status']['data'] = OrderedDict()

        stat_data['dom'] = {'title': 'Active Listings by Days On Market'}
        stat_data['dom']['data'] = OrderedDict()

        for r_type in RESIDENCE_TYPES:
            stat_data['residence_type']['data'][r_type] = active_properties.filter(C_L_Type__name=r_type).count()

        for price in PRICE_RANGE:
            if price[0] == '<':
                stat_data['price_range']['data']['less than equal $299,999'] = active_properties.filter(C_AskPrice__lte=\
                                                                                                        int(price[1])).count()
            elif price[0] == '>':
                stat_data['price_range']['data']['$3,000,000 and greater'] = active_properties.filter(C_AskPrice__gte=\
                                                                                                        int(price[1])).count()
            else:
                stat_data['price_range']['data']['$'+intcomma(price[0])+' - $'+ intcomma(price[1])] = active_properties\
                                                                                            .filter(C_AskPrice__range=[int(price[0]),\
                                                                                                    int(price[1])]).count()

        for community in COMMUNITIES:
            stat_data['community']['data'][community] = active_properties.filter(C_Community__name=community).count()

        for status in SUB_STATUS:
            stat_data['sub_status']['data'][status] = active_properties.filter(C_SubStatus=status).count()

        for dom in DOM:
            if dom[0] == '<':
                stat_data['dom']['data']['less than equal 29 days'] = active_properties.extra(where=\
                                                                                                ["ABS(DATEDIFF(C_LstDate, NOW())) <= %s"],\
                                                                                                params=[dom[1]]).count()
            elif dom[0] == '>':
                stat_data['dom']['data']['160 days and greater'] = active_properties.extra(where=\
                                                                                            ["ABS(DATEDIFF(C_LstDate, NOW())) >= %s"],\
                                                                                            params=[dom[1]]).count()
            else:
                stat_data['dom']['data'][dom[0]+' - '+dom[1]+' days'] = active_properties.extra(where=\
                                                                        ["ABS(DATEDIFF(C_LstDate, NOW())) between %s and %s"],\
                                                                        params=[dom[0], dom[1]]).count()

        return {'object':stat_data, 'json':json.dumps(stat_data)}

    @classmethod
    def sales_summary(cls, date_range):
        """
        returns the property sales based on the range given. Range should be a list of datetime object
        returns two data formats: json string and standard python object
        """
        RESIDENCE_TYPES = ['Boat Slip', 'Mobile Home', 'Single Family', 'Condominium/Townhouse',
                           'Share Ownership']
        PRICE_RANGE = [('<','299999'), ('300000','499999'), ('500000','699999'), \
                        ('700000','799999'), ('800000','999999'), ('1000000','2999999'), ('>','3000000')]
        SUB_STATUS = ['Standard', 'REO', 'Short Sale']
        DOM = [('<','29'), ('30','59'), ('60','89'), ('90','119'), ('120','159'), ('>','160')]
        COMMUNITIES = [community.name for community in Community.objects.exclude(name__in=\
                                                        ['SurroundingArea', 'Sierra County', 'None']).order_by('name')]
        FINANCING_TYPES = ['Cash', 'Conventional', 'Owner Finance',]

        sold_properties = cls.objects.filter(C_Status__name='SOLD', C_CloseDate__range=date_range)

        inventory = {}
        inventory['date'] = str(date_range[0])
        inventory['residence_type'] = {}
        inventory['sub_status'] = {}
        inventory['price_range'] = {'title': 'Sales by Price Range'}
        inventory['price_range']['data'] = OrderedDict()
        inventory['community'] = {'title': 'Sales by Community'}
        inventory['community']['data'] = OrderedDict()
        inventory['dom'] = {'title': 'Sales by Days On Market'}
        inventory['dom']['data'] = OrderedDict()
        inventory['financing_type'] = {'title': 'Sales by Type of Financing'}
        inventory['financing_type']['data'] = OrderedDict()
        inventory['total_sold'] = sold_properties.count()

        for r_type in RESIDENCE_TYPES:
            inventory['residence_type'][r_type] = sold_properties.filter(C_L_Type__name=r_type).count()

        for status in SUB_STATUS:
            inventory['sub_status'][status] = sold_properties.filter(C_SubStatus=status).count()

        for price in PRICE_RANGE:
            if price[0] == '<':
                inventory['price_range']['data']['less than equal $299,999'] = sold_properties.filter(C_SoldPrice__lte=int(price[1])).count()
            elif price[0] == '>':
                inventory['price_range']['data']['$3,000,000 and greater'] = sold_properties.filter(C_SoldPrice__gte=int(price[1])).count()
            else:
                inventory['price_range']['data']['$'+intcomma(price[0])+' - $'+ intcomma(price[1])] = sold_properties\
                                                                                            .filter(C_SoldPrice__range=[int(price[0]),\
                                                                                                    int(price[1])]).count()

        for community in COMMUNITIES:
            inventory['community']['data'][community] = sold_properties.filter(C_Community__name=community).count()

        for dom in DOM:
            if dom[0] == '<':
                inventory['dom']['data']['less than equal 29 days'] = sold_properties.extra(where=\
                                                                                            ["ABS(DATEDIFF(C_LstDate, C_CloseDate)) <= %s"],\
                                                                                            params=[dom[1]]).count()
            elif dom[0] == '>':
                inventory['dom']['data']['160 days and greater'] = sold_properties.extra(where=\
                                                                                        ["ABS(DATEDIFF(C_LstDate, C_CloseDate)) >= %s"],\
                                                                                        params=[dom[1]]).count()
            else:
                inventory['dom']['data'][dom[0]+' - '+dom[1]+' days'] = sold_properties.extra(where=\
                                                                        ["ABS(DATEDIFF(C_LstDate, C_CloseDate)) between %s and %s"],\
                                                                        params=[dom[0], dom[1]]).count()
        for f_type in FINANCING_TYPES:
            inventory['financing_type']['data'][f_type] = sold_properties.filter(C_HowSold__iexact=f_type).count()

        return {'object': inventory, 'json':json.dumps(inventory)}



    @classmethod
    def inventory_summary(cls, status, date_range):
        """
        returns inventory summary based on the the given parameters.
        date_range should be a list of datetime objects
        """
        RESIDENCE_TYPES = ['Boat Slip', 'Mobile Home', 'Single Family', 'Condominium/Townhouse',
                           'Share Ownership']
        SUB_STATUS = ['Standard', 'REO', 'Short Sale']
        PRICE_RANGE = [('<','299999'), ('300000','499999'), ('500000','699999'), \
                        ('700000','799999'), ('800000','999999'), ('1000000','2999999'), ('>','3000000')]
        DOM = [('<','29'), ('30','59'), ('60','89'), ('90','119'), ('120','159'), ('>','160')]
        COMMUNITIES = [community.name for community in Community.objects.exclude(name__in=\
                                    ['SurroundingArea', 'Sierra County', 'None']).order_by('name')]

        inventory_summary = {}
        inventory_summary['residence_type'] = OrderedDict()
        inventory_summary['sub_status'] = OrderedDict()
        inventory_summary['sold_price_range'] = OrderedDict()
        inventory_summary['community'] = OrderedDict()
        inventory_summary['sold_dom'] = OrderedDict()

        inventory = cls.objects.filter(C_Status__name=status)

        if date_range:
            inventory = inventory.filter(C_LstDate__range=date_range)

        inventory_summary['total'] = inventory.count()

        for r_type in RESIDENCE_TYPES:
            inventory_summary['residence_type'][r_type] = inventory.filter(C_L_Type__name=r_type).count()

        for status in SUB_STATUS:
            inventory_summary['sub_status'][status] = inventory.filter(C_SubStatus=status).count()

        for price in PRICE_RANGE:
            if price[0] == '<':
                inventory_summary['sold_price_range']['less than equal $299,999'] = inventory.filter(C_SoldPrice__lte=int(price[1])).count()
            elif price[0] == '>':
                inventory_summary['sold_price_range']['$3,000,000 and greater'] = inventory.filter(C_SoldPrice__gte=int(price[1])).count()
            else:
                inventory_summary['sold_price_range']['$'+intcomma(price[0])+' - $'+ intcomma(price[1])] = inventory\
                                                                                            .filter(C_SoldPrice__range=[int(price[0]),\
                                                                                                    int(price[1])]).count()

        for community in COMMUNITIES:
            inventory_summary['community'][community] = inventory.filter(C_Community__name=community).count()

        for dom in DOM:
            if dom[0] == '<':
                inventory_summary['sold_dom']['less than equal 29 days'] = inventory.extra(where=\
                                                                                    ["ABS(DATEDIFF(C_LstDate, NOW())) <= %s"],\
                                                                                    params=[dom[1]]).count()
            elif dom[0] == '>':
                inventory_summary['sold_dom']['160 days and greater'] = inventory.extra(where=\
                                                                                ["ABS(DATEDIFF(C_LstDate, NOW())) >= %s"],\
                                                                                params=[dom[1]]).count()
            else:
                inventory_summary['sold_dom'][dom[0]+' - '+dom[1]+' days'] = inventory.extra(where=\
                                                                        ["ABS(DATEDIFF(C_LstDate, NOW())) between %s and %s"],\
                                                                        params=[dom[0], dom[1]]).count()

        data = {'object': inventory_summary,
                'json':   json.dumps(inventory_summary),
                'date':   {'object':date_range,
                           'json':json.dumps([date_range[0].strftime("%d/%b/%y"),
                                              date_range[1].strftime("%d/%b/%y")])
                           }
                }

        return data

    @classmethod
    def sales_price_range(cls, date_range, price_ranges):
        """
        returns sales by price range in a given date range
        """
        floor_val = 0;
        ceiling_val = 0;
        sales = {}
        sales['data'] = {}
        sales['total'] = cls.objects.filter(C_Status__name='SOLD',
                                            C_CloseDate__range=date_range).count()
        for price_range in price_ranges:
            pr_key = str(price_range[0]) +"-"+ str(price_range[1])
            if price_range[1] > ceiling_val:
                ceiling_val = price_range[1]

            sales['data'][pr_key] = cls.objects.filter(C_Status__name='SOLD',
                                               C_AskPrice__range=price_range,
                                               C_CloseDate__range=date_range).count()

        sales['data'][">"+str(ceiling_val)] = cls.objects.filter(C_Status__name='SOLD',
                                                         C_AskPrice__gt=ceiling_val,
                                                         C_CloseDate__range=date_range).count()

        return {'object':sales, 'json':json.dumps(sales)}

    @classmethod
    def median_average(cls, date_range):

        def median_value(queryset,term):
            count = queryset.count()
            return queryset.values_list(term, flat=True).order_by(term)[int(round(count/2))]

        sale_queryset = cls.objects.filter(C_Status__name='SOLD',
                                           C_CloseDate__range=date_range)
        sale_count = sale_queryset.count()
        average =  cls.objects.filter(C_Status__name='SOLD',
                                        C_CloseDate__range=date_range).aggregate(Avg('C_SoldPrice'))
        try:
            sale_median = median_value(sale_queryset, 'C_SoldPrice')
        except IndexError:
            sale_median = 0

        return {'sale_count':sale_count, 'average':average,
                'median': sale_median, 'start_date':date_range[0].strftime("%d/%b/%y"),
                'end_date':date_range[1].strftime("%d/%b/%y")}

    def recent_sales_price_range(self):
        """
        returns recent sales of properties in the same price range
        """
        pass

    def address_location(self):
        if self.C_Community.name != 'SurroundingArea':
            return u'%s, %s, %s' % (self.C_Community, self.C_State, self.ZipCode())
        return u'%s, %s, %s' % (self.C_City, self.C_State, self.ZipCode())

    def get_picture_url(self):
        # picture = '/static/img/tmp/property-small-3.png'
        # return picture
        try:
            picture = Img.objects.filter(C_L_ListingID=self.C_MLSNo)[0]
            if picture.C_PATH == 'REFERENCE_ONLY':
                picture = picture.C_URL
            else:
                picture = 'http://media.cblaketahoe.com' +  picture.C_PATH.split('media')[1]
        except:
            picture = 'http://static.cblaketahoe.com/img/tmp/property-small-3.png'
        return picture

    def get_picture_url_normal(self):
        # picture = '/static/img/tmp/property-small-3.png'
        # return picture
        try:
            picture = Img.objects.filter(C_L_ListingID=self.C_MLSNo)[0]
            if picture.C_PATH == 'REFERENCE_ONLY':
                picture = picture.C_URL
            else:
                picture = '/media' +  picture.C_PATH.split('media')[1]
        except:
            picture = '/static/img/tmp/property-small-3.png'
        return picture

    def ZipCode(self):
        return self.C_Zip.replace('-', '')

    def get_thumbnail_270(self):        
        
        try:
            if os.path.isfile(self.image_thumbnail_270.name):
                if len(self.image_thumbnail_270.name.split('media')) > 1:
                    return 'http://media.cblaketahoe.com' + self.image_thumbnail_270.name.split('media')[1]
                else:
                    return self.get_picture_url()
            else:
                return self.get_picture_url()
        except ValueError:
            return self.get_picture_url()

    def get_thumbnail_100(self):
        try:
            if os.path.isfile(self.image_thumbnail_100.name):
                if len(self.image_thumbnail_100.name.split('media')) > 1:
                    return 'http://media.cblaketahoe.com' + self.image_thumbnail_100.name.split('media')[1]
                else:
                    return self.get_picture_url()    
            else:
                return self.get_picture_url()
        except ValueError:
            return self.get_picture_url()

    def create_thumbnails(self):
        infile = '/opt/webapps/cblaketahoe/cblaketahoe/cblt' + self.get_picture_url_normal()
        if os.path.isfile(infile):
            print 'creating thumbnail'
            outfile_270 = os.path.splitext(infile)[0] + "_s270.jpg"
            outfile_100 = os.path.splitext(infile)[0] + "_s100.jpg"
            try:
                im = Image.open(infile)
                im.thumbnail(size_270, Image.ANTIALIAS)
                im.save(outfile_270, "JPEG")

                im = Image.open(infile)
                im.thumbnail(size_100, Image.ANTIALIAS)
                im.save(outfile_100, "JPEG")

                self.image_thumbnail_270.name = outfile_270
                self.image_thumbnail_100.name = outfile_100
                print 'thumbnail created'
                self.save()

            except Exception as e:
                print e
                print "cannot create thumbnail for '%s'" % infile
        else:
            print "%s not found" % infile


        return

    def get_detailed_property_url(self):
        address_slug = slugify(str(self.get_property_full_address()) +' '+ str(self.C_Zip))
        return reverse('detailed_property', args=(self.C_Community.slug, self.C_Area.slug, self.C_MLSNo, address_slug, ))

    def get_related_property(self, length=10, status='ACTIVE'):
        """
        returns related properties based on the arguments given.
        """
        related_prop = Listing.objects.filter(C_Status__name=status, C_Community=self.C_Community, C_Area=self.C_Area)\
                        .exclude(C_MLSNo=self.C_MLSNo).order_by('-C_AskPrice')[:15]

        return related_prop

    def get_property_full_address(self):
        full_address = u'%s, %s, %s' % (self.C_Address, self.C_City, self.C_State,)
        return full_address

    def get_seller_comment(self):

        try:
            seller_comment = SellerComment.objects.filter(listing=self)
        except SellerComment.DoesNotExist:
            seller_comment = None

        return seller_comment

    def price_reductions(self):
        """
        returns a list of PriceChange reductions
        """
        price_reductions = PriceChange.get_listing_reductions(listing=self)

        return price_reductions

    def similar_properties(self):        
        similar_listings = self.__class__.objects.filter(C_Address=self.C_Address, C_LstDate__lt = self.C_LstDate).exclude(C_MLSNo=self.C_MLSNo)
        if self.C_L_Type.name == 'Condominium/Townhouse':
            print 'Condominium/Townhouse'
            similar_listings = similar_listings.filter(C_Address2=self.C_Address2)
        return similar_listings

    def last_reduction(self):

        price_reductions = self.price_reductions()

        if price_reductions:
            return price_reductions[0]
        else:
            return None

    def price_per_sqr_feet(self):
        return round(float(self.C_AskPrice)/float(self.C_SqFeet),2)

    def get_recent_sales(self):
        """
        returns recent sales with the closes value to the listing.
        """
        recent_sales = []
        base_price = self.C_AskPrice
        price_range_allowance = float(base_price) * 0.20
        days_range = 18 * 30

        ceiling_val = float(base_price) + price_range_allowance
        floor_val = float(base_price) - price_range_allowance
        ceiling_date = date.today()
        floor_date = ceiling_date - timedelta(days=days_range)


        lt_base_price = Listing.objects.filter(C_SoldPrice__range=[floor_val, base_price],
                                               C_CloseDate__range=[floor_date, ceiling_date],
                                               C_Status__name__iexact="SOLD",
                                               C_Community=self.C_Community).order_by('-C_CloseDate')[:2]

        gt_base_price = Listing.objects.filter(C_SoldPrice__range=[base_price, ceiling_val],
                                               C_CloseDate__range=[floor_date, ceiling_date],
                                               C_Status__name__iexact="SOLD",
                                               C_Community=self.C_Community).order_by('-C_CloseDate')[:3]

        recent_sales.extend(lt_base_price)
        recent_sales.extend(gt_base_price)

        #if recent sales is less than 5 items, pull recent sales from other communities
        if len(recent_sales) < 5:
            other_recent_sales = Listing.objects.filter(C_SoldPrice__range=[floor_val, ceiling_val],
                                                        C_CloseDate__range=[floor_date, ceiling_date])\
                                                        .exclude(C_Community=self.C_Community)\
                                                        .order_by('-C_CloseDate')[:5-len(recent_sales)]
            recent_sales.extend(other_recent_sales)


        #if recent sales is still less than 5, extend search parameters
        if len(recent_sales) < 5:
            new_range = 36 * 30
            price_range_allowance = float(base_price) * 0.50
            ceiling_val = float(base_price) + price_range_allowance
            floor_date = ceiling_date - timedelta(days=new_range)


            other_recent_sales = Listing.objects.filter(C_SoldPrice__lte=ceiling_val,
                                                        C_CloseDate__range=[floor_date, ceiling_date])\
                                                        .exclude(C_MLSNo=self.C_MLSNo)\
                                                        .order_by('-C_SoldPrice')[:5-len(recent_sales)]
            recent_sales.extend(other_recent_sales)

        return recent_sales



class Img(models.Model):
    C_L_ListingID = models.CharField(max_length=102)
    C_INDEX = models.CharField(max_length=102)
    C_URL = models.CharField(max_length=600)
    C_PATH = models.CharField(max_length=102)
    C_THUMB = models.ImageField(upload_to='thumbnail_100_img', null=True, blank=True, max_length=255)
    C_LARGE_THUMB = models.ImageField(upload_to='thumbnail_540_img', null=True, blank=True, max_length=255)

    def __unicode__(self):
        return u'%s' % (self.C_L_ListingID,)

    def create_thumbnail(self):
        if not self.C_THUMB:
            try:
                image = self.C_PATH.split('/mnt')[1]
                #print image
                #print settings.PROJECT_ROOT
		DEFAULT_ROOT = "/opt/webapps/cblaketahoe/cblaketahoe/cblt"
                infile = DEFAULT_ROOT + image

                if os.path.exists(infile):
                    print 'creating thumbnail'
                    outfile_100 = os.path.splitext(infile)[0] + "_s100.jpg"
                    try:
                        im = Image.open(infile)
                        im.thumbnail(size_100, Image.ANTIALIAS)
                        im.save(outfile_100, "JPEG")

                        self.C_THUMB.name = outfile_100
                        print 'thumbnail created'
                        self.save()

                    except Exception as e:
                        print e
                        print "cannot create thumbnail for '%s'" % infile
                else:
                    print "%s not found" % infile
            except IndexError:
                image = self.C_URL
                print 'error'
        else:
            print 'existing thumbnail exists'
        return

    def create_large_thumbnail(self):
        if not self.C_LARGE_THUMB:
            try:
                image = self.C_PATH.split('/mnt')[1]
                print 'large image'
                #print image
                #infile = settings.PROJECT_ROOT + image
                DEFAULT_ROOT = "/opt/webapps/cblaketahoe/cblaketahoe/cblt"
                infile = DEFAULT_ROOT + image
                if os.path.exists(infile):
                    print 'creating large thumbnail'
                    outfile_540 = os.path.splitext(infile)[0] + "_s540.jpg"
                    try:
                        im = Image.open(infile)
                        im.thumbnail(size_540, Image.ANTIALIAS)
                        im.save(outfile_540, "JPEG")

                        self.C_LARGE_THUMB.name = outfile_540
                        print 'thumbnail created'
                        self.save()

                    except Exception as e:
                        print e
                        print "cannot create thumbnail for '%s'" % infile
                else:
                    print "%s not found" % infile
            except IndexError:
                image = self.C_URL
                print 'error'
        else:
            print 'existing thumbnail exists'
        return

    def get_thumbnail(self):
        try:

            if os.path.exists(self.C_THUMB.name):
                return 'http://media.cblaketahoe.com/images/' + self.C_THUMB.name.split('/')[-1]
            else:
                return self.get_image()
        except ValueError:
            return self.get_image()

    def get_image(self):
        try:
            if os.path.exists(self.C_PATH):
                return 'http://media.cblaketahoe.com/images/' + self.C_PATH.split('/')[-1]
            else:
                return 'http://static.cblaketahoe.com/img/tmp/property-small-3.png'
        except IndexError:
            return 'http://static.cblaketahoe.com/img/tmp/property-small-3.png'

        '''
        try:
            if os.path.isfile(self.C_LARGE_THUMB.url):
                return self.C_LARGE_THUMB.url.split('cblt')[1]
            else:
                try:
                    return self.C_PATH.split('/mnt/images')[1]
                except IndexError:
                    return self.C_URL
        except:
        '''


class FeaturedListing(models.Model):
    listing = models.OneToOneField(Listing)

    def __unicode__(self):
        return u'%s' % (str(self.listing))


class SellerComment(models.Model):
    """
    Contains the site admin comment for a particular listing
    """
    listing = models.ForeignKey(Listing)
    comment = models.TextField(max_length=3000)
    selected_review = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s' % (str(self.comment))


class LocationType(models.Model):
    """
    articles should be able to be categorized by topics/article types (trails, beaches, marinas,
    parks, businesses, events).
    """
    name = models.CharField(max_length=100, unique=True,)
    marker = models.ImageField(upload_to='markers', null=True, blank=True)
    blog_image = models.ImageField(upload_to='blog_image', null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.name)

    def get_marker(self):
        if self.marker:
            return str(self.marker.url)
        else:
            return None

    def get_dict_form(self):
        """
        returns an array of serializable dict of the LocationType array
        """
        serializable_loc = ({'id': self.id,
                             'name': self.name,
                             'marker': self.get_marker()})

        return serializable_loc


class CustomImage(models.Model):
    name = models.CharField(max_length=35, null=True, blank=True)
    image = models.ImageField(upload_to='custom_images')
    listing = models.ForeignKey(Listing, null=True, blank=True)


class SiteContent(models.Model):
    """
    articles should be able to be categorized by topics/article types (trails, beaches, marinas,
    parks, businesses, events).
    They should also be categorized by location (city, area, neighborhood) so that we can display
    snippets of the content on the property pages.
    For the following example http://www.cblaketahoe.com/realestate/listing/2901130 we would display
    the HOA photos for the dollar point neighborhood. For beaches we would display articles that were
    categorized as area=north Lake Tahoe and/or Tahoe city.
    """
    title = models.CharField(max_length=100)
    short_desc = models.CharField(max_length=140, null=True, blank=True)
    content = models.TextField(max_length=5000, null=True, blank=True)
    city = models.ManyToManyField(City, null=True, blank=True)
    community = models.ManyToManyField(Community, null=True, blank=True)
    area = models.ManyToManyField(Area, null=True, blank=True)
    gallery = models.ForeignKey('photologue.Gallery', null=True, blank=True)
    thumbnail = models.ImageField(upload_to='site_contents_thumbnail', blank=True, null=True)
    location_type = models.ManyToManyField(LocationType)
    order = models.IntegerField(null=True, blank=True)
    latitude = models.CharField(max_length='100', null=True, blank=True)
    longitude = models.CharField(max_length='100', null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    website = models.URLField(null=True, blank=True)
    is_blog = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % (self.title)

    def get_thumbnail(self):
        if self.thumbnail:
            return self.thumbnail.url
        else:
            return '/static/img/lake-tahoe.jpg'

    def get_absolute_url(self):
        return reverse('content_page', kwargs={'content_id':self.id, 'content_slug':slugify(self.title)})

    def is_plottable(self):
        """
        returns true if content has geolocations
        """
        if bool(self.latitude) and bool(self.longitude):
            return True

        return False

    @classmethod
    def get_content_by_loctype(cls, community, area):
        contents = cls.objects.filter(Q(community=community) | Q(area=area)).distinct()
        content_groups = {}

        for content in contents:
            #for every location type associated with the content add the content in that group
            for loc_type in content.location_type.all():

                if not content_groups.has_key(loc_type):
                    content_groups[loc_type] = []

                content_groups[loc_type].append(content)

        return content_groups


    def is_full_content(self):
        """
        returns true if either or all of the following attributes is filled up:
        content, image
        """
        if bool(self.content) or bool(self.gallery):
            return True

        return False

    def get_json_form(self):
        """
        returns a serializable dict object; same as the serialize method
        """
        #serialize location_type objects
        loc_types = self.location_type.all()
        loc_types = [loc.get_dict_form() for loc in loc_types]

        json_form = {'id': self.id,
                     'title': self.title,
                     'short_desc': self.short_desc,
                     'content': self.content,
                     'latitude': self.latitude,
                     'longitude': self.longitude,
                     'order': self.order,
                     'location_type': loc_types}

        return json_form


class PriceChange(CachingMixin, models.Model):
    """
    Stores listing record of price reductions per update for the residential property type
    """
    timestamp = models.DateTimeField(auto_now_add=True)
    listing = models.ForeignKey(Listing)
    prev_price = models.DecimalField(decimal_places=2, max_digits=12)
    new_price = models.DecimalField(decimal_places=2, max_digits=12)
    objects = CachingManager()

    @classmethod
    def get_recent_reduced(cls, max_length=15):
        """
        returns the recently reduced prices of the listings based on the PriceChange table.
        """
        #@here philip fix
        #reduced_listings = PriceChange.objects.none()

        reduced_listings = cls.objects.select_related('listing__C_Community', 'listing', 'listing__C_City', 'listing__CArea',)\
                                        .filter(new_price__lt=F('prev_price'), new_price=F('listing__C_AskPrice'))\
                                        .order_by('-timestamp')

        '''
        for price_change in price_change_set:
            #check if new price is lesser than prev price and if
            #new price reflects the most recent price of the listing
            if (price_change.new_price < price_change.prev_price and
                price_change.new_price == price_change.listing.C_AskPrice):

                reduced_listings = reduced_listings|PriceChange.objects.filter(pk=price_change.pk)
        '''

        return reduced_listings

    @classmethod
    def get_listing_reductions(cls, listing):
        """
        returns price reduction objects for a given listing instance
        """
        reduced_listings = []
        price_changes = cls.objects.filter(listing=listing).order_by('timestamp')

        for price_change in price_changes:
            #check if new price is lesser than prev price and if
            #new price reflects the most recent price of the listing
            if (Decimal(price_change.new_price) < Decimal(listing.C_OrigPrice)):
                if (Decimal(price_change.new_price) < Decimal(price_change.prev_price)):
                    reduced_listings.append(price_change)
        return reduced_listings

    @property
    def get_price_delta(self):
        """
        returns price delta
        """
        price_delta = self.new_price - self.prev_price

        return price_delta


class BigTable(models.Model):
    """
        Stores predefined computations of commonly used listing set; i.e. recently reduced prices
    """
    function = models.CharField(max_length=100)
    scope = models.CharField(max_length=100)
    listings = models.ManyToManyField(Listing)
    content = models.TextField(null=True, blank=True, max_length=5000)
    timestamp = models.DateTimeField(null=True, blank=True)

    class Meta:
        unique_together = (('function', 'scope'),)

def get_create_city(city):
    city_instance = get_or_none(City, name=city)

    if not city_instance:
        city_instance = City(name=city)
        city_instance.save()

    return city_instance


def get_create_area(area):
    area_instance = None
    if area != "":
        area_instance = get_or_none(Area, name__iexact=area)

        if not area_instance:
            area_instance = Area(name=area)
            area_instance.save()

    return area_instance


def get_create_neighborhood(neighborhood):
    n_instance = get_or_none(Neighborhood, name=neighborhood)

    if not n_instance:
        n_instance = Neighborhood(name=neighborhood)
        n_instance.save()

    return n_instance

def get_create_setting(setting):
    setting_instance = get_or_none(Setting, name=setting)

    if not setting_instance:
        setting_instance = Setting(name=setting)
        setting_instance.save()

    return setting_instance

def get_acreage(acreage_range):
    try:
        acreage = float(acreage_range.split('-')[1])
    except:
        acreage = 0
    return acreage

def get_create_view(views):

    views = [x.strip() for x in views.split(',')]
    views_array = []
    for view in views:
        view_instance = get_or_none(View, name=view)
        if not view_instance:
            view_instance = View(name=view)
            view_instance.save()
        views_array.append(view_instance)
    return views_array


def get_create_community(community):
    if community == None:
        community = 'None'
    community_instance = get_or_none(Community, name=community)

    if not community_instance:
        community_instance = Community(name=community)
        community_instance.save()

    return community_instance


def get_create_hoa_amenities(hoa_amenities):
    amenities = [x.strip() for x in hoa_amenities.split(',')]
    hoa_objects = []
    for hoa in amenities:
        hoa_instance = get_or_none(HOAAmenities, name=hoa)
        if not hoa_instance:
            hoa_instance = HOAAmenities(name=hoa)
            hoa_instance.save()
        hoa_objects.append(hoa_instance)
    return hoa_objects


def get_create_water_amenities(water_amenities):
    amenities = [x.strip() for x in str(water_amenities).split(',')]
    water_objects = []
    for water in amenities:
        water_instance = get_or_none(WaterAmenities, name=water)
        if not water_instance:
            water_instance = WaterAmenities(name=water)
            water_instance.save()
        water_objects.append(water_instance)
    return water_objects


def get_create_status(status):
    status_instance = get_or_none(ListingStatus, name=status)

    if not status_instance:
        status_instance = ListingStatus(name=status)
        status_instance.save()

    return status_instance

def get_create_type(listing_type):
    type_instance = get_or_none(Type, name=listing_type)

    if not type_instance:
        type_instance = Type(name=listing_type)
        type_instance.save()

    return type_instance

def assoc_community_city(community, city):
    try:
        community.city.add(city)
    except:
        pass
    return None

def assoc_community_area(community, area):
    try:
        community.area.add(area)
    except Exception as ex:
        print 'Exception in ASSOC community area'
        print ex.message
    return None


def get_big_table(function, scope):
        try:
            big_table = BigTable.objects.get(function=function, scope=scope)
            big_table.listings.clear()
        except BigTable.DoesNotExist:
            big_table = BigTable(function=function, scope=scope)
            big_table.save()

        return big_table


def create_price_change_set():
    location_classes = [Area, Community, City, Setting]

    for location in location_classes:
        places = location.objects.all()

        for place in places:
            scope = location.__name__ +'_'+ str(place)
            pricechange_listings = place.price_reduced()

            big_table = get_big_table(function='price_reduced', scope=scope)

            for pc in pricechange_listings:
                print "Adding " + str(pc.listing) + " to " + str(big_table.function) + str(big_table.scope)
                big_table.listings.add(pc.listing)

    scope = 'All'
    big_table = get_big_table(function='price_reduced', scope=scope)

    all_pricechanges = PriceChange.get_recent_reduced(max_length=15).exclude(\
                    Q(listing__C_Community__name='SurroundingArea')|Q(listing__C_Community__name='Sierra County'))\
                    .order_by('-timestamp')[:15]

    for pc in all_pricechanges:
        print "Adding " + str(pc.listing) + " to " + str(big_table.function) + str(big_table.scope)
        big_table.listings.add(pc.listing)

def create_recent_sold():
    location_classes = [Community, City, Setting]

    for location in location_classes:
        places = location.objects.all()

        for place in places:
            scope = location.__name__ +'_'+ str(place)
            listings = place.recently_sold()
            big_table = get_big_table(function='recent_sold', scope=scope)

            for listing in listings:
                print "Adding " + str(listing) + " to " + str(big_table.function) + str(big_table.scope)
                big_table.listings.add(listing)

    scope = 'All'
    big_table = get_big_table(function='recent_sold', scope=scope)


def create_statistics():
    location_classes = [Area, Community, City]

    for location in location_classes:
        places = location.objects.all()

        for place in places:
            scope = location.__name__ +'_'+ str(place)

            big_table = get_big_table(function='statistics', scope=scope)

            available_property = place.available_property()
            statistics_data = place.statistics_data()

            stat_text = "Currently there are " + str(available_property) + " properties for sale in " +  str(place) +".\
                        Properties range in asking price from $"+str(intcomma(statistics_data['prices']['C_AskPrice__min'] or 0))+" -\
                         $"+str(intcomma(statistics_data['prices']['C_AskPrice__max'] or 0))+" with an average asking price of \
                         $" + str(intcomma(int(round(statistics_data['prices']['C_AskPrice__avg'] or 0)))) +" and a median asking price of \
                         $"+str(intcomma(statistics_data['median_price']))+".  Price per square foot ranges from \
                         $"+str(intcomma(statistics_data['min_price_per_sqr_feet']))+"/Sq Ft - "+str(intcomma(statistics_data['max_price_per_sqr_feet']))+"/Sq Ft.\
                         On average these " + str(available_property) + " properties have been on the market for "+str(intcomma(round(statistics_data['avg_market_days'], 2)))+"\
                         days. "+str(intcomma(statistics_data['price_change_count']))+" of these properties have reduced their asking price with the average reduction\
                         being $"+str(intcomma(int(round(statistics_data['price_change_avg'], 2))))+".  "+str(statistics_data['distressed_sales'])+"\
                         of the properties are distressed sales (either REO bank owned or Short Sale).  In the past 180 days, "+str(statistics_data['sold_properties_count'])+" properties have sold in "+str(place)+".\
                         Sales prices during this time period ranged from $"+str(intcomma(statistics_data['sold_properties_prices']['C_AskPrice__min'] or 0))+" - $"+str(intcomma(statistics_data['sold_properties_prices']['C_AskPrice__max'] or 0))+".\
                         On average these properties were on the market for "+str(intcomma(round(statistics_data['avg_sold_market_days'], 2))) + " days and final sales prices were on average "+str(round(statistics_data['sold_price_percentage'],2))+"% of the original listing price.\
                             " + str(round(statistics_data['cash_sales'],2)) + "% of the sales were reported as cash sales."

            big_table.content = stat_text
            print str(big_table.function) + str(big_table.scope)
            print big_table.content
            big_table.save()

    settings = Setting.objects.all()

    for setting in settings:
        scope = Setting.__name__ +'_'+ str(setting)

        big_table = get_big_table(function='statistics', scope=scope)
        available_property = setting.available_property()
        statistics_data = setting.statistics_data()

        stat_text = "Currently there are " + str(available_property) + " properties for sale.\
                    Properties range in asking price from $"+str(intcomma(statistics_data['prices']['C_AskPrice__min'] or 0))+" -\
                     $"+str(intcomma(statistics_data['prices']['C_AskPrice__max'] or 0))+" with an average asking price of \
                     $" + str(intcomma(int(round(statistics_data['prices']['C_AskPrice__avg'] or 0)))) +" and a median asking price of \
                     $"+str(intcomma(statistics_data['median_price']))+".  Price per square foot ranges from \
                     $"+str(intcomma(statistics_data['min_price_per_sqr_feet']))+"/Sq Ft - "+str(intcomma(statistics_data['max_price_per_sqr_feet']))+"/Sq Ft.\
                     On average these " + str(available_property) + " properties have been on the market for "+str(intcomma(round(statistics_data['avg_market_days'], 2)))+"\
                     days. "+str(intcomma(statistics_data['price_change_count']))+" of these properties have reduced their asking price with the average reduction\
                     being $"+str(intcomma(int(round(statistics_data['price_change_avg'], 2))))+".  "+str(statistics_data['distressed_sales'])+"\
                     of the properties are distressed sales (either REO bank owned or Short Sale).  In the past 180 days, "+str(statistics_data['sold_properties_count'])+" properties have sold in "+str(place)+".\
                     Sales prices during this time period ranged from $"+str(intcomma(statistics_data['sold_properties_prices']['C_AskPrice__min'] or 0))+" - $"+str(intcomma(statistics_data['sold_properties_prices']['C_AskPrice__max'] or 0))+".\
                     On average these properties were on the market for "+str(intcomma(round(statistics_data['avg_sold_market_days'], 2))) + " days and final sales prices were on average "+str(round(statistics_data['sold_price_percentage'],2))+"% of the original listing price.\
                         " + str(round(statistics_data['cash_sales'],2)) + "% of the sales were reported as cash sales."

        big_table.content = stat_text
        print str(big_table.function) + str(big_table.scope)
        print big_table.content
        big_table.save()


class VendorType(models.Model):
    """
    Vendor list should be able to be categorized by business types (painting, architects, tree removal etc.).
    """
    name = models.CharField(max_length=100, unique=True,)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.name)



class Vendor(models.Model):
    '''
        Model for Vendor list in property page i.e. Pest,Electronics etc.
    '''
    name_of_organization = models.CharField(max_length=200, default='',
                                    help_text='Enter vendor organization/company name')
    type_of_business = models.ForeignKey(VendorType)
    name_of_contractor = models.CharField(max_length=200, blank=True, null=True,
                                        help_text='Enter vendor contractor name')
    phone = models.CharField(max_length=200)
    address = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    vendor_image = models.ImageField(upload_to='vendor_images', null=True, blank=True)
    vendor_image_thumbnail = models.ImageField(upload_to='vendor_images/thumbnails/', null=True, blank=True)    
    vendor_video = models.FileField(upload_to='vendor_videos', null=True, blank=True)   
    vendor_video_thumbnail =  models.ImageField(upload_to='vendor_videos/thumbnails/', null=True, blank=True)    
    city = models.ManyToManyField(City, null=True, blank=True)
    community = models.ManyToManyField(Community, null=True, blank=True)
    area = models.ManyToManyField(Area, null=True, blank=True)

    @classmethod
    def get_vendorlist_by_type_in_community_area(cls, community = None, area = None, city=None):
        
        if area and community :            
            vendors = cls.objects.filter(Q(community=community) | Q(area=area)).distinct()        
        elif area:             
            vendors = cls.objects.filter(area=area).distinct()
        elif community:             
            vendors = cls.objects.filter(community=community).distinct()                                
        elif city:             
            vendors = cls.objects.filter(city=city).distinct()                        

        vendor_groups = {}
        for vendor in vendors:
            #for every business type associated with the vendor add the vendor in that group
            business_type = str(vendor.type_of_business.name)
            if business_type in vendor_groups:                           
                vendor_groups[business_type].append(vendor)
            else:
                 vendor_groups[business_type] = [vendor]           

        return vendor_groups

    def create_vendor_image_thumbnails(self):
        '''
            Create thumbnail for vendor image
        '''
        
        
        try:
            if self.vendor_image:
                logger.debug('Creating Vendor image thumbnail....')
                THUMBSIZE = (100, 100)
                image = Image.open(self.vendor_image)
                if image.mode not in ('L', 'RGB'):
                    image = image.convert('RGB')
                image.thumbnail(THUMBSIZE,
                                Image.ANTIALIAS)
                temp_handle = StringIO()
                image.save(temp_handle, 'png')
                temp_handle.seek(0)

                uploaded_file = SimpleUploadedFile(os.path.split(self.vendor_image.name)[-1],
                                                   temp_handle.read(), content_type='image/png')
                self.vendor_image_thumbnail.save(os.path.splitext(uploaded_file.name)[0] + '.thumb.png', uploaded_file, save=False)
        except Exception as e :
            logger.info("Error occured from function create vendor image thumbnail...")        
            logger.error(e)

        return


    def create_video_thumb(self):
        
        instance = self        
        if instance.vendor_video:
            try:
                #source = "%s" % smart_unicode(self.vendor_video,encoding='utf-8',errors='strict')  
                sourcefile = settings.MEDIA_ROOT +'/'+ str(instance.vendor_video)           
                thumbnail_filename = "%s/vendor_videos/thumbnails/%s.png" % (settings.MEDIA_ROOT, sourcefile.split('/')[-1].split('.')[0])
                OUTPUT_IMAGE_EXT = 'png'
                OUTPUT_IMAGE_CONTENT_TYPE = 'image/png'    
                f_out = tempfile.NamedTemporaryFile(suffix=".%s"%OUTPUT_IMAGE_EXT, delete=False)
                tmp_output_image = f_out.name
                ffmpeg_command_thumbnail = "ffmpeg -y -i %s -vframes 1 -ss 00:00:02 -an -vcodec png -f rawvideo -s 120x80 %s" % (
                    sourcefile, tmp_output_image)
                grab = commands.getoutput(ffmpeg_command_thumbnail)
                logger.debug(grab)                
                imagefilename = "%s_%s.%s" % (instance.vendor_video, instance.id, OUTPUT_IMAGE_EXT)
                uploaded_file = SimpleUploadedFile(imagefilename,
                                                   f_out.read(),  content_type=OUTPUT_IMAGE_CONTENT_TYPE)
                instance.vendor_video_thumbnail.save(imagefilename, uploaded_file, save=False)
                os.remove(tmp_output_image)           
            except Exception, e:
                print 'Error creating video thumbnail using FFMPEG'
                print(str(e))    

            return

    

    def save(self, *args, **kwargs):

        '''
            Override save method to create thumbnail dynamically while creating objects.
        '''
        super(Vendor, self).save(*args, **kwargs)

        try : 
            self.create_vendor_image_thumbnails()            
        except Exception as e:            
            print "Exception Occurred while creating vendor thumbnail"
            print e        
        try : 
            self.create_video_thumb()
        except Exception as e:            
            print "Exception Occurred while creating vendor video image thumbnail"
            print e

        super(Vendor, self).save(*args, **kwargs)


    def __unicode__(self):
        if self.name_of_organization:
            vendor =  u'%s - %s' % (self.name_of_organization, self.phone)
        if self.name_of_contractor:
            vendor =  u'%s - %s' % (self.name_of_contractor, self.phone)
        return vendor



class PropertyManagement(models.Model):
    """
        Model for Property Management page
    """
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.description)


class PropertyManagementImage(models.Model):
    """
        Model for Image in Property Manage Page
    """
    mangagement = models.ForeignKey(PropertyManagement, related_name = "images")
    image = models.ImageField(upload_to='management_images', null=True, blank=True)


#Business name, business type, business address full, business phone number, business website, business email, description of business    


class BusinessType(models.Model):
    """
    Business directory list should be able to be categorized by business types.
    """
    name = models.CharField(max_length=100, unique=True,)
    description = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u'%s' % (self.name)


class BusinessDirectory(models.Model):
    '''
        Model for BusinessDirectory 
    '''
    name = models.CharField(max_length=200, default='',
                                    help_text='Enter Business Name')
    type_of_business = models.ForeignKey(BusinessType)
    email = models.EmailField()        
    phone = models.CharField(max_length=200)
    website = models.URLField(max_length=500, blank=True, default='')
    address = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    business_image = models.ImageField(upload_to='busniness_images', null=True, blank=True)
    business_image_thumbnail = models.ImageField(upload_to='busniness_images/thumbnails/', null=True, blank=True)    
    city = models.ManyToManyField(City, null=True, blank=True)
    community = models.ManyToManyField(Community, null=True, blank=True)
    area = models.ManyToManyField(Area, null=True, blank=True)


    @classmethod
    def get_businessslist_by_type_in_community_area(cls, community = None, area = None, city=None):
        
        if area and community :            
            businesses = cls.objects.filter(Q(community=community) | Q(area=area)).distinct()        
        elif area:             
            businesses = cls.objects.filter(area=area).distinct()
        elif community:             
            businesses = cls.objects.filter(community=community).distinct()                                
        elif city:             
            businesses = cls.objects.filter(city=city).distinct()                        

        business_groups = {}
        for business in businesses:
            #for every business type associated with the vendor add the vendor in that group
            business_type = str(business.type_of_business.name)
            if business_type in business_groups:                           
                business_groups[business_type].append(business)
            else:
                 business_groups[business_type] = [business]           

        return business_groups

    def create_businesss_image_thumbnails(self):
        '''
            Create thumbnail for businesss image
        '''
        from PIL import ImageOps
        
        try:
            if self.business_image:
                logger.debug('Creating bunsiness image thumbnail....')
                THUMBSIZE = (700, 500)
                image = Image.open(self.business_image)
                if image.mode not in ('L', 'RGB'):
                    image = image.convert('RGB')
                image.thumbnail(THUMBSIZE,
                                Image.ANTIALIAS)
                ImageOps.fit(image, THUMBSIZE, Image.ANTIALIAS)
                
                temp_handle = StringIO()
                image.save(temp_handle, 'png')
                temp_handle.seek(0)

                uploaded_file = SimpleUploadedFile(os.path.split(self.business_image.name)[-1],
                                                   temp_handle.read(), content_type='image/png')
                self.business_image_thumbnail.save(os.path.splitext(uploaded_file.name)[0] + '.thumb.png', uploaded_file, save=False)
        except Exception as e :
            logger.info("Error occured from function create vendor image thumbnail...")        
            logger.error(e)

        return

    def save(self, *args, **kwargs):

        '''
            Override save method to create thumbnail dynamically while creating objects.
        '''
        super(BusinessDirectory, self).save(*args, **kwargs)

        try : 
            self.create_businesss_image_thumbnails()            
        except Exception as e:            
            print "Exception Occurred while creating vendor thumbnail"
            print e        
        

        super(BusinessDirectory, self).save(*args, **kwargs)


    def __unicode__(self):
        return u'%s' % (self.name)
